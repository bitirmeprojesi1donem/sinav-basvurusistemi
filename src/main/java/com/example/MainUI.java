package com.example;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.example.interfaces.ExamsUsers;
import com.example.interfaces.addApplication;
import com.example.model.Officer;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.service.ExamService;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hatice
 */

@Theme("mytheme")
@SpringUI(path = "")

public class MainUI extends UI{
    
    public static CssLayout layout = new CssLayout();

    @Autowired
    private SpringViewProvider viewProvider;
     
    @Autowired
    ExamService examService;

    @Override
    protected void init(VaadinRequest request) {
        layout.setStyleName("vlayout");
        
        setContent(layout);
        
        Navigator navigator = new Navigator(this, layout);
        navigator.addProvider(viewProvider);
        if(getSession().getAttribute("userRole") instanceof Officer && getSession().getAttribute("userRole") instanceof Teacher){
            navigator.navigateTo("Sinav-DerslikEkleme");
        }
        else if(getSession().getAttribute("userRole") instanceof Teacher){
            navigator.navigateTo("SinavEkleme");
        }
//        else if(getSession().getAttribute("userRole") instanceof Student){
//            navigator.navigateTo("SinavBasvuru");
//        }
        else{
            navigator.navigateTo("Login");
        }
    }
    
}
