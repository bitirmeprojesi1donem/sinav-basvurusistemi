/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Application;
import com.example.model.Document;
import com.example.model.Information;
import com.example.model.Teacher;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hatice
 */
@Repository
public interface ApplicationRepository extends GraphRepository<Application>{
    
    @Query ("MATCH (e:Application {name: {name}, teacher: {teacher}}) RETURN count(e)")
    public int control(@Param("name") String name);

    @Query ("START e=NODE({id}) MATCH (e) -[r:aittir] - (b :Department) RETURN  b.name")
    public String getApplicationDepartment(@Param("id") Long id);
    
    @Query ("START s=NODE({infoId}) MATCH (s) -[r:requiredInformation] -> (t) RETURN t")
    public ArrayList<Information> getInformationByAppId(@Param("infoId") Long infoId);
    
    @Query("START s=NODE({value}), a=NODE({id}) MATCH(a) MATCH(s) CREATE (a)-[:aittir]->(s)")    
    public void isBelongTo(@Param("value")Object value, @Param("id") Long id);
    
    @Query ("START s=NODE({teacherId}), a=NODE({examId}) MATCH(a) MATCH(s) CREATE (a)-[:isToughtBy]->(s)")
    public void isToughtBy(@Param("teacherId") Long teacherId, @Param("examId") Long examId);
    
    @Query ("Start i=NODE({iid}), e=NODE({id}) MATCH(i) MATCH(e) MATCH( (e)-[r:requiredInformation]->(i) ) SET r.percent={yuzde}")
    public void requiredInformationWithCriterion(@Param("iid") Long iid, @Param("id") Long id, @Param("yuzde") String yuzde);
    
    @Query ("Start i=NODE({iid}), e=NODE({id}) MATCH(i) MATCH(e) CREATE (e)-[:requiredInformation]->(i)")
    public void requiredInformation(@Param("iid") Long iid, @Param("id") Long id);
    
    
    @Query ("Start d=NODE({did}), e=NODE({id}) MATCH(d) MATCH(e) CREATE (e)-[:required]->(d)")
    public void required(@Param("did") Long did, @Param("id") Long id);
    
    @Query ("START s=NODE({infoId}) MATCH (s) -[r:requiredInformation] -> (t) RETURN t.name")
    public ArrayList<String> getInformationNameByAppId(@Param("infoId")Long infoId);
    
    @Query ("START s=NODE({docId}) MATCH (s) -[r:required] -> (t) RETURN t")
    public ArrayList<Document> getDocumentByAppId(@Param("docId") Long docId);
    
    @Query ("START s=NODE({docId}) MATCH (s)-[r:required]-> (t) RETURN t.name")
    public ArrayList<String> getDocumentNameByAppId(@Param("docId") Long docId);
    
    @Query ("START s=NODE({id}) MATCH (s) -[r] - (b :Department) DELETE  r")
    public void deleteDeparmentRelationship(@Param("id") Long id);

    @Query ("START s=NODE({id}) MATCH (s)-[r]-(t:Information) DELETE  r")
    public void deleteRequiredInformation(@Param("id") Long id);
    
    @Query ("START s=NODE({id}) MATCH (s)-[r]-(t:Document) DELETE  r")
    public void deleteRequired(@Param("id") Long id);
    
    @Query ("START s=NODE({id}) MATCH (s) -[r] - (t:Teacher) DELETE  r")
    public void deleteTeacherRelationship(@Param("id") Long id);
    
    @Query ("START s=NODE({id}) MATCH (s) -[r] - (t :Student) DELETE  r")
    public void deleteApplyRelationship(@Param("id") Long id);
    
    @Query ("MATCH (e:Application {name: {name}}) DELETE e")
    public void deleteExam(@Param("name") String name);
    
    @Query ("START s=NODE({sId}) MATCH(s) MATCH( (s)-[:isApply]->(e:Exam)-[:isType]->(ty:Type) ) MATCH((s)-[:isApply]->(e:Exam)-[:aittir]->(d:Department)) MATCH( (s)-[:isApply]->(e:Exam)-[:isToughtBy]->(t:Teacher) ) RETURN e.name,e.time, e.date, e.personCount, e.startTime, e.endTime, d.name, ty.name, t.name, t.surname, ID(e) ORDER BY e.date DESC")
    public Iterable<Map<String, String>> getReferencedAppsOfStudent(@Param("sId") Long sId);
    
    @Query ("START t=NODE({id}) MATCH(t) MATCH((t)<-[:isToughtBy]-(e:Application)) MATCH((t)<-[:isToughtBy]-(e:Application)-[:aittir]->(d:Department)) RETURN e.name,e.time, e.date, e.personCount, e.startTime, e.endTime, d.name, ID(e) ORDER BY ID(e) DESC")
    Iterable<Map<String, String>> getAllApps(@Param("id") Long id);

    @Query ("Start i=NODE({iid}), e=NODE({id}) MATCH(i) MATCH(e) CREATE (e)-[:requiredInformation{percent:{yuzde}}]->(i)")
    public void requiredInformation(@Param("iid") Long iid, @Param("id") Long id, @Param("yuzde") String yuzde);
   
    @Query (" START info=NODE({id0}), bsv=NODE({id})  MATCH p=(bsv)-[r:requiredInformation]->(info) RETURN r.percent")
    public String getRequiredInfo(@Param("id") Long id, @Param("id0") Long id0);

    
    @Query ("START n=NODE({id})  RETURN n.totalOfPercent")
    public Integer getAppTotOfPer(@Param("id") Long id);

    @Query ("START d=NODE({id}) MATCH (d)<-[r:aittir]-(e:Application) RETURN  e")   
    public List<Application> getAppOfDepartment(@Param("id") Long bolumId);
    
    
    @Query("START e=NODE({id}) MATCH(e) RETURN e")
    public Application getAppFromId(@Param("id") Long id);
    
    
    @Query ("START e=NODE({id}) MATCH (e) -[r:aittir] - (b :Department) RETURN  b.name")
    public String getAppDepartment(@Param("id") Long id);
    
    @Query ("START e=NODE({appId}) MATCH (e) -[r:isToughtBy] - (t:Teacher) RETURN  t")
    public Teacher getTeacher(@Param("appId") Long appId); 
    
     
    @Query ("START s=NODE({studentId}), a=NODE({examId}) MATCH(a) MATCH(s) MATCH((s)-[:isApply]->(a)) RETURN a")
    public Application getApp(@Param("studentId") Long studentId, @Param("examId") Long examId);
    
    @Query ("START s=NODE({studentId}), a=NODE({examId}) MATCH(a) MATCH(s) CREATE (s)-[r:isApply]->(a) RETURN ID(a)")
    public Long isApply(@Param("studentId") Long studentId, @Param("examId") Long examId);

    @Query ("START s=NODE({sId}), d=NODE({dId}) MATCH(s) MATCH(d) MATCH( (s)-[l:loadFor]->(d) ) RETURN l.file")
    public String getLoadFor(@Param("sId") Long sId, @Param("dId") Long dId);

    @Query ("START s=NODE({sId}), i=NODE({fId}) MATCH(s) MATCH(i) MATCH( (s)-[e:enterFor]->(i) ) RETURN e.value")
    public String getEnterFor(@Param("sId") Long sId, @Param("fId") Long fId);

    @Query ("START s=NODE({sid}), d=NODE({iid}) MATCH(s) MATCH(d) CREATE (s)-[:enterFor {value:{ivalue}}]->(d)")
    public void enterFor(@Param("sid") Long sid, @Param("iid") Long iid, @Param("ivalue") String ivalue);
    
    @Query ("START s=NODE({sid}), d=NODE({did}) MATCH(s) MATCH(d) MATCH((s)-[l:enterFor]->(d)) SET l.value={value}")
    public void updateEnterFor(@Param("sid") Long sid, @Param("did") Long did, @Param("value") String value);
    
     @Query ("START s=NODE({sid}), d=NODE({did}) MATCH(s) MATCH(d) MATCH((s)-[l:loadFor]->(d)) SET l.file={dname}")
    public void updateLoadFor(@Param("sid") Long sid, @Param("did") Long did, @Param("dname") String dname);
   
     @Query ("START s=NODE({sid}), d=NODE({did}) MATCH(s) MATCH(d) CREATE (s)-[:loadFor {file:{dname}}]->(d)")
    public void loadFor(@Param("sid") Long sid, @Param("did") Long did, @Param("dname") String dname);

    @Query ("START b=NODE({bId}), k=NODE({kId}) MATCH(b) MATCH(k) CREATE (b)-[:hasCriterion]->(k)")
    public void createCriterionRelation(@Param("bId") Long bId, @Param("kId") Long kId);

    @Query ("START k=NODE({kId}), i=NODE({iId}) MATCH(k) MATCH(i) CREATE (k)-[:percentCriterion{percent:{percent}}]->(i)")
    public void createPercentRelation(@Param("kId") Long kId, @Param("iId") Long iId, @Param("percent") Integer percent);

    @Query ("START b=NODE({bId}), k=NODE({kId}) MATCH(b) MATCH(k) CREATE (b)-[:hasFinalCriterion]->(k)")
    public void createFinalCriterion(@Param("bId") Long bId, @Param("kId") Long kId);

    

    
    
    
    
    
    
}
