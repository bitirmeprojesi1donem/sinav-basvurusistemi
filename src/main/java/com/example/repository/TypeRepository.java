/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Type;
import java.util.List;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hatice
 */
@Repository
public interface TypeRepository extends GraphRepository<Type>{

    @Query ("MATCH (t:Type) RETURN t")
    public List<Type> getAllTypes();
    
}
