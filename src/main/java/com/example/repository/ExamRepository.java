/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Classroom;
import com.example.model.Department;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.Student;
import com.example.model.Teacher;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hatice
 */
@Repository
public interface ExamRepository extends GraphRepository<Exam>{
    
    @Query ("START t=NODE({id}) MATCH(t) MATCH((t)<-[:isToughtBy]-(e:Exam)-[:isType]->(ty:Type)) MATCH((t)<-[:isToughtBy]-(e:Exam)-[:aittir]->(d:Department)) RETURN e.name,e.time, e.date, e.personCount, e.startTime, e.endTime, d.name, ty.name, ID(e) ORDER BY ID(e) DESC")
    Iterable<Map<String, String>> getAllExams(@Param("id") Long id);
    
    @Query ("CREATE (e:Exam {name: {name} , type: {type} , teacher: {teacher}, date: {date}, time:{time}}) RETURN e.name")
    public String Create(@Param("name") String name, @Param("type") String type, @Param("teacher") String teacher, @Param("date") String date ,@Param("time") String time);

    @Query ("MATCH (e:Exam {name: {name}, type: {type} , teacher: {teacher}, date: {date},  time:{time}}) RETURN count(e)")
    public int control(@Param("name") String name, @Param("date") String date, @Param("time") String time);

    @Query ("MATCH (e:Exam {name: {name}, date: {date},  time:{time}}) DELETE e")
    public void deleteExam(@Param("name") String name, @Param("date") String date,@Param("time") String time);

    @Query("START s=NODE({value}), a=NODE({id}) MATCH(a) MATCH(s) CREATE (a)-[:aittir]->(s)")    
    public void isBelongTo(@Param("value")Object value, @Param("id") Long id);
    
  /*   @Query ("START n=node(*) \n" +
    "MATCH (n)-[rel:icerir]->(r) \n" +
    "WHERE n.name={value} AND r.name={id} \n" +
    "DELETE rel")
    public void deleteDeparmentRelationship(@Param("value")Object value,@Param("id") Long id);*/
    
    @Query ("START s=NODE({id}) MATCH (s) -[r] - (b :Department) DELETE  r")
    public void deleteDeparmentRelationship(@Param("id") Long id);

     
  /*  @Query("START s=NODE({b}), a=NODE({id}) MATCH(a) MATCH(s) CREATE (s)-[:icerir]->(a)")     
    public void isBelongToUpdate(Long b, Long id);*/

    @Query ("START e=NODE({id}) MATCH (e) -[r:aittir] - (b :Department) RETURN  b.name")
    public String getExamDepartment(@Param("id") Long id);

    
    @Query ("START d=NODE({id}) MATCH (d)<-[r:aittir]-(e:Exam) RETURN  e")
    public List<Exam> getExamOfDepartment(@Param("id") Long id);
    
    @Query("START e=NODE({id}) MATCH(e) RETURN e")
    public Exam getExamFromId(@Param("id") Long id);

    @Query ("START s=NODE({teacherId}), a=NODE({examId}) MATCH(a) MATCH(s) CREATE (a)-[:isToughtBy]->(s)")
    public void isToughtBy(@Param("teacherId") Long teacherId, @Param("examId") Long examId);

    @Query ("START s=NODE({examId}), a=NODE({typeId}) MATCH(a) MATCH(s) CREATE (s)-[:isType]->(a)")
    public void isType(@Param("examId") Long examId, @Param("typeId") Long typeId);

    @Query ("START s=NODE({id}) MATCH (s) -[r] - (t :Type) DELETE  r")
    public void deleteTypeRelationship(@Param("id") Long id);

    @Query ("START e=NODE({id}) MATCH (e) -[r:isType] - (t:Type) RETURN  t.name")
    public String getExamType(@Param("id") Long id);

    @Query ("START s=NODE({id}) MATCH (s) -[r] - (t:Teacher) DELETE  r")
    public void deleteTeacherRelationship(@Param("id") Long id);

    @Query ("START e=NODE({sinavId}) MATCH (e) -[r:isToughtBy] - (t:Teacher) RETURN  t")
    public Teacher getTeacher(@Param("sinavId") Long sinavId); 

    @Query ("START s=NODE({studentId}), a=NODE({examId}) MATCH(a) MATCH(s) CREATE (s)-[r:isApply]->(a) RETURN ID(a)")
    public Long isApply(@Param("studentId") Long studentId, @Param("examId") Long examId);

    @Query ("START t=NODE({id}) MATCH(t) MATCH((t)<-[:isToughtBy]-(e:Exam)<-[:isApply]-(s:Student)) RETURN s.name, s.surname, e.name")
    public Iterable<Map<String, String>> getApplies(@Param("id") Long id);

    @Query ("START s=NODE({id}) MATCH (s) -[r] - (t :Student) DELETE  r")
    public void deleteApplyRelationship(@Param("id") Long id);
    
    @Query ("START s=NODE({studentId}), a=NODE({examId}) MATCH(a) MATCH(s) MATCH((s)-[:isApply]->(a)) RETURN a")
    public Exam getApp(@Param("studentId") Long studentId, @Param("examId") Long examId);
    
    @Query ("Start d=NODE({did}), e=NODE({id}) MATCH(d) MATCH(e) CREATE (e)-[:required]->(d)")
    public void required(@Param("did") Long did, @Param("id") Long id);

    @Query ("MATCH (d:Document{ name: {get}}) RETURN ID(d)")
    public Long getDocument(@Param("get") String get);

    @Query ("START e=NODE({sinavId}) MATCH(e) MATCH((e)-[:required]->(d)) RETURN ID(d)")
    public ArrayList<Long> getRequiredDocumentForExam(@Param("sinavId") Long sinavId);

    @Query ("START s=NODE({sid}), d=NODE({did}) MATCH(s) MATCH(d) CREATE (s)-[:loadFor {file:{dname}}]->(d)")
    public void loadFor(@Param("sid") Long sid, @Param("did") Long did, @Param("dname") String dname);

    @Query ("START e=NODE({id}) MATCH(e) SET e.{info}=0")
    public void setProperty(@Param("id") Long id, @Param("info") String info);

    @Query ("Start i=NODE({iid}), e=NODE({id}) MATCH(i) MATCH(e) CREATE (e)-[:requiredInformation]->(i)")
    public void requiredInformation(@Param("iid") Long iid, @Param("id") Long id);

    @Query ("START s=NODE({sid}), d=NODE({iid}) MATCH(s) MATCH(d) CREATE (s)-[:enterFor {value:{ivalue}}]->(d)")
    public void enterFor(@Param("sid") Long sid, @Param("iid") Long iid, @Param("ivalue") String ivalue);

    @Query ("START s=NODE({id}) MATCH (s)-[r]-(t:Document) DELETE  r")
    public void deleteRequired(@Param("id") Long id);

    @Query ("START s=NODE({id}) MATCH (s)-[r]-(t:Information) DELETE  r")
    public void deleteRequiredInformation(@Param("id") Long id);

    @Query ("START e=NODE({eid}), c=NODE({cid}) MATCH(e) MATCH(c) CREATE (e)-[:madeIn]->(c)")
    public void madeIn(@Param("eid") Long eid, @Param("cid") Long cid);
    
    //START s=NODE(20) MATCH (s) -[r:madeIn] -> (t) RETURN t

    @Query ("START s=NODE({eid}) MATCH (s) -[r:madeIn] -> (t) RETURN t")
    public ArrayList<Classroom> getClassroomByExamId(@Param("eid") Long sinavId);

    @Query ("START s=NODE({infoId}) MATCH (s) -[r:requiredInformation] -> (t) RETURN t")
    public ArrayList<Information> getInformationByExamId(@Param("infoId") Long infoId);

    @Query ("START s=NODE({infoId}) MATCH (s) -[r:requiredInformation] -> (t) RETURN t.name")
    public ArrayList<String> getInformationNameByExamId(@Param("infoId")Long infoId);

    @Query ("START s=NODE({docId}) MATCH (s) -[r:required] -> (t) RETURN t.name")
    public ArrayList<String> getDocumentNameByExamId(@Param("docId") Long docId);

    @Query ("START s=NODE({docId}) MATCH (s) -[r:required] -> (t) RETURN t")
    public ArrayList<Document> getDocumentByExamId(@Param("docId") Long docId);

    @Query ("START s=NODE({id}) MATCH (s)-[r]-(t:Classroom) DELETE  r")
    public void deleteMadeInRelationship(@Param("id") Long id);

    @Query ("START t=NODE({id}) MATCH (t) <-[r:isToughtBy]-(e:Exam) RETURN e")
    public ArrayList<Exam> getExamsName(@Param("id") Long id);

    @Query ("START t=NODE({id}), e=NODE({sinavId}) MATCH(t) MATCH(e) MATCH((t)<-[:isToughtBy]-(e)<-[:isApply]-(s:Student)) RETURN s.name, s.surname, e.name")
    public Iterable<Map<String, String>> getApplicantStudents(@Param("id") Long id, @Param("sinavId") Long sinavId);

    @Query ("START t=NODE({id}), e=NODE({sinavId}) MATCH(t) MATCH(e) MATCH((t)<-[:isToughtBy]-(e)<-[:isApply]-(s:Student)) RETURN ID(s)")
    public List<Integer> getApplicantStudentsId(@Param("id") Long id, @Param("sinavId") Long sinavId);
    
    @Query ("START t=NODE({id}) MATCH(t) MATCH((t)<-[:isToughtBy]-(e:Exam)<-[:isApply]-(s:Student)) RETURN ID(s), ID(e)")
    public Iterable<Map<String, Integer>> getAppliesID(@Param("id") Long id);

    @Query ("START t=NODE({id}) MATCH (t)<-[r:isToughtBy]-(e:Exam) RETURN  e")
    public List<Exam> getPastExams(@Param("id") Long id);

    @Query ("START e=NODE({examId}) MATCH(e) MATCH((e)<-[:isApply]-(s:Student)) RETURN s")
    public List<Student> getStudentsOfPastExam(@Param("examId") Long examId);

    @Query ("START e=NODE({examId}), s=NODE({studentId}) MATCH(e) MATCH(s) MATCH( (s)-[i:isApply]->(e) ) SET i.mark={mark}, i.dateOfPublication={tarih} RETURN i.mark")
    public int addMarksOfExam(@Param("examId") Long examId, @Param("studentId") Long studentId, @Param("mark") Integer mark, @Param("tarih") String tarih);

    @Query ("START e=NODE({examId}), s=NODE({studentId}) MATCH(e) MATCH(s) MATCH( (s)-[i:isApply]->(e) ) RETURN i.mark")
    public int getStudentsMarkOfExam(@Param("examId") Long examId, @Param("studentId") Long studentId);

//    @Query ("")
//    public List<Information> getInformationOfStudent(@Param("studentId") Long studentId);

    @Query ("START s=NODE({sId}) MATCH(s) MATCH( (s)-[:isApply]->(e:Exam)-[:isType]->(ty:Type) ) MATCH((s)-[:isApply]->(e:Exam)-[:aittir]->(d:Department)) MATCH( (s)-[:isApply]->(e:Exam)-[:isToughtBy]->(t:Teacher) ) RETURN e.name,e.time, e.date, e.personCount, e.startTime, e.endTime, d.name, ty.name, t.name, t.surname, ID(e) ORDER BY e.date DESC")
    public Iterable<Map<String, String>> getReferencedExamsOfStudent(@Param("sId") Long sId);

    @Query ("START s=NODE({sId}) MATCH(s) MATCH( (s)-[i:isApply]->(e:Exam) ) RETURN e.name, i.mark, i.dateOfPublication")
    public Iterable<Map<String, String>> getResultsOfExamForStudent(@Param("sId") Long sId);

    @Query ("START s=NODE({sId}), d=NODE({dId}) MATCH(s) MATCH(d) MATCH( (s)-[l:loadFor]->(d) ) RETURN l.file")
    public String getLoadFor(@Param("sId") Long sId, @Param("dId") Long dId);

    @Query ("START s=NODE({sId}), i=NODE({fId}) MATCH(s) MATCH(i) MATCH( (s)-[e:enterFor]->(i) ) RETURN e.value")
    public String getEnterFor(@Param("sId") Long sId, @Param("fId") Long fId);

    @Query ("START s=NODE({studentId}), e=NODE({examId}) MATCH(s) MATCH(e) MATCH( (s)-[ef:enterFor]->(i:Information)<-[:requiredInformation]-(e) ) RETURN i.name, ef.value")
    public Iterable<Map<String, String>> getInformationOfStudentForExam(@Param("studentId") Long studentId, @Param("examId") Long examId);

    @Query ("START s=NODE({studentId}), e=NODE({examId}) MATCH(s) MATCH(e) MATCH( (s)-[l:loadFor]->(d:Document)<-[:required]-(e) ) RETURN d.name, l.file")
    public Iterable<Map<String, String>> getDocumentOfStudentForExam(@Param("studentId") Long studentId, @Param("examId") Long examId);

    @Query ("START s=NODE({studentId}), e=NODE({examId}) MATCH(s) MATCH(e) MATCH( (s)-[i:isApply]->(e) ) DELETE i")
    public void deleteIsApply(@Param("studentId") Long studentId, @Param("examId") Long examId);

    @Query ("START s=NODE({studentId}), e=NODE({examId}) MATCH(s) MATCH(e) MATCH( (s)-[i:isApply]->(e) ) RETURN ID(i)")
    public Long getIsApply(@Param("studentId") Long studentId, @Param("examId") Long examId);

    @Query ("START s=NODE({studentId}), e=NODE({examId}) MATCH(e) MATCH(s) CREATE (s)-[r:isDelete]->(e) RETURN ID(r)")
    public Long isDelete(@Param("studentId") Long studentId, @Param("examId") Long examId);

    @Query ("START s=NODE({sId}) MATCH(s) MATCH( (s)-[:isDelete]->(e:Exam) ) RETURN e")
    public List<Exam> getIsDelete(@Param("sId") Long sId);

    @Query ("START s=NODE({sId}), e=NODE({eId}) MATCH(e) MATCH(s) MATCH( (s)-[r:isDelete]->(e) ) DELETE r")
    public void deleteIsDelete(@Param("sId") Long sId, @Param("eId") long eId);
    
    @Query ("START s=NODE({sId}), e=NODE({eId}) MATCH(s) MATCH(e) MATCH( (s)-[i:isDelete]->(e) ) RETURN ID(i)")
    public Long getOneIsDelete(@Param("sId") Long sId, @Param("eId") long eId);
    
    @Query ("START s=NODE({sid}), d=NODE({did}) MATCH(s) MATCH(d) MATCH((s)-[l:enterFor]->(d)) SET l.value={value}")
    public void updateEnterFor(@Param("sid") Long sid, @Param("did") Long did, @Param("value") String value);
    
    @Query ("START s=NODE({sid}), d=NODE({did}) MATCH(s) MATCH(d) MATCH((s)-[l:loadFor]->(d)) SET l.file={dname}")
    public void updateLoadFor(@Param("sid") Long sid, @Param("did") Long did, @Param("dname") String dname);
    
    @Query ("START b=NODE({bId}), e=NODE({eId}) MATCH(b) MATCH(e) CREATE (e)-[:doFor]->(b)")
    public void doFor(@Param("bId") Long bId, @Param("eId") Long eId);

    @Query ("START e=NODE({eId}), e2=NODE({eFId}) MATCH(e) MATCH(e2) CREATE (e)-[:criterion{percent:{value}}]->(e2)")
    public void addCriterion(@Param("eId") Long eId, @Param("eFId") Long eFId, @Param("value") String value);

    @Query("START r=NODE({id}) MATCH p=(b)-[d:doFor]->(r) RETURN b")   
    public ArrayList<Exam> findExamsforapp(@Param("id") Long id);

    @Query("START s=NODE({id}) MATCH p=(s)-[r:isType]->(b) RETURN b.name")
    public String getTypeForExam(@Param("id") Long id);

     @Query("START s=NODE({id}) MATCH p=(s)-[r:madeIn]->(b) RETURN b.name")
    public String getClasroom(@Param("id") Long id);

    
}
