/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Department;
import java.util.List;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Deneme
 */
@Repository
public interface DepartmentRepository extends GraphRepository<Department>
{
    @Query ("MATCH (d:Department) RETURN d ")
    List<Department> getAllDepartments();

    @Query ("START t=NODE({i}), o=NODE({oid}) MATCH(t) MATCH(o) CREATE (o)-[r:workIn]->(t) RETURN ID(o)")
    public Long createWorkIn(@Param("i") Long i, @Param("oid") Long oid);
    
    
}
