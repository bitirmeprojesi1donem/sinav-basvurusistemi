/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Officer;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hatice
 */
@Repository
public interface OfficerRepository extends GraphRepository<Officer>{
    @Query ("MATCH (t:Officer {tc: {tc}, password: {password}}) RETURN t")
    public Officer getOfficer(@Param("tc") String tc, @Param("password") String password);

    @Query ("START o=NODE({id}) MATCH(o) MATCH((o)-[:workIn]->(d)) RETURN ID(d)")
    public Long getDepartmentOfOfficer(@Param("id") Long id);

    @Query ("START d=NODE({did}), c=NODE({cid}) MATCH(d) MATCH(c) CREATE (d)-[r:has]->(c) RETURN ID(r)")
    public Long has(@Param("did") Long did, @Param("cid") Long cid);
}
