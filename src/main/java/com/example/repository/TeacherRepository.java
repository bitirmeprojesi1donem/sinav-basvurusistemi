/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Teacher;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hatice
 */

@Repository
public interface TeacherRepository extends GraphRepository<Teacher>{
    
    @Query ("MATCH (t:Teacher {tc: {tc}, password: {password}}) RETURN t")
    public Teacher getTeacher(@Param("tc") String tc, @Param("password") String password);
}
