/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Document;
import com.example.model.Exam;
import java.util.ArrayList;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hatice
 */
@Repository
public interface DocumentRepository extends GraphRepository<Document> {
    @Query ("MATCH (d:Document{ name: {get}}) RETURN ID(d)")
    public Long getDocument(@Param("get") String get);
    
    @Query ("START e=NODE({sinavId}) MATCH(e) MATCH((e)-[:required]->(d)) RETURN d")
    public ArrayList<Document> getRequiredDocumentForExam(@Param("sinavId") Long sinavId);
    
    @Query ("CREATE (d:Document { name: {get}}) RETURN ID(d)")
    public Long addNeedDocument(@Param("get") String get);
    
    
     @Query ("START e=NODE({appId}) MATCH(e) MATCH((e)-[:required]->(d)) RETURN d")
    public ArrayList<Document> getRequiredDocumentForApp(@Param("appId") Long appId);
}
