/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import com.vaadin.data.util.BeanItemContainer;
import java.util.Collection;

/**
 *
 * @author hatice
 */
public class Util {
    public static <T> BeanItemContainer<T> getFilledBeanItemContainer(final Class<T> cla, Collection<T> c) {
        final BeanItemContainer<T> bic = new BeanItemContainer<T>(cla);
        bic.addAll(c);
        return bic;
    }
}

