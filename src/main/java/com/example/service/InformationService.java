/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Information;
import com.example.repository.InformationRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hatice
 */
@Service
@Transactional
public class InformationService {
    @Autowired
    InformationRepository informationRepository;

    public Long getInformation(String get) {
        return informationRepository.getInformation(get);
    }

    public Long addNeedInformation(String get) {
        return informationRepository.addNeedInformation(get);
    }

    public ArrayList<Information> getRequiredInformationForExam(Long sinavId) {
        return informationRepository.getRequiredInformationForExam(sinavId);
    }

      public ArrayList<Information> getRequiredInformationForApp(Long appId) {
        return informationRepository.getRequiredInformationForApp(appId);
    }
    
}
