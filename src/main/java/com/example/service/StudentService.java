/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Student;
import com.example.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hatice
 */

@Service
@Transactional
public class StudentService {
    @Autowired
    StudentRepository studentRepository;

    public Student getStudent(String TC, String pass) {
        return studentRepository.getStudent(TC, pass);
    }
    
    
}
