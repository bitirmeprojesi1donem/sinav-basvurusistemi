/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Department;
import com.example.repository.DepartmentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Deneme
 */
@Service
@Transactional
public class DepartmentService {
    @Autowired
    DepartmentRepository dep;
    
    
      
    public List<Department> getAllDepartments()
    {
        return dep.getAllDepartments();
    }

    public Long createWorkIn(Long i, Long oid) {
        return dep.createWorkIn(i, oid);
    }
    
}
