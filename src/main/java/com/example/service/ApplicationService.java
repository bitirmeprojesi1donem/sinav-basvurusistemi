/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Application;
import com.example.model.Document;
import com.example.model.Information;
import com.example.model.Teacher;
import com.example.repository.ApplicationRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hatice
 */
@Service
@Transactional
public class ApplicationService {
    @Autowired
    ApplicationRepository appRepository;
    
    public boolean control(Application model) {
        int kactane = appRepository.control(model.name);
        if (kactane == 0) return true;
        return false;
    }
    
    public Application CreateApplication(Application model) {
        return appRepository.save(model);
        
    }
    
     public String getApplicationDepartment(Long id) {
        return appRepository.getApplicationDepartment(id);
    }
     
      public ArrayList<Information> getInformationByAppId(Long infoId) {
        return appRepository.getInformationByAppId(infoId);
    }
      
    public void isBelongTo(Object value, Long id) {
         appRepository.isBelongTo(value,id);
    }
    
    public void isToughtBy(Long teacherId, Long examId) {
        appRepository.isToughtBy(teacherId, examId);
    }
    
    public void requiredInformationWithCriterion(Long iid, Long id, String yuzde) {
        appRepository.requiredInformationWithCriterion(iid, id, yuzde);
    }
    
    public void requiredInformation(Long iid, Long id){
        appRepository.requiredInformation(iid, id);
    }
  
    public void required(Long did, Long id) {
        appRepository.required(did, id);
    }
    
    public ArrayList<String> getInformationNameByAppId(Long infoId) {
        return appRepository.getInformationNameByAppId(infoId);
    }
    
    public ArrayList<Document> getDocumentByAppId(Long docId) {
       return appRepository.getDocumentByAppId(docId);
    }
    
    public ArrayList<String> getDocumentNameByAppId(Long docId) {
        return appRepository.getDocumentNameByAppId(docId);
    }
    
    public Application UpdateApplication(Application model) {
        return appRepository.save(model);
    }
    
    public void deleteDepartmentRelationship(Long id)
    {
        appRepository.deleteDeparmentRelationship(id);
    }
    
    public void deleteRequiredInformation(Long id) {
        appRepository.deleteRequiredInformation(id);
    }
    
    public void deleteRequired(Long id) {
        appRepository.deleteRequired(id);
    }

    public void deleteTeacherRelationship(Long id) {
        appRepository.deleteTeacherRelationship(id);
    }
    
    public void deleteApplyRelationship(Long id) {
        appRepository.deleteApplyRelationship(id);
    }
    
    public void DeleteExam(Application model) {
        appRepository.deleteExam(model.name);
    }
    
    public Application getOne(Application exam){
        return appRepository.findOne(exam.id);
    }
    
    public Iterable<Map<String, String>> getReferencedExamsOfStudent(Long sId) {
        return appRepository.getReferencedAppsOfStudent(sId);
    }
    
    public Iterable<Map<String, String>> getAllApps(Long teacherId){
        return appRepository.getAllApps(teacherId);
    }
    
    public void requiredInformation(Long iid, Long id, String yuzde) {
        appRepository.requiredInformation(iid, id, yuzde);
    }

    public String getRequiredInfo(Long id, Long id0) {
         return appRepository.getRequiredInfo(id, id0);
    }

    public Integer getAppTotOfPer(Long id) {
        return  appRepository.getAppTotOfPer(id);
    }

    public List<Application> getAppOfDepartment(Long bolumId) {
        return appRepository.getAppOfDepartment(bolumId);
        
    }

    public Application getAppFromId(Long appId) {
        return appRepository.getAppFromId(appId);
    }

    public String getAppDepartment(Long appId) {
       return appRepository.getAppDepartment(appId);
    }

    public Teacher getTeacher(Long appId) {
        return appRepository.getTeacher(appId);
    }

    public Application getApp(Long id, Long id0) {
        return appRepository.getApp(id, id0);
    }

    public Long isApply(Long id, Long id0) {
        return appRepository.isApply(id, id0);
    }

    public String getEnterFor(Long id, Long id0) {
        return appRepository.getEnterFor(id, id0);
    }

    public void enterFor(Long id, Long id0, String value) {
        appRepository.enterFor(id, id0, value);
    }

    public void updateEnterFor(Long id, Long id0, String value) {
       appRepository.updateEnterFor(id, id0, value);
    }

    public String getLoadFor(Long id, Long get) {
       return appRepository.getLoadFor(id, get);
    }

    public void loadFor(Long id, Long get, String get0) {
       appRepository.loadFor(id, get, get0);
    }

    public void updateLoadFor(Long id, Long get, String get0) {
        appRepository.updateLoadFor(id, get, get0);
    }

    public void createCriterionRelation(Long bId, Long kId) {
        appRepository.createCriterionRelation(bId, kId);
    }

    public void createPercentRelation(Long kId, Long iId, Integer percent) {
        appRepository.createPercentRelation(kId, iId, percent);
    }

    public void createFinalCriterion(Long bId, Long kId) {
        appRepository.createFinalCriterion(bId, kId);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
