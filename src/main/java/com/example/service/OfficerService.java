/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Officer;
import com.example.repository.OfficerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hatice
 */
@Service
@Transactional
public class OfficerService {
    @Autowired 
    OfficerRepository officerRepository;

    public Officer getOfficer(String TC, String pass) {
        return officerRepository.getOfficer(TC, pass);
    }

    public Long getDepartmentOfOfficer(Long id) {
        return officerRepository.getDepartmentOfOfficer(id);
    }

    public Long has(Long did, Long cid) {
        return officerRepository.has(did, cid);
    }
}
