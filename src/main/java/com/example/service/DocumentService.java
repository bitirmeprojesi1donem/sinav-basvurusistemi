/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Document;
import com.example.repository.DocumentRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hatice
 */
@Service
@Transactional
public class DocumentService {
     @Autowired
     DocumentRepository documentRepository;
     
     
     public Long getDocument(String get)
     {
         return documentRepository.getDocument(get);
     }
     
     public ArrayList<Document> getRequiredDocumentForExam(Long sinavId) {
        return documentRepository.getRequiredDocumentForExam(sinavId);
    }
     
     public Long addNeedDocument(String get) {
        return documentRepository.addNeedDocument(get);
    }
     
      public ArrayList<Document> getRequiredDocumentForApp(Long appId) {
        return documentRepository.getRequiredDocumentForApp(appId);
    }
     

}
