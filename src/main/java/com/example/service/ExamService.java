/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Classroom;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.repository.ExamRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author hatice
 */
@Service
@Transactional
public class ExamService {
    @Autowired
    ExamRepository examRepository;

    public Iterable<Map<String, String>> getAllExams(Long teacherId){
        return examRepository.getAllExams(teacherId);
    }

    public Exam getOne(Exam exam){
        return examRepository.findOne(exam.id);
    }
    
    public Exam CreateExam(Exam model) {
    //    System.out.println(model.date + "-" + model.departmentName + "-" + model.name + "-" + model.teacher + "-" + model.type);
        /*
        String name = examRepository.Create(model.name, model.type, model.teacher, model.date, model.departmentName);
        System.out.println("name: "+name);
        return name;
        */
        return examRepository.save(model);
        
    }
    

    public boolean control(Exam model) {
        int kactane = examRepository.control(model.name, model.date,model.time);
        if (kactane == 0) return true;
        return false;
    }

    public void DeleteExam(Exam model) {
        examRepository.deleteExam(model.name, model.date,model.time);
    }


    public Exam UpdateExam(Exam model) {
        return examRepository.save(model);
    }

    public void isBelongTo(Object value, Long id) {
         examRepository.isBelongTo(value,id);
    }
    
    public void deleteDepartmentRelationship(Long id)
    {
        examRepository.deleteDeparmentRelationship(id);
    }

   /* public void isBelongToUpdate(Long b, Long id) 
    {
        examRepository.isBelongToUpdate(b, id);
    }*/

    public String getExamDepartment(Long id) {
        return examRepository.getExamDepartment(id);
    }

    public List<Exam> getExamOfDepartment(Long id) {
        return examRepository.getExamOfDepartment(id);
    }
    
    public Exam getExamFromId(Long id)
    {
        return examRepository.getExamFromId(id);
    }

    public Exam getApp(Long studentId, Long examId){
        return examRepository.getApp(studentId, examId);
    }
    
    public void isToughtBy(Long teacherId, Long examId) {
        examRepository.isToughtBy(teacherId, examId);
    }

    public void isType(Long examId, Long typeId) {
        examRepository.isType(examId, typeId);
    }

    public void deleteTypeRelationship(Long id) {
        examRepository.deleteTypeRelationship(id);
    }

    public String getExamType(Long id) {
        return examRepository.getExamType(id);
    }

    public void deleteTeacherRelationship(Long id) {
        examRepository.deleteTeacherRelationship(id);
    }

    public Teacher getTeacher(Long sinavId) {
        return examRepository.getTeacher(sinavId);
    }

    public Long isApply(Long studentId, Long examId) {
        return examRepository.isApply(studentId, examId);
    }

    public Iterable<Map<String, String>>  getApplies(Long id) {
        return examRepository.getApplies(id);
    }

    public void deleteApplyRelationship(Long id) {
        examRepository.deleteApplyRelationship(id);
    }
    
    
    public void required(Long did, Long id) {
        examRepository.required(did, id);
    }

    public ArrayList<Long> getRequiredDocumentForExam(Long sinavId) {
        return examRepository.getRequiredDocumentForExam(sinavId);
    }

    public void loadFor(Long sid, Long did, String dname) {
        examRepository.loadFor(sid, did, dname);
    }

    public void setProperty(Long id, String info) {
        examRepository.setProperty(id, info);
    }

    public void requiredInformation(Long iid, Long id) {
        examRepository.requiredInformation(iid, id);
    }

    public void enterFor(Long sid, Long iid, String ivalue) {
        examRepository.enterFor(sid, iid, ivalue);
    }

    public void deleteRequired(Long id) {
        examRepository.deleteRequired(id);
    }

    public void deleteRequiredInformation(Long id) {
        examRepository.deleteRequiredInformation(id);
    }

    public void madeIn(Long eid, Long cid) {
        examRepository.madeIn(eid, cid);
    }

    public ArrayList<Classroom> getClassroomByExamId(Long sinavId) {
        return examRepository.getClassroomByExamId(sinavId);
    }

    public ArrayList<Information> getInformationByExamId(Long infoId) {
        return examRepository.getInformationByExamId(infoId);
    }

    public ArrayList<String> getInformationNameByExamId(Long infoId) {
        return examRepository.getInformationNameByExamId(infoId);
    }

    public ArrayList<String> getDocumentNameByExamId(Long docId) {
        return examRepository.getDocumentNameByExamId(docId);
    }

    public ArrayList<Document> getDocumentByExamId(Long docId) {
       return examRepository.getDocumentByExamId(docId);
    }

    public void deleteMadeInRelationship(Long id) {
        examRepository.deleteMadeInRelationship(id); 
    }

    public ArrayList<Exam> getExamsName(Long id) {
        return examRepository.getExamsName(id);
    }

    public Iterable<Map<String, String>>  getApplicantStudents(Long id, Long sinavId) {
        return examRepository.getApplicantStudents(id, sinavId);
    }

    public List<Integer>  getApplicantStudentsId(Long id, Long sinavId) {
        return examRepository.getApplicantStudentsId(id, sinavId);
    }
    
    public Iterable<Map<String, Integer>> getAppliesID(Long id) {
        return examRepository.getAppliesID(id);
    }

    public List<Exam> getPastExams(Long id) {
        return examRepository.getPastExams(id);
    }

    public List<Student> getStudentsOfPastExam(Long examId) {
        return examRepository.getStudentsOfPastExam(examId);
    }

    public int addMarksOfExam(Long examId, Long studentId, Integer mark, String tarih) {
        return examRepository.addMarksOfExam(examId, studentId, mark, tarih);
    }

    public int getStudentsMarkOfExam(Long examId, Long studentId) {
        return examRepository.getStudentsMarkOfExam(examId, studentId);
    }

//    public List<Information> getInformationOfStudent(Long studentId) {
//        return examRepository.getInformationOfStudent(studentId);
//    }

    public Iterable<Map<String, String>> getReferencedExamsOfStudent(Long sId) {
        return examRepository.getReferencedExamsOfStudent(sId);
    }

    public Iterable<Map<String, String>> getResultsOfExamForStudent(Long sId) {
        return examRepository.getResultsOfExamForStudent(sId);
    }

    public String getLoadFor(Long sId, Long dId) {
        return examRepository.getLoadFor(sId, dId);
    }

    public String getEnterFor(Long sId, Long fId) {
        return examRepository.getEnterFor(sId, fId);
    }

    public Iterable<Map<String, String>> getInformationOfStudentForExam(Long studentId, Long examId) {
        return examRepository.getInformationOfStudentForExam(studentId, examId);
    }
    
    public Iterable<Map<String, String>> getDocumentOfStudentForExam(Long studentId, Long examId){
        return examRepository.getDocumentOfStudentForExam(studentId, examId);
    }

    public void deleteIsApply(Long studentId, Long examId) {
        examRepository.deleteIsApply(studentId, examId);
    }

    public Long getIsApply(Long studentId, Long examId) {
        return examRepository.getIsApply(studentId, examId);
    }

    public Long isDelete(Long studentId, Long examId) {
        return examRepository.isDelete(studentId, examId);
    }

    public List<Exam> getIsDelete(Long sId) {
        return examRepository.getIsDelete(sId);
    }

    public void deleteIsDelete(Long sId, Long eId) {
        examRepository.deleteIsDelete(sId, eId);
    }
    
    public Long getOneIsDelete(Long sId, Long eId){
        return examRepository.getOneIsDelete(sId, eId);
    }
    
    public void updateLoadFor(Long id, Long get, String get0) {
         examRepository.updateLoadFor(id, id, get0);
    }

    public void updateEnterFor(Long id, Long id0, String value) {
        examRepository.updateEnterFor(id,id0,value);
    }

    public void doFor(Long bId, Long eId) {
        examRepository.doFor(bId, eId);
    }

    public void addCriterion(Long eId, Long eFId, String value) {
        examRepository.addCriterion(eId, eFId, value);
    }
    
     public ArrayList<Exam> findExamsforapp(Long id) {
        
       return examRepository.findExamsforapp(id);
     
    }

    public String getTypeForExam(Long id) {
        return examRepository.getTypeForExam(id);
    }

    public String getClasroom(Long id) {
        return examRepository.getClasroom(id);
    }
   
}
