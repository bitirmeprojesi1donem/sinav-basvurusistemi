/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Teacher;
import com.example.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hatice
 */

@Service
@Transactional
public class TeacherService {
    @Autowired 
    TeacherRepository teacherRepository;

    public Teacher getTeacher(String TC, String pass) {
        return teacherRepository.getTeacher(TC, pass);
    }
    
    
}
