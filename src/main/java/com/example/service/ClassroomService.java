/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Classroom;
import com.example.repository.ClassroomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hatice
 */
@Service
@Transactional
public class ClassroomService {
    @Autowired
    ClassroomRepository crepository;

    public Classroom createClassroom(Classroom classR) {
        return crepository.save(classR);
    } 

    public List<Classroom> getAllClassrooms() {
        return crepository.getAllClassrooms();
    }
    
}
