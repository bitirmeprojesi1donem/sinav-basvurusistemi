 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;     

import com.example.model.Application;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.Student;
import com.example.service.ApplicationService;
import com.example.service.ExamService;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;




public class MyUpload extends HorizontalLayout {
    final Embedded image = new Embedded("Uploaded Image");
    int sayac=0;
    Button basvuru = new Button("Basvuruyu Tamamla");
    Button b = new Button("Siradaki Dosya");
    VerticalLayout dosyaLayout = new VerticalLayout();
    VerticalLayout bilgiLayout = new VerticalLayout();
    VerticalLayout basvuruButonLayout = new VerticalLayout();
    
    ArrayList<String> fileList = new ArrayList<String>();
    ArrayList<String> gecicifileList = new ArrayList<String>();
    ArrayList<Long> geciciliste = new ArrayList<Long>();
    ArrayList<TextField> tfield = new ArrayList<TextField>();
    
    String file;
    // UI
    OptionGroup  rb = new OptionGroup();
    Upload upload;
    Label label1 = new Label();
    Label label2 = new Label();
    Label label3 = new Label();
    
    // Logical
    private FileOutputStream fOutStream;
    private File fileUploadFolder;
    private File fileUpload;
    //private User user;

    Student student;
    Application app;
    ExamService examService;
    ApplicationService appService;
    ArrayList<Document> liste = new ArrayList<Document>();
    ArrayList<Information> zorunluBilgiler = new ArrayList<Information>();
    
    
    public MyUpload(Student _student, Application _app, ApplicationService _appService, ArrayList<Document> _liste, ArrayList<Information> _zorunluBilgi) {
        student = _student;
        app = _app;
        appService = _appService;
        liste = _liste;
        zorunluBilgiler = _zorunluBilgi;
        
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        buildUpload();
    }

    public void gerekliBilgiler(){
        TextField t;
         
        Label bilgiismi;
        for (int i = 0; i < zorunluBilgiler.size(); i++) {
            bilgiismi = new Label(zorunluBilgiler.get(i).getName()+" Bilgisini Yükleyiniz");
            t = new TextField();
            
            t.setRequired(false);
            t.setRequiredError("Gerekli Bilgiyi Girmeyi Unutmayın!");
            bilgiismi.setStyleName("bilgiismi");
            t.setStyleName("txt-basvuru");
            tfield.add(t);
            
            bilgiLayout.addComponent(bilgiismi);
            bilgiLayout.addComponent(t);
//            if(examService.getEnterFor(student.getId(),zorunluBilgiler.get(i).getId()) != null)
//            {
//                bilgiismi = new Label(zorunluBilgiler.get(i).getName()+" Bilgisini Yükleyiniz");
//                t = new TextField();
//                
//                t.setValue(examService.getEnterFor(student.getId(),zorunluBilgiler.get(i).getId()));
//                System.out.println("dindin");
//            }
//            else
//            {
//                bilgiismi = new Label(zorunluBilgiler.get(i).getName()+" Bilgisini Yükleyiniz");
//                t = new TextField();
//            }
            
        }
    }
    
    private void buildUpload() {
        MyReceiver myReceiver = new MyReceiver();
        fOutStream = null;
        upload = new Upload();
        upload.setButtonCaption("Dosya Seç");
        upload.setSizeFull();
        upload.setImmediate(true);
        upload.setReceiver(myReceiver);
        upload.addSucceededListener(myReceiver);
        basvuru.setStyleName("btn-disable-basvuru");
      
      
        try
        {
            if(zorunluBilgiler.size() != 0 && liste.size() == 0)
            {
                label1.setStyleName("labelU");
                bilgiLayout.addComponent(label1);
                bilgiLayout.setStyleName("bilgiLayout");                                  
                gerekliBilgiler(); 
                 basvuru.setStyleName("basvuruStyle");
                addComponent(dosyaLayout);
                addComponent(bilgiLayout);
                addComponent(basvuruButonLayout);
               
            }
            else if(liste.size() != 0 && zorunluBilgiler.size() == 0)
            {
                label2.setValue(liste.get(0).getName()+" Dosyanızı Yükleyiniz");
                label2.setStyleName("labelU2");
                dosyaLayout.addComponent(label2);
                dosyaLayout.setStyleName("dosyaLayout");
                dosyaLayout.addComponent(upload);
                
                addComponent(dosyaLayout);
                addComponent(bilgiLayout);
            }
             
            else if(liste.size() !=0 && zorunluBilgiler.size() !=0)
             {               
                //belge
              
                label2.setValue(liste.get(0).getName()+" Dosyanızı Yükleyiniz");
                label2.setStyleName("labelU2");
                dosyaLayout.addComponent(label2);
                dosyaLayout.setStyleName("dosyaLayout");
                dosyaLayout.addComponent(upload);
             
                addComponent(dosyaLayout);

                //bilgi 
                 
                label1.setStyleName("labelU");
                bilgiLayout.addComponent(label1);
                bilgiLayout.setStyleName("bilgiLayout");                                  
                gerekliBilgiler();              
                addComponent(bilgiLayout);
            }
            
            else if(liste.size() == 0 && zorunluBilgiler.size() == 0)
            {
                label3.setValue("İstenilen belge veya bilgi bulunmamaktadır.");
                label3.setStyleName("l3Yok");
                bilgiLayout.addComponent(label3);
                addComponent(dosyaLayout);
                addComponent(bilgiLayout);
                basvuru.setStyleName("basvuruStyle");
                addComponent(basvuruButonLayout);
            }
    
        }
       
        catch(IndexOutOfBoundsException e){ }
       
         
        basvuruButonLayout.addComponent(basvuru);
        basvuruButonLayout.setStyleName("basvuruButonLayout");
        
        addComponent(basvuruButonLayout);

        basvuru.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                sayac = 0;
                for (int i = 0; i < tfield.size(); i++) {
                    if(tfield.get(i).getValue() == ""){
                        tfield.get(i).setRequired(true);
                    }
                    if(tfield.get(i).getValue() != "")
                    {
                       sayac= sayac+1;
                    }
                }
                try
                {
                //     System.out.println(appService.isApply(student.getId(), app.getId())+"plly id syisin");
                //  a  System.out.println(app.getId()+"id sayisi");
                    if(zorunluBilgiler.size() != 0 && liste.size() == 0)
                    {
                        if(sayac != tfield.size())
                        {
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bilgi Girmeyi Unutmayiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 

                             notif.setDelayMsec(2000);
                             notif.setPosition(Position.TOP_CENTER);
                             notif.setStyleName("errorValidation");
                             notif.setHtmlContentAllowed(true); 

                             notif.show(Page.getCurrent());
                        }
                        else if(appService.getApp(student.getId(), app.getId()) == app){
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sinava daha önceden basvuru yaptiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 
                            for (int i = 0; i < tfield.size(); i++) {
                                System.out.println(tfield.get(i).getValue());
                            }
                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                           notif.show(Page.getCurrent());
                        }
                        else if(appService.isApply(student.getId(), app.getId()).equals(app.getId())){

                            for (int i = 0; i < tfield.size(); i++) {
                                if(appService.getEnterFor(student.getId(), zorunluBilgiler.get(i).getId()) == null){

                                    appService.enterFor(student.getId(), zorunluBilgiler.get(i).getId(), tfield.get(i).getValue());
                                }
                                else
                                {
                                    appService.updateEnterFor(student.getId(), zorunluBilgiler.get(i).getId(), tfield.get(i).getValue());
                                }
                                System.out.println(tfield.get(i).getValue());
                            }


                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>Basvuru isleminiz gerçeklesmistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("examValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                        else{
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Isleminiz Gerçeklesememistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                    }
                    else if(liste.size() != 0 && zorunluBilgiler.size() == 0)
                    {
                        if(file == null){
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Dosya Yüklemeyi Unutmayiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }

                        else if(appService.getApp(student.getId(), app.getId()) == app){
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sinava daha önceden basvuru yaptiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                        else if(appService.isApply(student.getId(), app.getId()).equals(app.getId())){
                            for(int i=0; i<gecicifileList.size(); i++){
                                if(appService.getLoadFor(student.getId(), geciciliste.get(i)) == null){ 
                                    appService.loadFor(student.getId(), geciciliste.get(i), gecicifileList.get(i));
                                }
                                else
                                {
                                    appService.updateLoadFor(student.getId(), geciciliste.get(i), gecicifileList.get(i));
                                }
                            }
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>Basvuru isleminiz gerçeklesmistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("examValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                        else{
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Isleminiz Gerçeklesememistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                    }

                    else if(liste.size() !=0 && zorunluBilgiler.size() !=0)
                    {
                        
                        System.out.println("sayac: "+sayac);
                        if(file == null){
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Dosya Yüklemeyi Unutmayiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }

                        else if(sayac != tfield.size())
                        {
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bilgi Girmeyi Unutmayiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }

                        else if(appService.getApp(student.getId(), app.getId()) == app){
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sinava daha önceden basvuru yaptiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 
                            for (int i = 0; i < tfield.size(); i++) {
                               System.out.println(tfield.get(i).getValue());
                            }
                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                        else if(appService.isApply(student.getId(), app.getId()).equals(app.getId())){
                            for(int i=0; i<gecicifileList.size(); i++){
                                if(appService.getLoadFor(student.getId(), geciciliste.get(i)) == null){ 
                                    appService.loadFor(student.getId(), geciciliste.get(i), gecicifileList.get(i));
                                }
                                else
                                {
                                    appService.updateLoadFor(student.getId(), geciciliste.get(i), gecicifileList.get(i));
                                }
                            }

                            for (int i = 0; i < tfield.size(); i++) {
                                if(appService.getEnterFor(student.getId(), zorunluBilgiler.get(i).getId()) == null){

                                   appService.enterFor(student.getId(), zorunluBilgiler.get(i).getId(), tfield.get(i).getValue());
                                }
                                else
                                {
                                   appService.updateEnterFor(student.getId(), zorunluBilgiler.get(i).getId(), tfield.get(i).getValue());
                                }
                                System.out.println(tfield.get(i).getValue());
                            }

                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>Basvuru isleminiz gerçeklesmistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("examValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                        else{
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Isleminiz Gerçeklesememistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                    }

                    else if(liste.size() == 0 && zorunluBilgiler.size() == 0)
                    {
                        label3.setValue("İstenilen belge veya bilgi bulunmamaktadır.");
                        label3.setStyleName("l3Yok");
                        bilgiLayout.addComponent(label3);
                        addComponent(dosyaLayout);
                        addComponent(bilgiLayout);
                        basvuru.setStyleName("basvuruStyle");
                        addComponent(basvuruButonLayout);
                        
                        if(appService.getApp(student.getId(), app.getId()).equals(app)){
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sinava daha önceden basvuru yaptiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 
                            for (int i = 0; i < tfield.size(); i++) {
                               System.out.println(tfield.get(i).getValue());
                            }
                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                        else if(appService.isApply(student.getId(), app.getId()).equals(app.getId())){
                            
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>Basvuru isleminiz gerçeklesmistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("examValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                        else{
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Isleminiz Gerçeklesememistir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(2000);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                    }
                }

                catch(IndexOutOfBoundsException e)
                {}
                
              
//                
//                if(file == null){
//                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Dosya Yüklemeyi Unutmayiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 
//
//                    notif.setDelayMsec(2000);
//                    notif.setPosition(Position.TOP_CENTER);
//                    notif.setStyleName("errorValidation");
//                    notif.setHtmlContentAllowed(true); 
//
//                    notif.show(Page.getCurrent());
//                }
//                
//                else if(examService.getApp(student.getId(), exam.getId()) == exam){
//                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sinava daha önceden basvuru yaptiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 
//                   for (int i = 0; i < tfield.size(); i++) {
//                        System.out.println(tfield.get(i).getValue());
//                    }
//                    notif.setDelayMsec(2000);
//                    notif.setPosition(Position.TOP_CENTER);
//                    notif.setStyleName("errorValidation");
//                    notif.setHtmlContentAllowed(true); 
//
//                    notif.show(Page.getCurrent());
//                }
//                else if(examService.isApply(student.getId(), exam.getId()) == exam.getId()){
//                    for(int i=0; i<gecicifileList.size(); i++){
//                        if(examService.getLoadFor(student.getId(), geciciliste.get(i)) == null){
//                            examService.loadFor(student.getId(), geciciliste.get(i), gecicifileList.get(i));
//                        }
//                    }
//                    for (int i = 0; i < tfield.size(); i++) {
//                        if(examService.getEnterFor(student.getId(), zorunluBilgiler.get(i).getId()) == null){
//                            examService.enterFor(student.getId(), zorunluBilgiler.get(i).getId(), tfield.get(i).getValue());
//                        }
//                        System.out.println(tfield.get(i).getValue());
//                    }
//                    
//                    
//                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>Basvuru isleminiz gerçeklesmistir!</b>", Notification.Type.WARNING_MESSAGE, true); 
//
//                    notif.setDelayMsec(2000);
//                    notif.setPosition(Position.TOP_CENTER);
//                    notif.setStyleName("examValidation");
//                    notif.setHtmlContentAllowed(true); 
//
//                    notif.show(Page.getCurrent());
//                }
//                else{
//                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Isleminiz Gerçeklesememistir!</b>", Notification.Type.WARNING_MESSAGE, true); 
//
//                    notif.setDelayMsec(2000);
//                    notif.setPosition(Position.TOP_CENTER);
//                    notif.setStyleName("errorValidation");
//                    notif.setHtmlContentAllowed(true); 
//
//                    notif.show(Page.getCurrent());
//                }
            }
            
        });
    }

    
   
    
    class MyReceiver implements Upload.Receiver, Upload.SucceededListener {
        
        @Override
        public OutputStream receiveUpload(String filename, String mimeType) {
            fOutStream = null;
            
            String[] mime = mimeType.split("/");
            
//            System.out.println(mime[1]);
            if(mime[1].equals("pdf")){
                String dosya = filename;
                filename = liste.get(0).getName()+"-"+student.getId()+"."+mime[1];
                file = filename;
                
                fileList.add(file);
               
                
                fileUploadFolder = new File("./uploads/" + filename);   
                //fileUploadFolder = new File(".\\uploads\\" + filename);   
                label1.setValue(dosya);
                
            }
            else{
                Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Sadece pdf yükleyebilirsiniz!</b>", Notification.Type.WARNING_MESSAGE, true); 

                notif.setDelayMsec(2000);
                notif.setPosition(Position.TOP_CENTER);
                notif.setStyleName("errorValidation");
                notif.setHtmlContentAllowed(true); 

                notif.show(Page.getCurrent());
                fileUploadFolder = new File("./hatali_dosyalar/"+filename);
            }
            try {
                fOutStream = new FileOutputStream(fileUploadFolder);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MyUpload.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
            return fOutStream; 
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent event) {
            if(fileList.size() == liste.size()){
                upload.setStyleName("btn-disable");
                basvuru.setStyleName("basvuruStyle");
                gecicifileList.add(fileList.get(0));
                geciciliste.add(liste.get(0).getId());
                System.out.println("yeter");
                
            }
            else if(fileList.size() < liste.size()){
                basvuru.setStyleName("btn-disable-basvuru");
                System.out.println("az oldu");
                gecicifileList.add(fileList.get(0));
                geciciliste.add(liste.get(0).getId());
                fileList.remove(0);
                liste.remove(0);
                label2.setValue(liste.get(0).getName()+" Dosyanızı Yükleyin!");
            }                 
        }
    }
    
}
