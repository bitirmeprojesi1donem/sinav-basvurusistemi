/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.StatikSinifVeFonksiyonlar;
import com.example.model.InformationOfApps;
import com.example.model.Student;
import com.example.service.ApplicationService;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hatice
 */
@DesignRoot
public class ReferencedExams extends Panel{
    ExamService examService;
    Grid grid = new Grid();
    
    List<InformationOfApps> sinavlar = new ArrayList<InformationOfApps>();
    
    String[] gosterilecekAlanlar = {"examName", "teacher", "type", "departmentName", "examTime", "examDate" };
    String[] alanBasliklari = {"Adı", "Öğretmeni", "Türü", "Bölümü", "Zamanı", "Tarihi"};
    
    public ReferencedExams(ApplicationService appService, ExamService _examService, Student student){
        Design.read(this);
        
        setStyleName("ReferencedExamsPanel");
        examService = _examService;
        grid.setStyleName("referencedGrid");
        appService.getReferencedExamsOfStudent(student.getId()).forEach((b)->{
                    InformationOfApps a = new InformationOfApps();
                    a.examName = b.get("e.name");
                    a.departmentName = b.get("d.name");
                    a.basvuruBaslama = b.get("e.startTime");
                    a.basvuruBitis = b.get("e.endTime");
                    a.teacher = b.get("t.name")+" "+b.get("t.surname");
                    a.examId = String.valueOf(b.get("ID(e)"));
                    sinavlar.add(a);
                }
        );
        
        final BeanItemContainer<InformationOfApps> examBeans = new BeanItemContainer<>(InformationOfApps.class);
        examBeans.addAll(sinavlar);

        grid.setContainerDataSource(examBeans);
        grid.removeAllColumns();
        grid.addColumn("examName");
        grid.addColumn("teacher");
        grid.addColumn("type");
        grid.addColumn("departmentName");
        grid.addColumn("examTime");
        grid.addColumn("examDate");
        grid.addColumn("examId").setHidden(true);
        
       
        
        grid.getColumn("examName").setHeaderCaption("Adı");
        grid.getColumn("teacher").setHeaderCaption("Öğretmeni");
        grid.getColumn("type").setHeaderCaption("Türü");
        grid.getColumn("departmentName").setHeaderCaption("Bölümü");
        grid.getColumn("examTime").setHeaderCaption("Zamanı");
        grid.getColumn("examDate").setHeaderCaption("Tarihi");
        
        grid.addSelectionListener(selectionEvent -> {
            final Window info = new Window();
            info.setModal(true);
            info.setStyleName("ilkwindow");
            info.setPositionX(100);
            info.setPositionY(10);
            info.setWidth(1100.0f, Unit.PIXELS);
            info.setHeight(645.0f, Unit.PIXELS);
            info.setDraggable(false);
            info.setResizable(false);
            info.setVisible(false);
            
            final VerticalLayout layoutInfo = new VerticalLayout();
            
            info.setContent(layoutInfo);
            
            UI.getCurrent().addWindow(info);
            
            Object selected = ((Grid.SingleSelectionModel) grid.getSelectionModel()).getSelectedRow();
            
            if(selected == null){
                info.setVisible(false);
                
                StatikSinifVeFonksiyonlar.refreshGridWithHeaders(sinavlar, InformationOfApps.class, grid, gosterilecekAlanlar, alanBasliklari);
                return;
            }
            
            layoutInfo.removeAllComponents();
            layoutInfo.addComponent(new InfoApplicationsWindow(selected, examService, appService));
            
            StatikSinifVeFonksiyonlar.refreshGridWithHeaders(sinavlar, InformationOfApps.class, grid, gosterilecekAlanlar, alanBasliklari);
            info.setVisible(true);
        });
        
        grid.setImmediate(true);
        
        setContent(grid);
    }
}
