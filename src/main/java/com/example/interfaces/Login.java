/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.Teacher;
import com.example.service.ExamService;
import com.example.service.StudentService;
import com.example.service.TeacherService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author hatice
 */

@DesignRoot
public class Login extends VerticalLayout{
    Label loginLabel, loginVerticalLabel, TCLabel, parolaLabel;
    VerticalLayout loginPanel, loginComu;
    Panel inputPanel;
    public  TextField tc;
    public  PasswordField parola;
    public  Button loginButton;
    public  String TC, pass;
    
    
    Teacher teacher;
    
    public Login(){
        
        Design.read(this);
        
        setStyleName("loginVertical");
        
        tc.setRequired(false);
        tc.setRequiredError("Bu alan boş bırakılamaz!");
        parola.setRequired(false);
        parola.setRequiredError("Bu alan boş bırakılamaz!");

        loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
    }
}
