/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.Application;
import com.example.model.Classroom;
import com.example.model.Department;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.InformationOfApps;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hatice
 */

@DesignRoot
public class DeleteExamsWindow extends VerticalLayout{
    Label label41, label42, label48, label47, label40, label50;
    Label lBolum40, lSinavAdi42, lbasvuruBaslama47, lbasvuruBitis48, lsinavBilgi41, lsinavBelge50;
    VerticalLayout ekleme1, ekleme2, ekleme3, ekleme4;
    Button sil;
    HorizontalLayout hEkleme;

    ArrayList<Information> infos = new ArrayList<Information>();
    ArrayList<Document> docs = new ArrayList<Document>();
    
    Application exam = new Application();
    InformationOfApps ioe = new InformationOfApps();
    
    ExamService examService;

    String info = "";
    String  doc = "";
    
    @Autowired
    Department dep;
    public DeleteExamsWindow(ApplicationService appService, Object selected, ExamService _examService, Teacher teach){
        Design.read(this);
        
        ioe = (InformationOfApps) selected;
        exam.id = Long.parseLong(ioe.getExamId());
        exam.name = ioe.getExamName();
        exam.startTime = ioe.getBasvuruBaslama();
        exam.endTime = ioe.getBasvuruBitis();
         
        examService = _examService;

        lBolum40.setValue(appService.getApplicationDepartment(exam.getId()));
        lSinavAdi42.setValue(exam.name);
        lbasvuruBaslama47.setValue(exam.getStartTime());
        lbasvuruBitis48.setValue(exam.getEndTime());
       
        infos = appService.getInformationByAppId(exam.getId());
        for (int i = 0; i < infos.size(); i++) {
            if(i == infos.size()-1){
                info += infos.get(i).getName();
            }
            else{
                info += infos.get(i).getName()+", ";
            } 
        }
        if(info.isEmpty()){
            lsinavBilgi41.setValue("Bulunmamaktadır");
        }
        else{
            lsinavBilgi41.setValue(info);
        }
        
        docs = appService.getDocumentByAppId(exam.getId());
        for (int i = 0; i < docs.size(); i++) {
            if(i == docs.size()-1){
                doc += docs.get(i).getName();
            }
            else{
                doc += docs.get(i).getName()+", ";
            } 
        }
        if(doc.isEmpty()){
            lsinavBelge50.setValue("Bulunmamaktadır");
        }
        else{
            lsinavBelge50.setValue(doc);
        }
        
        hEkleme.addComponent(ekleme1);
        hEkleme.addComponent(ekleme2);
        hEkleme.addComponent(ekleme4);
        this.addComponent(hEkleme);
        this.addComponent(ekleme3);
        
        sil.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
               /*
                *******************************************
               madeIn İLİŞKİSİ SİLİNECEK
                
                **********************************                
                */

               appService.deleteDepartmentRelationship(exam.getId());
               appService.deleteTeacherRelationship(exam.getId());
               appService.deleteApplyRelationship(exam.getId());
               appService.deleteRequired(exam.getId());
               appService.deleteRequiredInformation(exam.getId());
               appService.DeleteExam(exam);                                        
              
               if(appService.getOne(exam) != exam){
                    for (int i = 0; i<2; i++){
                        Iterator<Window> w = getUI().getWindows().iterator();
                        getUI().removeWindow(w.next()); 
                    }

                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>İşleminiz Başarıyla Gerçekleşmiştir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                    notif.setDelayMsec(200);
                    notif.setPosition(Position.TOP_CENTER);
                    notif.setStyleName("examValidation");
                    notif.setHtmlContentAllowed(true); 

                    notif.show(Page.getCurrent());
               }
               else{
                   for (Iterator<Window> w = getUI().getWindows().iterator(); w.hasNext();)
                        getUI().removeWindow(w.next());
                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>İşleminiz Gerçekleşememiştir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                    notif.setDelayMsec(200);
                    notif.setPosition(Position.TOP_CENTER);
                    notif.setStyleName("examValidation");
                    notif.setHtmlContentAllowed(true); 

                    notif.show(Page.getCurrent());
               }
            }
        });
    }
}
