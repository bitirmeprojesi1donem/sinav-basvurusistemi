/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.StatikSinifVeFonksiyonlar;
import com.example.model.Exam;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.util.List;

/**
 *
 * @author hatice
 */
@DesignRoot
public class InfoExam extends VerticalLayout{
    Label label40, label42, lSinavAdi42, label412, lSinavAdi412, label47, lbasvuruBaslama47, label48, lbasvuruBitis48, label444, lkriter44, lBolum40;
    Button sil;
    
   
    String derslikİsimleri = "";
    public InfoExam(List<Exam> examList, Window window10, Object selected){
        Design.read(this);
        
        
        Exam exam = (Exam) selected;
        
        for(int k = 0; k < exam.classliste.size(); k++){
            if(k == exam.classliste.size()-1){
                derslikİsimleri += exam.classliste.get(k).getName();
            }
            else{
                derslikİsimleri += exam.classliste.get(k).getName()+", ";
            } 
        }
        
        lSinavAdi42.setValue(exam.getName());
        lSinavAdi412.setValue(exam.examType.getName());
        lBolum40.setValue(derslikİsimleri);
        lbasvuruBaslama47.setValue(exam.getTime());
        lbasvuruBitis48.setValue(exam.getDate());
        lkriter44.setValue(Integer.toString(exam.getPersonCount()));
        
        sil.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                System.out.println(exam.getId());
                examList.remove(exam);
                
                window10.getUI().removeWindow(window10);
                
                String[] gosterilecekAlanlar = {"name", "id"};
                String[] alanBasliklari = {"Sınav Adı"};

                StatikSinifVeFonksiyonlar.refreshGridWithHeaders(examList, Exam.class, addApplicationsWindow.sinavlarGrid, gosterilecekAlanlar, alanBasliklari);
            }
        });
    }
}
