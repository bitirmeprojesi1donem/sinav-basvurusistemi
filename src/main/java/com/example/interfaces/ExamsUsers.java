/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.StatikSinifVeFonksiyonlar;
import com.example.model.ApplyAndExam;
import com.example.model.Exam;
import com.example.model.Teacher;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.util.ArrayList;
import java.util.List;
import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

/**
 *
 * @author hatice
 */
@DesignRoot
public class ExamsUsers extends Panel{
    ComboBox sinavSecimi;
    Grid grid = new Grid();
    
    List<ApplyAndExam> basvuranlar = new ArrayList<ApplyAndExam>();
    List<Integer> sId = new ArrayList<Integer>();
    int i,j = 0; 
    public ExamsUsers(ExamService examService, Teacher teach){
        Design.read(this);
        setStyleName("eUsersPanel");
        
        sinavSecimi.setNullSelectionAllowed(false);
        
        VerticalLayout layout = new VerticalLayout();
        layout.setStyleName("examuserLayout");
        grid.setStyleName("gridExamsUsers");
        
        ArrayList<Exam> examsNameList = examService.getExamsName(teach.getId());
        BeanItemContainer<Exam> examl = new BeanItemContainer<>(Exam.class);
        examl.addAll(examsNameList);

        sinavSecimi.setContainerDataSource(examl);
        sinavSecimi.setItemCaptionPropertyId("name");
        
        if(sinavSecimi.getValue() == null){
            
            examService.getApplies(teach.getId()).forEach(
                (b)->{
                    ApplyAndExam a = new ApplyAndExam();
                    a.studentName = b.get("s.name");
                    a.studentSurname= b.get("s.surname");
                    a.examName = b.get("e.name");
                    basvuranlar.add(a);
                }
            );
            
            j=0;
            examService.getAppliesID(teach.getId()).forEach(
                (c)->{
                    basvuranlar.get(j).studentId = Long.valueOf(c.get("ID(s)"));
                    basvuranlar.get(j).examId = Long.valueOf(c.get("ID(e)"));
                    j++;
                }
            );
            
            String[] gosterilecekAlanlar = {"studentName", "studentSurname", "examName"};
            String[] alanBasliklari = {"Öğrenci Adı", "Öğrenci Soyadı", "Ders Adı"};

            StatikSinifVeFonksiyonlar.refreshGridWithHeaders(basvuranlar, ApplyAndExam.class, grid, gosterilecekAlanlar, alanBasliklari);

            grid.setImmediate(true);
            for (int i = 0; i < basvuranlar.size(); i++) {
                basvuranlar.clear();
            }
            
        }
        
        sinavSecimi.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Exam sinav = (Exam) sinavSecimi.getValue();
 
                for (int i = 0; i < basvuranlar.size(); i++) {
                    basvuranlar.clear();
                }
               
                examService.getApplicantStudents(teach.getId(), sinav.getId()).forEach(
                    (b)->{
                        ApplyAndExam a = new ApplyAndExam();
                        a.studentName = b.get("s.name");
                        a.studentSurname= b.get("s.surname");
                        a.examName = b.get("e.name");
                        a.examId = sinav.getId();
                        basvuranlar.add(a);
                    }
                );

                sId = examService.getApplicantStudentsId(teach.getId(), sinav.getId());
                for (int k = 0; k < sId.size(); k++) {
                    basvuranlar.get(k).studentId = Long.valueOf(sId.get(k));
                }
        
                String[] gosterilecekAlanlar = {"studentName", "studentSurname", "examName"};
                String[] alanBasliklari = {"Öğrenci Adı", "Öğrenci Soyadı", "Ders Adı"};
                
                StatikSinifVeFonksiyonlar.refreshGridWithHeaders(basvuranlar, ApplyAndExam.class, grid, gosterilecekAlanlar, alanBasliklari);

                grid.setImmediate(true);
            }
        });  
        
        grid.addSelectionListener(selectionEvent -> {
            final Window ogrenciBasvuruBilgi = new Window("Sınava Başvuran Öğrenci Bilgileri");
            ogrenciBasvuruBilgi.setModal(true);
            ogrenciBasvuruBilgi.setStyleName("ilkwindow");
            ogrenciBasvuruBilgi.setPositionX(400);
            ogrenciBasvuruBilgi.setPositionY(70);
            ogrenciBasvuruBilgi.setWidth(600.0f, Unit.PIXELS);
            ogrenciBasvuruBilgi.setHeight(450.0f, Unit.PIXELS);
            ogrenciBasvuruBilgi.setDraggable(false);
            ogrenciBasvuruBilgi.setResizable(false);
            ogrenciBasvuruBilgi.setVisible(false);
            
            UI.getCurrent().addWindow(ogrenciBasvuruBilgi);
            
            Object selected = ((Grid.SingleSelectionModel) grid.getSelectionModel()).getSelectedRow();
            
            if(selected == null){
                ogrenciBasvuruBilgi.setVisible(false);
                return;
            }

            ogrenciBasvuruBilgi.setContent(new StudentInformationForApply(selected, examService));           

            ogrenciBasvuruBilgi.setVisible(true);
        });
        
        layout.addComponent(sinavSecimi);
        layout.addComponent(grid);
        setContent(layout);
    }
    
    
}
