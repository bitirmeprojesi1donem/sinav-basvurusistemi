/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.ApplyAndExam;
import com.example.model.Information;
import com.example.model.Teacher;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

/**
 *
 * @author hatice
 */
@DesignRoot
public class StudentInformationForApply extends VerticalLayout{
    VerticalLayout gerekliBVLayout, gerekliDVLayout;
    Label sinav, basvuran, gerekliB, examLabel, studentLabel, gerekliD;
    Button iptal;
    
    ExamService examService;
    ApplyAndExam apply = new ApplyAndExam();
    
    public StudentInformationForApply(Object selected, ExamService _examService){
        Design.read(this);
        examService = _examService;
        apply = (ApplyAndExam) selected;
        
        examLabel.setValue(apply.getExamName());
        studentLabel.setValue(apply.getStudentName()+" "+apply.getStudentSurname());
         
        int infoCount = newArrayList(examService.getInformationOfStudentForExam(apply.getStudentId(), apply.getExamId())).size();
        
        if(infoCount == 0){
            HorizontalLayout gerekliBLayout = new HorizontalLayout();
            Label a = new Label("Gerekli Bilgi Bulunmamakta!");
            a.setStyleName("aStyle");
            gerekliBLayout.addComponent(a);
            gerekliBVLayout.addComponent(gerekliBLayout);
        }
        else{
            examService.getInformationOfStudentForExam(apply.getStudentId(), apply.getExamId()).forEach(
                (a)->{
                    HorizontalLayout gerekliBLayout = new HorizontalLayout();
                    gerekliBLayout.setStyleName("gerekliBLayout");
                    Label info = new Label(a.get("i.name")+" : ");
                    Label i = new Label();
                    i.setStyleName("iStyle");
                    i.setIcon(new ThemeResource("images/circle.png"));
                    info.setStyleName("info");
                    Label value = new Label(a.get("ef.value"));
                    value.setStyleName("value");
                    
                    gerekliBLayout.addComponent(i);
                    gerekliBLayout.addComponent(info);
                    gerekliBLayout.addComponent(value);
                    
                    gerekliBVLayout.addComponent(gerekliBLayout);
                }
            );
        }
        
        int docCount = newArrayList(examService.getDocumentOfStudentForExam(apply.getStudentId(), apply.getExamId())).size();
        
        if(docCount == 0){
            HorizontalLayout gerekliDLayout = new HorizontalLayout();
            Label a = new Label("Gerekli Belge Bu Sınav İçin İstenmemiştir!");
            a.setStyleName("aStyle");
            gerekliDLayout.addComponent(a);
            gerekliDVLayout.addComponent(gerekliDLayout);
        }
        else{
            examService.getDocumentOfStudentForExam(apply.getStudentId(), apply.getExamId()).forEach(
                (a)->{
                    HorizontalLayout gerekliDLayout = new HorizontalLayout();
                    gerekliDLayout.setStyleName("gerekliDLayout");
                    Label doc = new Label(a.get("d.name")+" : ");
                    Label i = new Label();
                    i.setStyleName("iStyle");
                    i.setIcon(new ThemeResource("images/circleMor.png"));
                    doc.setStyleName("info");
                    
                    Button button = new Button();
                    button.setStyleName("butonBelgeAc");
                    
                    gerekliDLayout.addComponent(i);
                    gerekliDLayout.addComponent(doc);
                    gerekliDLayout.addComponent(button);
                    
                    gerekliDVLayout.addComponent(gerekliDLayout);
                    
                    button.addClickListener(new Button.ClickListener() {
                        @Override
                        public void buttonClick(Button.ClickEvent event) {
                            Window window = new Window();
                            window.setModal(true);
                            window.setResizable(false);
                            window.setDraggable(false);
                            window.setCaption(apply.getStudentName()+" "+apply.getStudentSurname()+" "+apply.getExamName()+" için "+a.get("d.name"));
                            window.setWidth("800");
                            window.setHeight("600");
                            window.center();
                            
                            StreamSource s = new StreamResource.StreamSource() {

                                @Override
                                public InputStream getStream() {
                                    try {
                                        File f = new File("uploads/"+a.get("l.file"));
                                        FileInputStream fis = new FileInputStream(f);
                                        return fis;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        return null;
                                    }
                                }
                            };

                            StreamResource r = new StreamResource(s, a.get("l.file"));
                            Embedded e = new Embedded();
                            e.setSizeFull();
                            e.setType(Embedded.TYPE_BROWSER);
                            r.setMIMEType("application/pdf");

                            e.setSource(r);
                            window.setContent(e);
                            UI.getCurrent().addWindow(window);
                        }
                    });
                }
            );
        }
        
        iptal.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                examService.deleteIsApply(apply.getStudentId(), apply.getExamId());
                Long rId = examService.isDelete(apply.getStudentId(), apply.getExamId());
                
                if(examService.getIsApply(apply.getStudentId(), apply.getExamId()) == null && rId != null){
                    for (int i = 0; i<1; i++){
                        Iterator<Window> w = getUI().getWindows().iterator();
                        getUI().removeWindow(w.next()); 
                    }

                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>İşleminiz Başarıyla Gerçekleşmiştir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                    notif.setDelayMsec(300);
                    notif.setPosition(Position.TOP_CENTER);
                    notif.setStyleName("examValidation");
                    notif.setHtmlContentAllowed(true); 

                    notif.show(Page.getCurrent());
                    Page.getCurrent().reload();
                }
                else{
                    for (Iterator<Window> w = getUI().getWindows().iterator(); w.hasNext();)
                        getUI().removeWindow(w.next());
                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>İşlem Gerçekleşirken Bir Hata Oluştu!</b>", Notification.Type.WARNING_MESSAGE, true); 

                    notif.setDelayMsec(300);
                    notif.setPosition(Position.TOP_CENTER);
                    notif.setStyleName("examValidation");
                    notif.setHtmlContentAllowed(true); 

                    notif.show(Page.getCurrent());
                }
            }
        });
    }
}
