/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.StatikSinifVeFonksiyonlar;
import com.example.model.Application;
import com.example.model.Classroom;
import com.example.model.Criterion;
import com.example.model.Department;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.ClassroomService;
import com.example.service.CriterionService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.example.service.TypeService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import static java.lang.Math.toIntExact;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author hatice
 */


@DesignRoot
public class addApplicationsWindow extends VerticalLayout implements View{
    VerticalLayout ekleme1, ekleme2, ekleme4, ekleme5;
    ComboBox bolum; 
    TextField sinavAdi, sinavBelge, sinavBilgi;
    Button kaydet, belgeEkle, bilgiEkle, kriterEkle, sinavEkle, sonKriterEkle;
    DateField basvuruBaslama, basvuruBitis;  
    Label label1, label2, label7, label8, lBasvuruBaslama, lBasvuruBitis;
    HorizontalLayout hEkleme;
    
    ArrayList<String> liste = new ArrayList<String>();

    List<Document> dliste = new ArrayList<Document>();
    List<Information> iliste = new ArrayList<Information>();
    List<TextField> tfield = new ArrayList<TextField>();
    List<Exam> examList = new ArrayList<Exam>();
    ArrayList<String> bilgiListe = new ArrayList<String>();
    List<Criterion> kriterList = new ArrayList<Criterion>();
    List<String> kriterFormul = new ArrayList<String>();
    
    BeanItemContainer<Document> dBean = new BeanItemContainer<>(Document.class);
    BeanItemContainer<Information> iBean = new BeanItemContainer<>(Information.class);
    BeanItemContainer<Classroom> classBean = new BeanItemContainer<>(Classroom.class);
    BeanItemContainer<Criterion> kBean = new BeanItemContainer<>(Criterion.class);
    
    public Grid belgeler = new Grid();
    public Grid bilgilerGrid = new Grid();
    public static Grid sinavlarGrid = new Grid();
    public Grid kriterlerGrid = new Grid();
    
    Application exam = new Application();

    ExamService examService;
    DocumentService documentService;
    InformationService informationService;
    ClassroomService cService;
    
    Long b, typeId;
    int  sayac = 0;
    Button kkButon = new Button("Kriteri Kaydet");
    VerticalLayout kriterLayout = new VerticalLayout();
    
    boolean addExam = true;
    int kacinciWindow = 0;
    int kacinciSinavWindowu = 0;
    
    int toplama = 0;
    int sinavSayac = 0;
    public addApplicationsWindow(CriterionService criterionService, ApplicationService appService, ExamService _examService,DepartmentService dep, TypeService typeService, Teacher teach, DocumentService dservice, InformationService _informationService, ClassroomService _cservice){
        Design.read(this);
        examList.clear();
        examService = _examService;
        documentService = dservice;
        informationService = _informationService;
        cService = _cservice;
        
        sinavlarGrid.setStyleName("basvuruSinavlarGrid");
        sinavlarGrid.setVisible(false);
        kriterlerGrid.setStyleName("basvuruKriterlerGrid");
        kriterlerGrid.setVisible(false);
        belgeler.setVisible(false);
        bilgilerGrid.setVisible(false);
        
        bilgilerGrid.setStyleName("bilgilerGrid");
        belgeler.addStyleName("belgeler");
        bolum.setNullSelectionAllowed(false);
        
        bolum.setInputPrompt("Bölüm Seçilmedi!");

        
        List<Department> depList = dep.getAllDepartments();
        BeanItemContainer<Department> depl = new BeanItemContainer<>(Department.class);
        depl.addAll(depList);
        
        bolum.setContainerDataSource(depl);
        bolum.setItemCaptionPropertyId("name");  //BOLUMUN ISIMLERINI LISTELETMEK ICIN
        bolum.setRequired(false);
        bolum.setRequiredError("Bölüm Seçimini Unutmayın!");
        
        sinavEkle.setEnabled(false);
        
        List<Classroom> cList = cService.getAllClassrooms();
        BeanItemContainer<Classroom> cBean = new BeanItemContainer<>(Classroom.class);
        cBean.addAll(cList);

        if(sinavBilgi.getValue() == "")
        {
            bilgiEkle.setEnabled(false);
        }
        else
        {
            bilgiEkle.setEnabled(true);
        }
        
        
        if(sinavBelge.getValue() == "")
        {
            belgeEkle.setEnabled(false);
        }
        else
        {
            belgeEkle.setEnabled(true);
        }

        kriterEkle.setEnabled(false);
        
        sinavlarGrid.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) sinavlarGrid.getSelectionModel()).getSelectedRow();
            Exam e = (Exam) selected;
            
            if(selected == null){
                return;
            }
            else{
                System.out.println("sinavlar selected: "+e.getId());
                final Window window10 = new Window(" Sınavının Bilgileri");
                window10.setModal(true);
                
                window10.setStyleName("ilkwindow");
                window10.setWidth("400");
                window10.setHeight("400");

                window10.setDraggable(false);
                window10.setResizable(false);
                for (int i = 0; i < examList.size(); i++) {
                    System.out.println("examList:"+examList.get(i).getName());
                }
                window10.setContent(new InfoExam(examList, window10, selected));

                UI.getCurrent().addWindow(window10);
            }
        });
        
        bilgilerGrid.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) bilgilerGrid.getSelectionModel()).getSelectedRow();
            sayac = sayac -1;
            System.out.println(sayac);
            if(sayac != 0) kriterEkle.setEnabled(true);
            else kriterEkle.setEnabled(false);
            
            if(!kriterEkle.isEnabled())
            {
                sinavEkle.setEnabled(false);
            }
            
            Information c = (Information) selected;
            
            for (int i = 0; i < iliste.size(); i++) {
                if(iliste.get(i).equals(selected)){
                    bilgiListe.remove(c.getName());
                    iliste.remove(selected);
                    if(kacinciWindow > 0) tfield.remove(i);
                }
            }
 
            iBean.removeAllItems();
            iBean.addAll(iliste);
            bilgilerGrid.setContainerDataSource(iBean);
            bilgilerGrid.removeAllColumns();
            bilgilerGrid.addColumn("name");
            bilgilerGrid.getColumn("name").setHeaderCaption("Bilgi İsmi");
            bilgilerGrid.setImmediate(true);
        });
        
        belgeler.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) belgeler.getSelectionModel()).getSelectedRow();
            
            Document c = (Document) selected;
            
            for (int i = 0; i < dliste.size(); i++) {
                if(dliste.get(i).equals(selected)){
                    liste.remove(c.getName());
                    dliste.remove(c);
                }
            }

            dBean.removeAllItems();
            dBean.addAll(dliste);
            belgeler.setContainerDataSource(dBean);
            belgeler.removeAllColumns();
            belgeler.addColumn("name");
            belgeler.getColumn("name").setHeaderCaption("Belge İsmi");
            belgeler.setImmediate(true);
        });
       
        sinavAdi.setRequired(false);
        sinavAdi.setRequiredError("Başvuru Adı Boş Bırakılamaz!");
      
        basvuruBaslama.setRequired(false);
        basvuruBaslama.setRequiredError("Tarih Seçimi Boş Bırakılamaz!");
        basvuruBitis.setRequired(false);
        basvuruBitis.setRequiredError("Tarih Seçimi Boş Bırakılamaz!");  

        basvuruBaslama.setDateFormat("dd-MM-yyyy");
        basvuruBitis.setDateFormat("dd-MM-yyyy");

        bolum.addValueChangeListener(new ValueChangeListener()
        {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Item item = bolum.getItem(bolum.getValue());
               String selectedDepartment = item.getItemProperty("id").toString();

               for (int i = 0; i < depList.size(); i++) {
                   if (depList.get(i).getId().toString().equals(selectedDepartment)) { //
                       b = Long.parseLong(selectedDepartment);
                   }
               }
            }
        });

        sinavBilgi.addTextChangeListener(new TextChangeListener() {
            @Override
            public void textChange(FieldEvents.TextChangeEvent event) {
                 String bilgiTxt = event.getText();
                 String baskaTxt = "";
                 System.out.println(bilgiTxt);
                if(baskaTxt.equals(bilgiTxt))
                {
                    
                    System.out.println(bilgiTxt+"nulla geldi");
                    bilgiEkle.setEnabled(false);

                }
                else
                {
                   System.out.println(bilgiTxt+"nulla dusmedi");                   
                   bilgiEkle.setEnabled(true);
                    
                }
            }
        });
        
         sinavBelge.addTextChangeListener(new TextChangeListener() {
            @Override
            public void textChange(FieldEvents.TextChangeEvent event) {
                 String belgeTxt = event.getText();
                 String baskaTxt = "";
                
                if(baskaTxt.equals(belgeTxt))
                {                                      
                    belgeEkle.setEnabled(false);
                }
                else
                {                 
                 belgeEkle.setEnabled(true);
                    
                }
            }
        });
        
        
        bilgiEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String bilgi = sinavBilgi.getValue();
                System.out.println(bilgi);
                
                Information iveri = new Information();
                iveri.name = sinavBilgi.getValue();
                
                if( bilgi == null)
                {                   
                    bilgiEkle.setEnabled(false);                   
                }
                else
                {                    
                    bilgiListe.add(bilgi);
                    sayac= sayac+1;
                    iliste.add(iveri); 
                    if(sayac != 0) kriterEkle.setEnabled(true);
                    else kriterEkle.setEnabled(false);
                }             
                iBean.removeAllItems();
                iBean.addAll(iliste);
                
                bilgilerGrid.setContainerDataSource(iBean);
                bilgilerGrid.removeAllColumns();
                bilgilerGrid.addColumn("name");
                bilgilerGrid.getColumn("name").setHeaderCaption("Bilgi İsmi");
                bilgilerGrid.setImmediate(true);
               bilgilerGrid.setVisible(true);
            }
        
        }); 

        belgeEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String veri = sinavBelge.getValue();
                
                Document dveri = new Document();
                dveri.name = sinavBelge.getValue();
                
                if( veri == null)
                {
                   
                    belgeEkle.setEnabled(false);                   
                }
                else
                {
                    liste.add(veri);
                    dliste.add(dveri);
                    
                }

                dBean.removeAllItems();
                dBean.addAll(dliste);
                
                belgeler.setContainerDataSource(dBean);
                belgeler.removeAllColumns();
                belgeler.addColumn("name");
                belgeler.getColumn("name").setHeaderCaption("Belge İsmi");
                belgeler.setImmediate(true);
                belgeler.setVisible(true);
            }
        }
        );

        ekleme1.addComponent(sinavlarGrid);
        hEkleme.addComponent(ekleme1);
        hEkleme.addComponent(ekleme2);
        ekleme4.addComponent(bilgilerGrid);
        ekleme4.addComponent(belgeler);
        ekleme4.addComponent(kriterlerGrid);
        hEkleme.addComponent(ekleme4);
        this.addComponent(hEkleme);
        
        kriterEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                tfield.clear();
                kriterLayout.removeAllComponents();
                kacinciWindow++;

                Window window = new Window("Başvuruya Kriter Ekleme");
                window.setModal(true);
                
                window.setStyleName("ilkwindow");
                window.setWidth("400");
                window.setHeight("400");
                
                window.setDraggable(false);
                window.setResizable(false);
                
                if(kriterList.isEmpty()){
                    for (int i = 0; i < bilgiListe.size(); i++) {
                        HorizontalLayout h = new HorizontalLayout();
                        Label a = new Label("->");
                        a.setStyleName("appALabel");
                        Label bilgi = new Label(bilgiListe.get(i)+" bilgisinin");
                        bilgi.setStyleName("appBilgiLabel");

                        Label y = new Label("%");
                        y.setStyleName("appYLabel");

                        TextField t = new TextField();
                        t.setRequired(false);
                        t.setRequiredError("toplam 100e eşit olmalı");
                        t.setStyleName("kriterBilgiTextfield");
                        tfield.add(t);
                        
                        h.addComponent(a);
                        h.addComponent(bilgi);
                        h.addComponent(y);
                        h.addComponent(t);
                        kriterLayout.addComponent(h);
                    }
                    HorizontalLayout h2 = new HorizontalLayout();
                    Label toplam = new Label("Ortalaması");
                    toplam.setStyleName("appToplamLabel");

                    TextField t2 = new TextField();
                    tfield.add(t2);
                    t2.setStyleName("kriterBilgiTextfield");

                    Label b = new Label("den büyük olsun.");
                    b.setStyleName("appBLabel");

                    h2.addComponent(toplam);
                    h2.addComponent(t2);
                    h2.addComponent(b);

                    kriterLayout.addComponent(h2);
                }
                else if(kriterList.size() > 0){
                    for (int i = 0; i < bilgiListe.size(); i++) {
                        HorizontalLayout h = new HorizontalLayout();
                        Label a = new Label("->");
                        a.setStyleName("appALabel");
                        Label bilgi = new Label(bilgiListe.get(i)+" bilgisinin");
                        bilgi.setStyleName("appBilgiLabel");

                        Label y = new Label("%");
                        y.setStyleName("appYLabel");

                        TextField t = new TextField();
                        t.setRequired(false);
                        t.setRequiredError("toplam 100e eşit olmalı");
                        t.setStyleName("kriterBilgiTextfield");
                        tfield.add(t);

                        h.addComponent(a);
                        h.addComponent(bilgi);
                        h.addComponent(y);
                        h.addComponent(t);
                        kriterLayout.addComponent(h);
                    }
                    HorizontalLayout h2 = new HorizontalLayout();
                    Label toplam = new Label("Ortalaması");
                    toplam.setStyleName("appToplamLabel");

                    TextField t2 = new TextField();
                    tfield.add(t2);
                    t2.setStyleName("kriterBilgiTextfield");

                    Label b = new Label("den büyük olsun.");
                    b.setStyleName("appBLabel");

                    h2.addComponent(toplam);
                    h2.addComponent(t2);
                    h2.addComponent(b);

                    kriterLayout.addComponent(h2);
                    
                    for (int i = 0; i < kriterList.size(); i++) {
                        for (int j = 0; j < kriterList.get(i).infoList.size(); j++) {
                            for (int k = 0; k < iliste.size(); k++) {
                                if(kriterList.get(i).infoList.get(j).equals(iliste.get(k))){
                                    tfield.get(k).setEnabled(false);
                                }
                            }
                        }
                    }
                }

                kkButon.setStyleName("kkButon");
                              
                kriterLayout.addComponent(kkButon);
                window.setContent(kriterLayout);

                UI.getCurrent().addWindow(window);
                
                kkButon.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        Criterion kriter = new Criterion();
                        String formul = "";
                        for (int i = 0; i < tfield.size()-1; i++) 
                        {
                            if(!tfield.get(i).getValue().isEmpty()){
                                System.out.println("i şöyle: "+i);
                                kriter.percentList.add(Integer.parseInt(tfield.get(i).getValue()));
                                kriter.infoList.add(iliste.get(i));
                            }
                            //toplama = Integer.parseInt(tfield.get(i).getValue()) + toplama;
                   
                        }

                        for (int i = 0; i < kriter.percentList.size(); i++) {
                            if(i == kriter.percentList.size()-1) formul += kriter.infoList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)";
                            else formul +=  kriter.infoList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)+";
                        }
                        
                        kriter.averagePercent = Integer.parseInt(tfield.get(tfield.size()-1).getValue());
                        
                        formul = "("+formul+")/"+kriter.percentList.size()+" > "+kriter.averagePercent;
                        kriter.formul = formul;

                        kriter.averagePercent = Integer.parseInt(tfield.get(tfield.size()-1).getValue());

                        boolean esitmi = false;
                        for (int i = 0; i < kriterList.size(); i++) {
                            if(kriterList.get(i).equals(kriter)) esitmi = true; 
                        }
                        if(esitmi == false) kriterList.add(kriter);

                        for (int i = 0; i < tfield.size()-1; i++) {
                            if(toplama > 100 || toplama <100)
                            {
                                tfield.get(i).setRequired(true);
                            }
                            
                        }
                        UI.getCurrent().removeWindow(window);
                        
                        kriterlerGrid.setVisible(true);
                        String[] gosterilecekAlanlar = {"formul", "id"};
                        String[] alanBasliklari = {"Kriter Formülü"};

                        StatikSinifVeFonksiyonlar.refreshGridWithHeaders(kriterList, Criterion.class, kriterlerGrid, gosterilecekAlanlar, alanBasliklari);
                    }
                });
                 
                for (int i = 0; i < tfield.size(); i++)
                {
                    if(tfield.isEmpty())
                    {
                        sinavSayac = sinavSayac - 1;
                    }
                    else
                    {
                        sinavSayac = sinavSayac +1;
                    }    
                }

                if(sinavSayac == 0)
                {
                    sinavEkle.setEnabled(false);
                }
                else 
                {
                    sinavEkle.setEnabled(true);
                }


            }
        });
       
        sinavEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                kacinciSinavWindowu++;
                
                final Window window2 = new Window("Başvuru İçin Sınav Ekle");
                window2.setModal(true);

                window2.setStyleName("ilkwindow");
                window2.setWidth("1200");
                window2.setHeight("500");

                window2.setDraggable(false);
                window2.setResizable(false);

                window2.setContent(new AddExamsWindow(kacinciSinavWindowu, window2, examList, iliste, tfield, examService, typeService, teach, cService));

                UI.getCurrent().addWindow(window2);
            }
        });
        
        sonKriterEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final Window window5 = new Window("Başvuru İçin Son Kriter Ekle");
                window5.setModal(true);

                window5.setStyleName("ilkwindow");
                window5.setWidth("600");
                window5.setHeight("500");

                window5.setDraggable(false);
                window5.setResizable(false);

                window5.setContent(new addFinalCriterion(window5, iliste, examList, exam));

                UI.getCurrent().addWindow(window5);
            }
        });
        
        kaydet.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if(bolum.getValue() == null ) bolum.setRequired(true);           
                else if(sinavAdi.getValue() == "") sinavAdi.setRequired(true);
                else if(basvuruBaslama.getValue() == null) basvuruBaslama.setRequired(true);
                else if(basvuruBitis.getValue() == null) basvuruBitis.setRequired(true);
                else{
                    exam.name = sinavAdi.getValue();
                    SimpleDateFormat sdf = new SimpleDateFormat();
                    sdf.applyPattern("dd-MM-yyyy");
                    exam.startTime = sdf.format(basvuruBaslama.getValue());
                    exam.endTime = sdf.format(basvuruBitis.getValue());
                    
                    if(appService.control(exam) == true){
                        Application a = appService.CreateApplication(exam);

                        appService.isBelongTo(b, a.getId());
                        appService.isToughtBy(teach.getId(), a.getId());

                        for(int i = 0; i< bilgiListe.size(); i++){
                            Long varmi = informationService.getInformation(bilgiListe.get(i));
                            
                            if(varmi == null){
                                Long iid =  informationService.addNeedInformation(bilgiListe.get(i));
                                //if(tfield.size() > 0){
                                    appService.requiredInformation(iid, exam.getId());
                                    //appService.requiredInformationWithCriterion(iid, exam.getId(), tfield.get(i).getValue());
                                //}
                                //else if(tfield.size() == 0) appService.requiredInformation(iid, exam.getId());
                            }
                            else{
                                //if(tfield.size() > 0){
                                    appService.requiredInformation(varmi, exam.getId());
                                //    appService.requiredInformationWithCriterion(varmi, exam.getId(), tfield.get(i).getValue());
                                //}
                                //else if(tfield.size() == 0) appService.requiredInformation(varmi, exam.getId());
                            }
                        }
                        
                        for(int i = 0; i< liste.size(); i++){
                            Long varmi = documentService.getDocument(liste.get(i));
                            
                            if(varmi == null){
                                Long did =  documentService.addNeedDocument(liste.get(i));
                                appService.required(did, exam.getId());
                            }
                            else{
                                appService.required(varmi, exam.getId());
                            }
                        }
                        
                        if(a != null){
                            if(examList.isEmpty() && !kriterList.isEmpty()){
                                for (int i = 0; i < kriterList.size(); i++) {
                                    Criterion k = criterionService.createCriterion(kriterList.get(i));
                                    appService.createCriterionRelation(a.getId(), k.getId());
                                    System.out.println("infolist: "+kriterList.get(i).infoList.size());
                                    for (int j = 0; j < kriterList.get(i).infoList.size(); j++) {
                                        appService.createPercentRelation(k.getId(), kriterList.get(j).infoList.get(j).getId(), kriterList.get(j).percentList.get(j));
                                    }
                                }
                            }
                            else if(!examList.isEmpty()){
                                for (int i = 0; i < examList.size(); i++) {
                                    Exam e = examService.CreateExam(examList.get(i));
                                    examService.doFor(a.getId(), e.getId());
                                    if(i == 0 && !kriterList.isEmpty()){
                                        for (int j = 0; j < kriterList.size(); j++) {
                                            Criterion k = criterionService.createCriterion(kriterList.get(j));
                                            appService.createCriterionRelation(e.getId(), k.getId());
                                            for (int l = 0; l < kriterList.get(j).infoList.size(); l++) {
                                                appService.createPercentRelation(k.getId(), kriterList.get(l).infoList.get(l).getId(), kriterList.get(l).percentList.get(l));
                                            }
                                        }
                                    }
                                    else if( i > 0 && !e.examKriterList.isEmpty()){
                                        for (int j = 0; j < e.examKriterList.size(); j++) {
                                            Criterion k = criterionService.createCriterion(e.examKriterList.get(j));
                                            for (int l = 0; l < e.examKriterList.get(j).percentList.size(); l++) {
                                                if(l <= e.examKriterList.get(l).examList.size()-1){
                                                    appService.createPercentRelation(k.getId(), e.examKriterList.get(l).examList.get(l).getId(), e.examKriterList.get(l).percentList.get(l));
                                                }
                                                else if(l > e.examKriterList.get(l).examList.size()-1){
                                                    appService.createPercentRelation(k.getId(), e.examKriterList.get(l).infoList.get(l-e.examKriterList.get(l).examList.size()-1).getId(), e.examKriterList.get(l).percentList.get(l));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            Criterion k = criterionService.createCriterion(exam.sonKriter);
                            appService.createFinalCriterion(a.getId(), k.getId());
                            for (int l = 0; l < exam.sonKriter.percentList.size(); l++) {
                                if(l <= exam.sonKriter.examList.size()-1){
                                    appService.createPercentRelation(k.getId(), exam.sonKriter.examList.get(l).getId(), exam.sonKriter.percentList.get(l));
                                }
                                else if(l > exam.sonKriter.examList.size()-1){
                                    appService.createPercentRelation(k.getId(), exam.sonKriter.infoList.get(l-exam.sonKriter.examList.size()-1).getId(), exam.sonKriter.percentList.get(l));
                                }
                            }
                            
                            for (int i = 0; i<1; i++){
                               Iterator<Window> w = getUI().getWindows().iterator();
                                getUI().removeWindow(w.next()); 
                            }

                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>İşleminiz Başarıyla Gerçekleşmiştir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(200);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("examValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                    }
                    else{
                        for (int i = 0; i<2; i++){
                            Iterator<Window> w = getUI().getWindows().iterator();
                            getUI().removeWindow(w.next()); 
                        }
                        
                        Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Daha Önce Kaydedilmiş Bir Sınav!</b>", Notification.Type.WARNING_MESSAGE, true); 

                        notif.setDelayMsec(300);
                        notif.setPosition(Position.TOP_CENTER);
                        notif.setStyleName("errorValidation");
                        notif.setHtmlContentAllowed(true); 

                        notif.show(Page.getCurrent());
                    }
                }
            }
        });
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
