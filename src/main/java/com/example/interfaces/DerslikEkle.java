/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.Classroom;
import com.example.model.Officer;
import com.example.model.Teacher;
import com.example.service.ClassroomService;
import com.example.service.OfficerService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.util.Iterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;

/**
 *
 * @author hatice
 */
@DesignRoot
public class DerslikEkle extends VerticalLayout{
    Label lDerslikAdi, lDerslikKontenjan;
    TextField derslikAdi, derslikKontenjan;
    Button kaydet;
    Officer officer;
    
    @Autowired
    OfficerService oService;
    
    @Autowired
    ClassroomService cService;
    
    Classroom classR;
    Classroom classRo;
    public DerslikEkle(OfficerService _oService, ClassroomService _cService){
        Design.read(this);
        oService = _oService;        
        cService = _cService;
        
        derslikAdi.setStyleName("derslikAdi");
        lDerslikAdi.setStyleName("lDerslikAdi");
        derslikKontenjan.setStyleName("derslikKontenjan");
        lDerslikKontenjan.setStyleName("lDerslikKontenjan");
        
        officer = (Officer) getUI().getCurrent().getSession().getAttribute("userRole");
        
        derslikAdi.setRequired(false);
        derslikAdi.setRequiredError("Derslik Adını Yazmayı Unutmayınız!");
        
        derslikKontenjan.setRequired(false);
        derslikKontenjan.setRequiredError("Dersliğin Aldığı Kişi Sayısını Girmeyi Unutmayınız!");
        
        kaydet.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String dAdi = derslikAdi.getValue();
                String dkontenjan = derslikKontenjan.getValue();
                try{
                    if(dAdi == null) derslikAdi.setRequired(true);
                    else if(dkontenjan == null) derslikKontenjan.setRequired(true);
                    else{
                        classR = new Classroom();
                        classR.name = dAdi;
                        classR.quota = Integer.parseInt(dkontenjan);
                        classRo = cService.createClassroom(classR);
                        if(classR != null){
                            Long did = oService.getDepartmentOfOfficer(officer.getId());
                            Long rid = oService.has(did, classRo.getId());
                            if(rid != null){
                                for (int i = 0; i<1; i++){
                                   Iterator<Window> w = getUI().getWindows().iterator();
                                    getUI().removeWindow(w.next()); 
                                }

                                Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>Derslik Ekleme İşleminiz Başarıyla Gerçekleşmiştir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                                notif.setDelayMsec(200);
                                notif.setPosition(Position.TOP_CENTER);
                                notif.setStyleName("examValidation");
                                notif.setHtmlContentAllowed(true); 

                                notif.show(Page.getCurrent());
                            }
                            else{
                                for (int i = 0; i<1; i++){
                                    Iterator<Window> w = getUI().getWindows().iterator();
                                    getUI().removeWindow(w.next()); 
                                }

                                Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Derslik Ekleme İşlemi Gerçekleşirken Bir Hata Oluştu!</b>", Notification.Type.WARNING_MESSAGE, true); 

                                notif.setDelayMsec(300);
                                notif.setPosition(Position.TOP_CENTER);
                                notif.setStyleName("errorValidation");
                                notif.setHtmlContentAllowed(true); 

                                notif.show(Page.getCurrent());
                            }
                        }  
                        else{
                            for (int i = 0; i<2; i++){
                                Iterator<Window> w = getUI().getWindows().iterator();
                                getUI().removeWindow(w.next()); 
                            }

                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Derslik Ekleme İşlemi Gerçekleşirken Bir Hata Oluştu!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(300);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("errorValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                    }
                }
                catch(NumberFormatException e){
                    derslikAdi.setRequired(true);
                    derslikKontenjan.setRequired(true);
                }
            }
        });
    }
}
