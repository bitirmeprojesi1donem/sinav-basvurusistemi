/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.StatikSinifVeFonksiyonlar;
import com.example.model.Application;
import com.example.model.Criterion;
import com.example.model.Exam;
import com.example.model.Information;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hatice
 */
@DesignRoot
public class addFinalCriterion extends VerticalLayout{
    VerticalLayout kriter;
    Button kkButon;
    TextField asilKontenjan, yedekKontenjan;
    Label label2, label3;
    
    List<TextField> kriterTextField = new ArrayList<TextField>();
    VerticalLayout kriterLayout = new VerticalLayout();
    
    int toplama = 0;
    public addFinalCriterion(Window window5, List<Information> infoList, List<Exam> examList, Application app){
        Design.read(this);
        kriter.addComponent(kriterLayout);
        for (int i = 0; i < examList.size(); i++) {
            HorizontalLayout h = new HorizontalLayout();
            Label a = new Label("->");
            a.setStyleName("appALabel");
            Label bilgi = new Label(examList.get(i).getName()+" sınavının");
            bilgi.setStyleName("appBilgiLabel");

            Label y = new Label("%");
            y.setStyleName("appYLabel");

            TextField t = new TextField();
            t.setRequired(false);
            t.setRequiredError("toplam 100e eşit olmalı");
            t.setStyleName("kriterBilgiTextfield");
            kriterTextField.add(t);

            h.addComponent(a);
            h.addComponent(bilgi);
            h.addComponent(y);
            h.addComponent(t);
            kriterLayout.addComponent(h);
        }
        System.out.println("bilgi liste: "+infoList.size());
        for (int i = 0; i < infoList.size(); i++) {
            HorizontalLayout h = new HorizontalLayout();
            Label a = new Label("->");
            a.setStyleName("appALabel");
            Label bilgi = new Label(infoList.get(i).getName()+" bilgisinin");
            bilgi.setStyleName("appBilgiLabel");

            Label y = new Label("%");
            y.setStyleName("appYLabel");

            TextField t = new TextField();
            t.setRequired(false);
            t.setRequiredError("toplam 100e eşit olmalı");
            t.setStyleName("kriterBilgiTextfield");
            kriterTextField.add(t);

            h.addComponent(a);
            h.addComponent(bilgi);
            h.addComponent(y);
            h.addComponent(t);
            kriterLayout.addComponent(h);
        }
        HorizontalLayout h2 = new HorizontalLayout();
        Label toplam = new Label("Ortalaması");
        toplam.setStyleName("appToplamLabel");

        TextField t2 = new TextField();
        kriterTextField.add(t2);
        t2.setStyleName("kriterBilgiTextfield");

        Label b = new Label("den büyük olsun.");
        b.setStyleName("appBLabel");

        h2.addComponent(toplam);
        h2.addComponent(t2);
        h2.addComponent(b);

        kriterLayout.addComponent(h2);
        kkButon.setStyleName("kkButon");

        kriterLayout.addComponent(kkButon);
        
        kkButon.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Criterion kriter = new Criterion();
                String formul = "";
                System.out.println("tfield size ı: "+kriterTextField.size());
                for (int i = 0; i < kriterTextField.size()-1; i++) 
                {
                    if(!kriterTextField.get(i).getValue().isEmpty()){
                        System.out.println("i şöyle: "+i);
                        kriter.percentList.add(Integer.parseInt(kriterTextField.get(i).getValue()));
                        if(i <= examList.size()-1) kriter.examList.add(examList.get(i));
                        else if(i > examList.size()-1) kriter.infoList.add(infoList.get(i-examList.size()));
                    }
                    //toplama = Integer.parseInt(tfield.get(i).getValue()) + toplama;

                }

                for (int i = 0; i < kriter.percentList.size(); i++) {
                    if(!kriter.examList.isEmpty() && kriter.infoList.isEmpty()){
                        if(i == kriter.percentList.size()-1) formul += kriter.examList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)";
                        else formul += kriter.examList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)+";
                    }
                    else if(kriter.examList.isEmpty() && !kriter.infoList.isEmpty()){
                        if(i == kriter.percentList.size()-1) formul += kriter.infoList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)";
                        else formul += kriter.infoList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)+";
                    }
                    else if(!kriter.examList.isEmpty() && !kriter.infoList.isEmpty()){
                        for (int j = 0; j < kriter.examList.size(); j++) {
                            if(j == (kriter.percentList.size()-kriter.infoList.size()-1)) formul += kriter.examList.get(j).getName()+"*("+kriter.percentList.get(j)+"/100)";
                            else formul += kriter.examList.get(j).getName()+"*("+kriter.percentList.get(j)+"/100)+";
                        }
                        for (int j = 0; j < kriter.infoList.size(); j++) {
                            formul += "+ "+kriter.infoList.get(j).getName()+"*("+kriter.percentList.get(j+examList.size())+"/100)";
                        }
                        break;
                    }

                }

                kriter.averagePercent = Integer.parseInt(kriterTextField.get(kriterTextField.size()-1).getValue());

                formul = "("+formul+")/"+kriter.percentList.size()+" > "+kriter.averagePercent;
                kriter.formul = formul;
                kriter.asilKontenjan = Integer.parseInt(asilKontenjan.getValue());
                kriter.yedekKontenjan = Integer.parseInt(yedekKontenjan.getValue());
                
                System.out.println("formul: "+formul);

                for (int i = 0; i < kriterTextField.size()-1; i++) {
                    if(toplama > 100 || toplama <100)
                    {
                        kriterTextField.get(i).setRequired(true);
                    }

                }
                UI.getCurrent().removeWindow(window5);

            }
        });
        
        this.addComponent(kriterLayout);
    }
}
