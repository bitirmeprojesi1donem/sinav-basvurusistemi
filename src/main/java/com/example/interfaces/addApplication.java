/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.StatikSinifVeFonksiyonlar;
import com.example.model.Exam;
import com.example.model.InformationOfApps;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.ClassroomService;
import com.example.service.CriterionService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.example.service.TypeService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.ValoTheme;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hatice
 */



@DesignRoot
public class addApplication extends Panel{
    protected Button yeniSinav;
    public Grid grid = new Grid();
    
    List<InformationOfApps> sinavlar = new ArrayList<InformationOfApps>();
    
     String[] gosterilecekAlanlar = {"examName",  "departmentName",  "basvuruBaslama", "basvuruBitis", };
    String[] alanBasliklari = {"Adı", "Bölümü",  "Başvuru Başlama", "Başvuru Bitiş"};
    
    public addApplication(CriterionService criterionService, ApplicationService appService, ExamService examService,DepartmentService dep,TypeService typeService, Teacher teach, DocumentService dservice, InformationService informationService, ClassroomService cService) {
        Design.read(this);
        setStyleName("addExamsPanel");
        
        VerticalLayout layout = new VerticalLayout();
        
        /***************YENİ Başvuru EKLEME KISMI****************/
        yeniSinav.addClickListener(new ClickListener() {
            //@Override
            public void buttonClick(Button.ClickEvent event) {
                final Window window = new Window("Yeni Başvuru Ekleme");
                window.setModal(true);
                //window.setPositionX(500);
                //window.setPositionY(80);
                
                window.setStyleName("ilkwindow");
                window.setWidth("1100");
                window.setHeight("600");
                
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(new addApplicationsWindow(criterionService, appService, examService,dep, typeService,teach, dservice, informationService, cService));

                UI.getCurrent().addWindow(window);
            }
        });
       /***************YENİ Başvuru EKLEME KISMI****************/
       
       /***************VAR OLAN SINAVLARIN GÖSTERİLDİĞİ KISIM****************/
        
        
        
        
        grid.setStyleName("addExamgrid");
        //grid.setSizeFull();
        
        appService.getAllApps(teach.getId()).forEach((b)->{
                    InformationOfApps a = new InformationOfApps();
                    a.examName = b.get("e.name");
                    a.departmentName = b.get("d.name");
                    a.basvuruBaslama = b.get("e.startTime");
                    a.basvuruBitis = b.get("e.endTime");
                    a.examId = String.valueOf(b.get("ID(e)"));
                    sinavlar.add(a);
                }
        );
        
        final BeanItemContainer<InformationOfApps> examBeans = new BeanItemContainer<>(InformationOfApps.class);
        examBeans.addAll(sinavlar);

        grid.setContainerDataSource(examBeans);
        grid.removeAllColumns();
        grid.addColumn("examName");
        grid.addColumn("departmentName");
        grid.addColumn("basvuruBaslama");
        grid.addColumn("basvuruBitis");
        grid.addColumn("examId").setHidden(true);
        
       
        
        grid.getColumn("examName").setHeaderCaption("Adı");
        grid.getColumn("departmentName").setHeaderCaption("Bölümü");
        grid.getColumn("basvuruBaslama").setHeaderCaption("Başvuru Başlama");
        grid.getColumn("basvuruBitis").setHeaderCaption("Başvuru Bitiş");
 
        grid.addSelectionListener(selectionEvent -> {
            try {
                final Window updateAndDelete = new Window();
                updateAndDelete.setModal(true);
                updateAndDelete.setStyleName("ilkwindow");
                updateAndDelete.setPositionX(100);
                updateAndDelete.setPositionY(10);
                updateAndDelete.setWidth(1100.0f, Unit.PIXELS);
                updateAndDelete.setHeight(645.0f, Unit.PIXELS);
                updateAndDelete.setDraggable(false);
                updateAndDelete.setResizable(false);
                //updateAndDelete.setClosable(false);
                updateAndDelete.setVisible(false);

                TabSheet sample = new TabSheet();
                sample.setHeight(95.0f, Unit.PERCENTAGE);
                final VerticalLayout layoutInfo = new VerticalLayout();
                final VerticalLayout layoutUpdate = new VerticalLayout();
                final VerticalLayout layoutDelete = new VerticalLayout();

                sample.addTab(layoutInfo, "Başvuru Bilgleri");
                sample.addTab(layoutUpdate, "Başvuru Güncelleme");
                sample.addTab(layoutDelete, "Başvuru Silme");
                updateAndDelete.setContent(sample);

                UI.getCurrent().addWindow(updateAndDelete);
                
                Object selected = ((SingleSelectionModel) grid.getSelectionModel()).getSelectedRow();
                
                if(selected == null){
                    updateAndDelete.setVisible(false);

                    StatikSinifVeFonksiyonlar.refreshGridWithHeaders(sinavlar, InformationOfApps.class, grid, gosterilecekAlanlar, alanBasliklari);
                    return;
                }
                
                layoutInfo.removeAllComponents();
                layoutInfo.addComponent(new InfoApplicationsWindow(selected, examService,appService));
                StatikSinifVeFonksiyonlar.refreshGridWithHeaders(sinavlar, InformationOfApps.class, grid, gosterilecekAlanlar, alanBasliklari);
                
                layoutUpdate.removeAllComponents();
                layoutUpdate.addComponent(new UpdateApplicationsWindow(appService, selected, examService,dep, typeService, dservice, informationService, cService, teach));
                StatikSinifVeFonksiyonlar.refreshGridWithHeaders(sinavlar, InformationOfApps.class, grid, gosterilecekAlanlar, alanBasliklari);
                
                layoutDelete.removeAllComponents();
                layoutDelete.addComponent(new DeleteExamsWindow(appService, selected, examService, teach));
                StatikSinifVeFonksiyonlar.refreshGridWithHeaders(sinavlar, InformationOfApps.class, grid, gosterilecekAlanlar, alanBasliklari);
                /*
                try {
                updateAndDelete.setContent(new UpdateApplicationsWindow(selected));
                } catch (ParseException ex) {
                Logger.getLogger(addApplication.class.getName()).log(Level.SEVERE, null, ex);
                }*/
                updateAndDelete.setVisible(true);
            } catch (ParseException ex) {
                Logger.getLogger(addApplication.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        grid.setImmediate(true);
        
       /***************VAR OLAN SINAVLARIN GÖSTERİLDİĞİ KISIM****************/
        
        
        
        layout.addComponent(yeniSinav);
        layout.addComponent(grid);
        setContent(layout);
    }
}
