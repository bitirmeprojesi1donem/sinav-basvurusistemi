/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.Application;
import com.example.model.Classroom;
import com.example.model.Department;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.InformationOfApps;
import com.example.model.Teacher;
import com.example.model.Type;
import com.example.service.ApplicationService;
import com.example.service.ClassroomService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.example.service.TypeService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import static java.lang.Math.toIntExact;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hatice
 */

@DesignRoot
public class UpdateApplicationsWindow extends VerticalLayout{
    VerticalLayout ekleme1, ekleme2, ekleme3, ekleme4,ekleme5;
    ComboBox bolum; 
    TextField sinavAdi,sinavBelge, sinavBilgi,basvuruSinavSayisi;
    Button guncelle, belgeEkle, bilgiEkle,kriterEkle;
    DateField basvuruBaslama, basvuruBitis;  
    Label label1, label2, label7, label8, lBasvuruBaslama, lBasvuruBitis;
    HorizontalLayout hEkleme;
    
    ArrayList<String> liste = new ArrayList<String>();
    ArrayList<String> bilgiListe = new ArrayList<String>();
    List<Document> dliste = new ArrayList<Document>();
    List<Information> iliste = new ArrayList<Information>();
     
    BeanItemContainer<Document> dBean = new BeanItemContainer<>(Document.class);
    BeanItemContainer<Information> iBean = new BeanItemContainer<>(Information.class);
    
    public Grid belgeler = new Grid();
    public Grid bilgilerGrid = new Grid();
   
    Application exam = new Application();

    ExamService examService;
    DocumentService documentService;
    InformationService informationService;
    ClassroomService cService;
    
    Long b, typeId;
     List<TextField> tfield = new ArrayList<TextField>();
      List<Exam> examList = new ArrayList<Exam>();
    Button kkButon = new Button("Kriteri Kaydet");
    VerticalLayout kriterLayout = new VerticalLayout();
    int  sayac = 0;
    
     final List<String> options = Arrays.asList(new String[] {"Başvuruya Sınav Eklemek İstiyorum", "Başvuruya Sınav Eklemek İstemiyorum"});
    boolean addExam = true;
    int kacinciWindow = 0;
    String secim = "Başvuruya Sınav Eklemek İstiyorum";
    InformationOfApps ioe = new InformationOfApps();
 
    Label lbb = new Label();
    public UpdateApplicationsWindow(ApplicationService appService, Object select, ExamService _examService,DepartmentService dep, TypeService typeService, DocumentService dservice, InformationService _informationService, ClassroomService _cservice, Teacher teach) throws ParseException{
        Design.read(this);
        
        examService = _examService;
        documentService = dservice;
        informationService = _informationService;
        cService = _cservice;
        
        
        
        ioe = (InformationOfApps) select;
        exam.id = Long.parseLong(ioe.getExamId());
        exam.name = ioe.getExamName();
        exam.startTime = ioe.getBasvuruBaslama();
        exam.endTime = ioe.getBasvuruBitis();
       
        
         basvuruSinavSayisi.setRequired(false);
        basvuruSinavSayisi.setRequiredError("Sınav Sayısı Boş Bırakılamaz!");
        if(secim == "Başvuruya Sınav Eklemek İstiyorum") basvuruSinavSayisi.setVisible(true);
        else basvuruSinavSayisi.setVisible(false);
        
        bilgilerGrid.setStyleName("bilgilerGrid");
        belgeler.addStyleName("belgeler");
        
        /*************************
         * BOLUM
         ***************************/
        //bolum.setInputPrompt("Bölüm Seçilmedi!");
        bolum.setNullSelectionAllowed(false);
        System.out.println(ioe.getDepartmentName());
//        bolum.setValue(ioe.getDepartmentName());
//        bolum.select(ioe.getDepartmentName());
        for (int i = 0; i < bilgiListe.size(); i++) {
            if(bilgiListe.isEmpty()) kriterEkle.setEnabled(false);
            else  kriterEkle.setEnabled(true);
        }
       
        List<Department> depList = dep.getAllDepartments();
        BeanItemContainer<Department> depl = new BeanItemContainer<>(Department.class);
        depl.addAll(depList);
        //System.out.println(depList.indexOf(examService.getExamDepartment(exam.id)));
        bolum.setContainerDataSource(depl);
        bolum.setItemCaptionPropertyId("name");  //BOLUMUN ISIMLERINI LISTELETMEK ICIN
        for(int i=0; i<depList.size();i++){
            if(depList.get(i).name.equals(appService.getApplicationDepartment(exam.id))){
                System.out.println("girdi");
                bolum.select(depList.get(i).id);
            }
        }
        //bolum.select(depList.indexOf(examService.getExamDepartment(exam.id)));
        //bolum.setValue(depList.get(depList.indexOf(examService.getExamDepartment(exam.id))));
        bolum.setContainerDataSource(depl);
   //     bolum.setValue(selected);
        bolum.setItemCaptionPropertyId("name");
        bolum.setRequired(false);
        bolum.setRequiredError("Bölüm Seçimini Unutmayın!");
        
         /*************************
         * BOLUM
         ***************************/
        
        sinavAdi.setRequired(false);
        sinavAdi.setRequiredError("Sınav Adı Boş Bırakılamaz!");
        sinavAdi.setValue(exam.name);

        List<Type> typeList = typeService.getAllTypes();
        BeanItemContainer<Type> typeBean = new BeanItemContainer<>(Type.class);
        typeBean.addAll(typeList);
        // Sınav türü comboboxı olacak!
        //sinavTuru.select(typeList.get(i).getId().toString());
        //sinavTuru.select(examService.getExamType(exam.getId()));
        
//        for(int i=0; i<typeList.size();i++){
//            if(typeList.get(i).name.equals(examService.getExamType(exam.getId()))){
//                System.out.println("aaaa"+typeList.get(i).getName());
//                sinavTuru.select(typeList.get(i).getName());
//            }
//        }
//        
        //sinavTuru.select(examService.getExamType(exam.getId()));

     
        bilgiListe = appService.getInformationNameByAppId(exam.getId());
        sayac = bilgiListe.size();
        iliste = appService.getInformationByAppId(exam.getId());
        
        iBean.removeAllItems();
        iBean.addAll(iliste);
        bilgilerGrid.setContainerDataSource(iBean);
        bilgilerGrid.removeAllColumns();
        bilgilerGrid.addColumn("name");
        bilgilerGrid.getColumn("name").setHeaderCaption("Bilgi İsmi");
        bilgilerGrid.setImmediate(true);
        ekleme4.addComponent(bilgilerGrid);
        
        
        
        dliste = appService.getDocumentByAppId(exam.getId());
        liste =  appService.getDocumentNameByAppId(exam.getId());
        
        dBean.removeAllItems();
        dBean.addAll(dliste);
        belgeler.setContainerDataSource(dBean);
        belgeler.removeAllColumns();
        belgeler.addColumn("name");
        belgeler.getColumn("name").setHeaderCaption("Belge İsmi");
        belgeler.setImmediate(true);
        ekleme4.addComponent(belgeler);

        bilgilerGrid.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) bilgilerGrid.getSelectionModel()).getSelectedRow();
             sayac = sayac -1;
            System.out.println(sayac);
            if(sayac != 0) kriterEkle.setEnabled(true);
            else kriterEkle.setEnabled(false);
            Information c = (Information) selected;
            
            for (int i = 0; i < iliste.size(); i++) {
                if(iliste.get(i).equals(selected)){
                    bilgiListe.remove(c.getName());
                    iliste.remove(selected);
                }
            }

            
            iBean.removeAllItems();
            iBean.addAll(iliste);
            bilgilerGrid.setContainerDataSource(iBean);
            bilgilerGrid.removeAllColumns();
            bilgilerGrid.addColumn("name");
            bilgilerGrid.getColumn("name").setHeaderCaption("Bilgi İsmi");
            bilgilerGrid.setImmediate(true);
        });
        
        belgeler.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) belgeler.getSelectionModel()).getSelectedRow();
            
            Document c = (Document) selected;
            
            for (int i = 0; i < dliste.size(); i++) {
                if(dliste.get(i).equals(selected)){
                    liste.remove(c.getName());
                    dliste.remove(c);
                    
                }
            }

            dBean.removeAllItems();
            dBean.addAll(dliste);
            belgeler.setContainerDataSource(dBean);
            belgeler.removeAllColumns();
            belgeler.addColumn("name");
            belgeler.getColumn("name").setHeaderCaption("Belge İsmi");
            belgeler.setImmediate(true);
        });
      
        basvuruBaslama.setRequired(false);
        basvuruBaslama.setRequiredError("Tarih Seçimi Boş Bırakılamaz!");
        basvuruBitis.setRequired(false);
        basvuruBitis.setRequiredError("Tarih Seçimi Boş Bırakılamaz!");  

        basvuruBaslama.setDateFormat("dd-MM-yyyy");
        basvuruBitis.setDateFormat("dd-MM-yyyy");

        basvuruBaslama.setDateFormat("dd-MM-yyyy"); 
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
        Date date2 = formatter2.parse(exam.startTime);
        basvuruBaslama.setValue(date2);
        
        basvuruBitis.setDateFormat("dd-MM-yyyy");
        Date date3 = formatter2.parse(exam.endTime);
        basvuruBitis.setValue(date3);
        
        bolum.addValueChangeListener(new Property.ValueChangeListener()
        {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Item item = bolum.getItem(bolum.getValue());
               String selectedDepartment = item.getItemProperty("id").toString();

               for (int i = 0; i < depList.size(); i++) {
                   if (depList.get(i).getId().toString().equals(selectedDepartment)) { //
                       b = Long.parseLong(selectedDepartment);
                   }
               }
            }
        });
     
        bilgiEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                
                
                String bilgi = sinavBilgi.getValue();
                System.out.println(bilgi+"bubu");
                
                Information iveri = new Information();
                iveri.name = sinavBilgi.getValue();
                if(bilgiListe.isEmpty() && bilgi !="")
                {
                    bilgiListe.add(bilgi);
                    System.out.println(bilgiListe.get(0)+" kontrol");

                }
                for (int i = 0; i < bilgiListe.size(); i++) {
                    System.out.println(bilgiListe.get(i)+"bububu");
                    if(bilgiListe.get(i)!=bilgi)
                    {
                        bilgiListe.add(bilgi);
                        System.out.println(bilgiListe.get(i)+" kontrol");
                        
                    }
                    
                }
                 sayac= sayac+1;
                iliste.add(iveri);
                
                if(sayac != 0) kriterEkle.setEnabled(true);
                else kriterEkle.setEnabled(false);
                  
                iBean.removeAllItems();
                iBean.addAll(iliste);
                
                bilgilerGrid.setContainerDataSource(iBean);
                bilgilerGrid.removeAllColumns();
                bilgilerGrid.addColumn("name");
                bilgilerGrid.getColumn("name").setHeaderCaption("Bilgi İsmi");
                bilgilerGrid.setImmediate(true);
                ekleme4.addComponent(bilgilerGrid);
            }
        
        }); 
        
        //final BeanItemContainer<Document> docName = new BeanItemContainer<>(Document.class);
        belgeEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                for (int i = 0; i < dliste.size(); i++) {
                    System.out.println("dliste "+dliste.get(i).getName());
                }
                String veri = sinavBelge.getValue();
                System.out.println(veri);
                
                Document dveri = new Document();
                dveri.name = sinavBelge.getValue();
                
                for (int i = 0; i < liste.size(); i++) {
                    if(liste.get(i)!=veri)
                    {
                        liste.add(veri);
                    }
                }
                
                dliste.add(dveri);
                System.out.println(liste);
               
                dBean.removeAllItems();
                dBean.addAll(dliste);
                
                belgeler.setContainerDataSource(dBean);
                belgeler.removeAllColumns();
                belgeler.addColumn("name");
                belgeler.getColumn("name").setHeaderCaption("Belge İsmi");
                belgeler.setImmediate(true);
                ekleme4.addComponent(belgeler);               
            }
        }
        );
        
         final OptionGroup group = new OptionGroup("", options);
        group.setNullSelectionAllowed(false);
        group.select("Başvuruya Sınav Eklemek İstiyorum");
        
        group.addValueChangeListener( ( Property.ValueChangeEvent event ) -> {

            if(event.getProperty().getValue() == "Başvuruya Sınav Eklemek İstemiyorum"){
                addExam = false;
                basvuruSinavSayisi.setVisible(false);
            }
            else if(event.getProperty().getValue() == "Başvuruya Sınav Eklemek İstiyorum"){
                addExam = true;
                 basvuruSinavSayisi.setVisible(true);
            }
            
        } );
        
        ekleme5.addComponent(group);
        
        
        kriterEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                kriterLayout.removeAllComponents();
                for (int i = 0; i < bilgiListe.size(); i++) {
                    System.out.println(bilgiListe.get(i)+"--------");
                }
                final Window window = new Window("Başvuruya Kriter Ekleme");
                window.setModal(true);
                
                window.setStyleName("ilkwindow");
                window.setWidth("400");
                window.setHeight("400");
                
                window.setDraggable(false);
                window.setResizable(false);
                
                for (int i = 0; i < bilgiListe.size(); i++) {
                    HorizontalLayout h = new HorizontalLayout();
                    Label a = new Label("->");
                    a.setStyleName("appALabel");
                    Label bilgi = new Label(bilgiListe.get(i)+" bilgisinin");
                    bilgi.setStyleName("appBilgiLabel");
                    
                    Label y = new Label("%");
                    y.setStyleName("appYLabel");
                    
                    TextField t = new TextField();
                    t.setStyleName("kriterBilgiTextfield");
                    if(appService.getRequiredInfo(exam.getId(),iliste.get(i).getId()) != null)
                    {
                        t.setValue(appService.getRequiredInfo(exam.getId(),iliste.get(i).getId()));
                    }
                    //eger requieredInfo vr ise hzir gelsin 
                    tfield.add(t);
                    
                    h.addComponent(a);
                    h.addComponent(bilgi);
                    h.addComponent(y);
                    h.addComponent(t);
                    kriterLayout.addComponent(h);
                }
                HorizontalLayout h2 = new HorizontalLayout();
                Label toplam = new Label("Toplamı");
                toplam.setStyleName("appToplamLabel");
                
                TextField t2 = new TextField();
                tfield.add(t2);
                t2.setStyleName("kriterBilgiTextfield");
                
                Label b = new Label("den büyük olsun.");
                b.setStyleName("appBLabel");
                h2.addComponent(toplam);
                h2.addComponent(t2);
                h2.addComponent(b);
                
                kkButon.setStyleName("kkButon");
                
                kriterLayout.addComponent(h2);
                kriterLayout.addComponent(kkButon);
                window.setContent(kriterLayout);

                UI.getCurrent().addWindow(window);
                
                kkButon.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        window.getUI().removeWindow(window);
                    }
                });
            }
        });
        
        
        
        
        
        
        
        
        guncelle.addClickListener(new Button.ClickListener(){
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if(bolum.getValue() == null ) bolum.setRequired(true);           
                else if(sinavAdi.getValue() == "") sinavAdi.setRequired(true);
                else if(basvuruBaslama.getValue() == null) basvuruBaslama.setRequired(true);
                else if(basvuruBitis.getValue() == null) basvuruBitis.setRequired(true);
                else if(addExam == true && basvuruSinavSayisi.getValue() == null) basvuruSinavSayisi.setRequired(true);
                else{
                    SimpleDateFormat sdf = new SimpleDateFormat();
                    sdf.applyPattern("dd-MM-yyyy");
                    Application updatedExam = new Application();
                    updatedExam.id = exam.id;
                    updatedExam.name = sinavAdi.getValue();                   
                    updatedExam.startTime = sdf.format(basvuruBaslama.getValue());
                    updatedExam.endTime = sdf.format(basvuruBitis.getValue());
                   
                   // updatedExam.date = sdf.format(sinavTarih.getValue());
               //     updatedExam.departmentName = (String) bolum.getValue();
                   // updatedExam.teacher = sinavOgretmeni.getValue();
                   // updatedExam.type = (String) sinavTuru.getValue();
                    //System.out.println(updatedExam.id+"-"+updatedExam.name+"-"+updatedExam.date+"-"+updatedExam.departmentName+"-"+updatedExam.teacher+"-"+updatedExam.type);
                    
                    
                    
                    Application a = appService.UpdateApplication(updatedExam);
                    appService.deleteDepartmentRelationship(a.getId());
                    appService.isBelongTo(b,a.getId());
                   
                    appService.deleteRequiredInformation(a.getId());
                    for(int i = 0; i< bilgiListe.size(); i++){
                            Long varmi = informationService.getInformation(bilgiListe.get(i));
                            
                            if(varmi == null){
                                Long iid =  informationService.addNeedInformation(bilgiListe.get(i));
                                appService.requiredInformation(iid, exam.getId(),tfield.get(i).getValue());
                            }
                            else{
                               appService.requiredInformation(varmi, exam.getId(),tfield.get(i).getValue());
                            }
                        }
                        //aaaaaaaaaaaahaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaajaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahhaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahaaaayaaaaaaaaaaaaahaaaaaaahazaaaaaaa
                    appService.deleteRequired(a.getId());
                    for(int i = 0; i< liste.size(); i++){
                        Long varmi = documentService.getDocument(liste.get(i));

                        if(varmi == null){//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahaaaaaaghaahahaaahaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                            Long did =  documentService.addNeedDocument(liste.get(i));
                            appService.required(did, exam.getId());
                        }
                        else{
                            appService.required(varmi, exam.getId());
                        }
                    }
                    
                    
                    if(a == updatedExam){
                        for (int i = 0; i<1; i++){
                            Iterator<Window> w = getUI().getWindows().iterator();
                            getUI().removeWindow(w.next()); 
                        }
                
//                        Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>İşleminiz Başarıyla Gerçekleşmiştir!</b>", Notification.Type.WARNING_MESSAGE, true); 
//
//                        notif.setDelayMsec(200);
//                        notif.setPosition(Position.TOP_CENTER);
//                        notif.setStyleName("examValidation");
//                        notif.setHtmlContentAllowed(true); 
//
//                        notif.show(Page.getCurrent());
                             if(addExam == true){
                                  boolean ilkmi = true;
                                final Window window2 = new Window("Başvuru İçin Sınav Ekle");
                                window2.setModal(true);

                                window2.setStyleName("ilkwindow");
                                window2.setWidth("1200");
                                window2.setHeight("500");

                                window2.setDraggable(false);
                                window2.setResizable(false);
                                 for (int i = 0; i < bilgiListe.size(); i++) {
                                     System.out.println(bilgiListe.get(i));
                                 }
                                //window2.setContent(new AddExamsWindow(examList,ilkmi, bilgiListe, tfield, examService, typeService, teach, cService));
                                
                                UI.getCurrent().addWindow(window2);
                            }
                    }
                    
                    else{
                        for (int i = 0; i<1; i++){
                            Iterator<Window> w = getUI().getWindows().iterator();
                            getUI().removeWindow(w.next()); 
                        }
                        
                        Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Daha Önce Kaydedilmiş Bir Sınav!</b>", Notification.Type.WARNING_MESSAGE, true); 

                        notif.setDelayMsec(300);
                        notif.setPosition(Position.TOP_CENTER);
                        notif.setStyleName("errorValidation");
                        notif.setHtmlContentAllowed(true); 

                        notif.show(Page.getCurrent());
                    }
                }
            }
        });

        hEkleme.addComponent(ekleme1);
        hEkleme.addComponent(ekleme2);
        hEkleme.addComponent(ekleme4);
        this.addComponent(hEkleme);
        this.addComponent(ekleme3);
        
        
    }

  
 
}
