/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.StatikSinifVeFonksiyonlar;
import com.example.model.Classroom;
import com.example.model.Criterion;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.Teacher;
import com.example.model.Type;
import com.example.service.ClassroomService;
import com.example.service.ExamService;
import com.example.service.TypeService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import java.util.ArrayList;
import java.util.List;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import java.text.SimpleDateFormat;
import java.util.Iterator;

/**
 *
 * @author hatice
 */
@DesignRoot
public class AddExamsWindow extends VerticalLayout{
    VerticalLayout ekleme1, ekleme2, ekleme3, ekleme4, addExamsSinavKriter;
    ComboBox sinavTuru, sinavSaati,sinavDak, derslik; 
    TextField sinavAdi, sinavKontenjani;
    Button kaydet, kriterEkle;
    DateField sinavTarih;  
    Label label2, label3, label5, label6, ikinokta, lSinavKontenjanı, lDerslikler, kriterBilgi;
    HorizontalLayout hEkleme, gizliKriter;
    
    List<TextField> kriterTextField = new ArrayList<TextField>();
    BeanItemContainer<Classroom> classBean = new BeanItemContainer<>(Classroom.class);

    public Grid derslikGrid = new Grid();
    public Grid kriterlerGrid = new Grid();
    
    Exam exam = new Exam();
    
    int kisiSayisi = 0;
    int kontenjan = 0;    
    int kacinciWindow = 0;
    int toplama = 0;
    
    ExamService examService;
    ClassroomService cService;
    
    Long b, typeId;
    
    Button kkButon = new Button("Kriteri Kaydet");
    VerticalLayout kriterLayout = new VerticalLayout();
    public AddExamsWindow(int kacinciSinavWindowu, Window window2, List<Exam> examList, List<Information> bilgiListe, List<TextField> tfield, ExamService _examService, TypeService typeService, Teacher teach, ClassroomService _cservice){
        Design.read(this);
        examService = _examService;
        cService = _cservice;
        
        derslikGrid.setStyleName("derslikGrid");
        
        kriterlerGrid.setStyleName("basvuruKriterlerGrid");
        kriterlerGrid.setVisible(false);
        
        derslik.setNullSelectionAllowed(false);
        sinavTuru.setNullSelectionAllowed(false);
        
        if(kacinciSinavWindowu > 1 && !examList.isEmpty()){
            gizliKriter.setVisible(false);
            kriterEkle.setVisible(true);
        }
        else if(examList.isEmpty()){
            kriterEkle.setVisible(false);
            gizliKriter.setVisible(true);
            for (int i = 0; i < bilgiListe.size(); i++) {
                Label l = new Label(bilgiListe.get(i).getName()+" bilgisinin %"+tfield.get(i).getValue());
                l.setStyleName("lKriterLabel");
                addExamsSinavKriter.addComponent(l);
            }
            Label toplam = new Label("Bu bilgilerin ortalaması "+tfield.get(tfield.size()-1).getValue()+" den/dan büyük olanlar sınava girebilecektir.");
            toplam.setStyleName("lKriterLabel");
            addExamsSinavKriter.addComponent(toplam);
        }
        else if(kacinciSinavWindowu == 1){
            kriterEkle.setVisible(false);
            gizliKriter.setVisible(true);
            for (int i = 0; i < bilgiListe.size(); i++) {
                Label l = new Label(bilgiListe.get(i).getName()+" bilgisinin %"+tfield.get(i).getValue());
                l.setStyleName("lKriterLabel");
                addExamsSinavKriter.addComponent(l);
            }
            Label toplam = new Label("Bu bilgilerin ortalaması "+tfield.get(tfield.size()-1).getValue()+" den/dan büyük olanlar sınava girebilecektir.");
            toplam.setStyleName("lKriterLabel");
            addExamsSinavKriter.addComponent(toplam);
        }
        
        List<Classroom> cList = cService.getAllClassrooms();
        BeanItemContainer<Classroom> cBean = new BeanItemContainer<>(Classroom.class);
        cBean.addAll(cList);

        if(kisiSayisi == 0) {
            kaydet.setEnabled(false);
            derslik.setEnabled(false);
        }
        else {
            kaydet.setEnabled(true);
            derslik.setEnabled(true);
        }
        
        sinavKontenjani.setRequired(false);
        sinavKontenjani.setRequiredError("Kişi Sayısını Girmeyi Unutmayın!");
        
        sinavKontenjani.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            public void textChange(FieldEvents.TextChangeEvent event) {
                try{
                    String varmi = event.getText();
                    if(varmi != null){
                        kisiSayisi = Integer.parseInt(varmi);
                        if(kisiSayisi == 0) {
                            kaydet.setEnabled(false);
                            derslik.setEnabled(false);
                        }
                        else {
                            kaydet.setEnabled(true);
                            derslik.setEnabled(true);
                            if(kisiSayisi > kontenjan && kontenjan > 0){
                                kaydet.setEnabled(false);

                                Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Kontenjan yetersiz.<br/> Lütfen ek derslik seçin!</b>", Notification.Type.WARNING_MESSAGE, true); 

                                notif.setDelayMsec(3000);
                                notif.setPosition(Position.TOP_CENTER);
                                notif.setStyleName("errorValidation");
                                notif.setHtmlContentAllowed(true); 

                                notif.show(Page.getCurrent());
                            }
                            else{
                                kaydet.setEnabled(true);
                            }
                        }
                        if(derslik.getValue() == null){
                            kaydet.setEnabled(false);
                        }
                    }
                    else {
                        kaydet.setEnabled(false);
                        derslik.setEnabled(false);
                    }
                }
                catch(NumberFormatException e){
                    kaydet.setEnabled(false);
                    derslik.setEnabled(false);
                }
            }
        });
        
        
        derslik.setContainerDataSource(cBean);
        derslik.setItemCaptionPropertyId("name");  //BOLUMUN ISIMLERINI LISTELETMEK ICIN
        derslik.setRequired(false);
        derslik.setRequiredError("Sınavın Yapılacağı Dersliklerin Seçimini Unutmayın!");
        derslik.addValueChangeListener(new ValueChangeListener(){
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Item item = derslik.getItem(derslik.getValue());
                Long selectedClassroom = Long.parseLong(item.getItemProperty("id").toString());
                
                for (int i = 0; i < cList.size(); i++) {
                   if (cList.get(i).getId().equals(selectedClassroom) && selectedClassroom != null) { //
                       exam.classliste.add(cList.get(i));
                       kontenjan += cList.get(i).getQuota();
                   }
                }
                
                classBean.removeAllItems();
                classBean.addAll(exam.classliste);
                derslikGrid.setContainerDataSource(classBean);
                derslikGrid.removeAllColumns();
                derslikGrid.addColumn("name");
                derslikGrid.addColumn("quota");      
                derslikGrid.addColumn("id").setHidden(true);
                
                derslikGrid.getColumn("name").setHeaderCaption("Derslik");
                derslikGrid.getColumn("quota").setHeaderCaption("Kapasite");

                if(kisiSayisi > kontenjan){
                    kaydet.setEnabled(false);
                    ekleme4.addComponent(derslikGrid);
                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Kontenjan yetersiz.<br/> Lütfen ek derslik seçin!</b>", Notification.Type.WARNING_MESSAGE, true); 

                    notif.setDelayMsec(3000);
                    notif.setPosition(Position.TOP_CENTER);
                    notif.setStyleName("errorValidation");
                    notif.setHtmlContentAllowed(true); 

                    notif.show(Page.getCurrent());
                }
                else{
                    kaydet.setEnabled(true);
                    ekleme4.addComponent(derslikGrid);
                }
            }
        });
        
        derslikGrid.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) derslikGrid.getSelectionModel()).getSelectedRow();
            
            Classroom c = (Classroom) selected;
            
            for (int i = 0; i < exam.classliste.size(); i++) {
                if(exam.classliste.get(i).equals(selected)){
                    exam.classliste.remove(selected);
                }
            }
            
            if(exam.classliste.size() == 0){
                kaydet.setEnabled(false);
                
                Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Kontenjan yetersiz.<br/> Lütfen ek derslik seçin!</b>", Notification.Type.WARNING_MESSAGE, true); 

                notif.setDelayMsec(3000);
                notif.setPosition(Position.TOP_CENTER);
                notif.setStyleName("errorValidation");
                notif.setHtmlContentAllowed(true); 

                notif.show(Page.getCurrent());
            }
            else{
                kaydet.setEnabled(true);
            }
            
            classBean.removeAllItems();
            classBean.addAll(exam.classliste);
            derslikGrid.setContainerDataSource(classBean);
            derslikGrid.removeAllColumns();
            derslikGrid.addColumn("name");
            derslikGrid.addColumn("quota");      
            derslikGrid.addColumn("id").setHidden(true);

            derslikGrid.getColumn("name").setHeaderCaption("Derslik");
            derslikGrid.getColumn("quota").setHeaderCaption("Kapasite");
        });

        List<Type> typeList = typeService.getAllTypes();
        BeanItemContainer<Type> typeBean = new BeanItemContainer<>(Type.class);
        typeBean.addAll(typeList);
        
        sinavTuru.setContainerDataSource(typeBean);
        sinavTuru.setItemCaptionPropertyId("name");
        sinavTuru.setInputPrompt("Sınav Türü Seçilmedi!");
        sinavTuru.setRequired(false);
        sinavTuru.setRequiredError("Sınav Türü Seçimini Unutmayın!");
        
        sinavAdi.setRequired(false);
        sinavAdi.setRequiredError("Sınav Adı Boş Bırakılamaz!");
        
        sinavTarih.setRequired(false);
        sinavTarih.setRequiredError("Tarih Seçimi Boş Bırakılamaz!");
        
        sinavSaati.setInputPrompt("Saat");
        sinavSaati.setRequiredError("Saat Secmeyi Unutmayin!");
        
        for(int i=0;i<10;i++)
        {
            sinavSaati.addItem("0"+i);
        }
        
        for(int j=10;j<25;j++)
        {
            sinavSaati.addItem(j);
        }
        
      
        sinavDak.setInputPrompt("Dakika");
        sinavDak.setRequiredError("Dakika Secmeyi Unutmayin!");
      
        for(int k=0;k<10;k++)
        {
            sinavDak.addItem("0"+k);
        }
        
        for(int h=10;h<60;h++)
        {
            sinavDak.addItem(h);
        }

        sinavTarih.setDateFormat("dd-MM-yyyy");
       
        sinavTuru.addValueChangeListener(new ValueChangeListener(){
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Item item = sinavTuru.getItem(sinavTuru.getValue());
                String selectedType = item.getItemProperty("id").toString();
                
                for (int i = 0; i < typeList.size(); i++) {
                   if (typeList.get(i).getId().toString().equals(selectedType)) { //
                       typeId = Long.parseLong(selectedType);
                   }
                }
                exam.examType.setId(typeId);
                exam.examType.setName(item.getItemProperty("name").toString());
            }
            
        });
        
        hEkleme.addComponent(ekleme1);
        hEkleme.addComponent(ekleme2);
        ekleme4.addComponent(kriterlerGrid);
        hEkleme.addComponent(ekleme4);
        this.addComponent(hEkleme);
        this.addComponent(ekleme3);
        
        kriterEkle.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                kriterTextField.clear();
                kriterLayout.removeAllComponents();
                kacinciWindow++;
                
                final Window window = new Window("Sınava Kriter Ekleme");
                window.setModal(true);
                
                window.setStyleName("ilkwindow");
                window.setWidth("400");
                window.setHeight("400");
                
                window.setDraggable(false);
                window.setResizable(false);
                
                if(exam.examKriterList.isEmpty()){
                    for (int i = 0; i < examList.size(); i++) {
                        HorizontalLayout h = new HorizontalLayout();
                        Label a = new Label("->");
                        a.setStyleName("appALabel");
                        Label bilgi = new Label(examList.get(i).getName()+" sınavının");
                        bilgi.setStyleName("appBilgiLabel");

                        Label y = new Label("%");
                        y.setStyleName("appYLabel");

                        TextField t = new TextField();
                        t.setRequired(false);
                        t.setRequiredError("toplam 100e eşit olmalı");
                        t.setStyleName("kriterBilgiTextfield");
                        kriterTextField.add(t);
                        
                        h.addComponent(a);
                        h.addComponent(bilgi);
                        h.addComponent(y);
                        h.addComponent(t);
                        kriterLayout.addComponent(h);
                    }
                    System.out.println("bilgi liste: "+bilgiListe.size());
                    for (int i = 0; i < bilgiListe.size(); i++) {
                        HorizontalLayout h = new HorizontalLayout();
                        Label a = new Label("->");
                        a.setStyleName("appALabel");
                        Label bilgi = new Label(bilgiListe.get(i).getName()+" bilgisinin");
                        bilgi.setStyleName("appBilgiLabel");

                        Label y = new Label("%");
                        y.setStyleName("appYLabel");

                        TextField t = new TextField();
                        t.setRequired(false);
                        t.setRequiredError("toplam 100e eşit olmalı");
                        t.setStyleName("kriterBilgiTextfield");
                        kriterTextField.add(t);
                        
                        h.addComponent(a);
                        h.addComponent(bilgi);
                        h.addComponent(y);
                        h.addComponent(t);
                        kriterLayout.addComponent(h);
                    }
                    HorizontalLayout h2 = new HorizontalLayout();
                    Label toplam = new Label("Ortalaması");
                    toplam.setStyleName("appToplamLabel");

                    TextField t2 = new TextField();
                    kriterTextField.add(t2);
                    t2.setStyleName("kriterBilgiTextfield");

                    Label b = new Label("den büyük olsun.");
                    b.setStyleName("appBLabel");

                    h2.addComponent(toplam);
                    h2.addComponent(t2);
                    h2.addComponent(b);

                    kriterLayout.addComponent(h2);
                }
                else if(exam.examKriterList.size() > 0){
                    for (int i = 0; i < examList.size(); i++) {
                        HorizontalLayout h = new HorizontalLayout();
                        Label a = new Label("->");
                        a.setStyleName("appALabel");
                        Label bilgi = new Label(examList.get(i).getName()+" bilgisinin");
                        bilgi.setStyleName("appBilgiLabel");

                        Label y = new Label("%");
                        y.setStyleName("appYLabel");

                        TextField t = new TextField();
                        t.setRequired(false);
                        t.setRequiredError("toplam 100e eşit olmalı");
                        t.setStyleName("kriterBilgiTextfield");
                        kriterTextField.add(t);

                        h.addComponent(a);
                        h.addComponent(bilgi);
                        h.addComponent(y);
                        h.addComponent(t);
                        kriterLayout.addComponent(h);
                    }
                    for (int i = 0; i < bilgiListe.size(); i++) {
                        HorizontalLayout h = new HorizontalLayout();
                        Label a = new Label("->");
                        a.setStyleName("appALabel");
                        Label bilgi = new Label(bilgiListe.get(i).getName()+" bilgisinin");
                        bilgi.setStyleName("appBilgiLabel");

                        Label y = new Label("%");
                        y.setStyleName("appYLabel");

                        TextField t = new TextField();
                        t.setRequired(false);
                        t.setRequiredError("toplam 100e eşit olmalı");
                        t.setStyleName("kriterBilgiTextfield");
                        kriterTextField.add(t);

                        h.addComponent(a);
                        h.addComponent(bilgi);
                        h.addComponent(y);
                        h.addComponent(t);
                        kriterLayout.addComponent(h);
                    }
                    HorizontalLayout h2 = new HorizontalLayout();
                    Label toplam = new Label("Ortalaması");
                    toplam.setStyleName("appToplamLabel");

                    TextField t2 = new TextField();
                    kriterTextField.add(t2);
                    t2.setStyleName("kriterBilgiTextfield");

                    Label b = new Label("den büyük olsun.");
                    b.setStyleName("appBLabel");

                    h2.addComponent(toplam);
                    h2.addComponent(t2);
                    h2.addComponent(b);

                    kriterLayout.addComponent(h2);
                    
//                    for (int i = 0; i < exam.examKriterList.size(); i++) {
//                        for (int j = 0; j < exam.examKriterList.get(i).infoList.size(); j++) {
//                            for (int k = 0; k < bilgiListe.size(); k++) {
//                                if(exam.examKriterList.get(i).infoList.get(j).equals(bilgiListe.get(k))){
//                                    kriterTextField.get(k).setEnabled(false);
//                                }
//                            }
//                        }
//                    }
                }

                kkButon.setStyleName("kkButon");
                              
                kriterLayout.addComponent(kkButon);
                window.setContent(kriterLayout);

                UI.getCurrent().addWindow(window);
                
                kkButon.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        Criterion kriter = new Criterion();
                        String formul = "";
                        System.out.println("tfield size ı: "+kriterTextField.size());
                        for (int i = 0; i < kriterTextField.size()-1; i++) 
                        {
                            if(!kriterTextField.get(i).getValue().isEmpty()){
                                System.out.println("i şöyle: "+i);
                                kriter.percentList.add(Integer.parseInt(kriterTextField.get(i).getValue()));
                                if(i <= examList.size()-1) kriter.examList.add(examList.get(i));
                                else if(i > examList.size()-1) kriter.infoList.add(bilgiListe.get(i-examList.size()));
                            }
                            //toplama = Integer.parseInt(tfield.get(i).getValue()) + toplama;
                   
                        }

                        for (int i = 0; i < kriter.percentList.size(); i++) {
                            if(!kriter.examList.isEmpty() && kriter.infoList.isEmpty()){
                                if(i == kriter.percentList.size()-1) formul += kriter.examList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)";
                                else formul += kriter.examList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)+";
                                break;
                            }
                            else if(kriter.examList.isEmpty() && !kriter.infoList.isEmpty()){
                                if(i == kriter.percentList.size()-1) formul += kriter.infoList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)";
                                else formul += kriter.infoList.get(i).getName()+"*("+kriter.percentList.get(i)+"/100)+";
                                break;
                            }
                            else if(!kriter.examList.isEmpty() && !kriter.infoList.isEmpty()){
                                for (int j = 0; j < kriter.examList.size(); j++) {
                                    if(j == (kriter.percentList.size()-kriter.infoList.size()-1)) formul += kriter.examList.get(j).getName()+"*("+kriter.percentList.get(j)+"/100)";
                                    else formul += kriter.examList.get(j).getName()+"*("+kriter.percentList.get(j)+"/100)+";
                                }
                                for (int j = 0; j < kriter.infoList.size(); j++) {
                                    formul += "+ "+kriter.infoList.get(j).getName()+"*("+kriter.percentList.get(j+examList.size())+"/100)";
                                }
                                break;
                            }
                            
                        }
                        
                        kriter.averagePercent = Integer.parseInt(kriterTextField.get(kriterTextField.size()-1).getValue());
                        
                        formul = "("+formul+")/"+kriter.percentList.size()+" > "+kriter.averagePercent;
                        kriter.formul = formul;
                        System.out.println("formul: "+formul);
                        kriter.averagePercent = Integer.parseInt(kriterTextField.get(kriterTextField.size()-1).getValue());

                        boolean esitmi = false;
                        for (int i = 0; i < exam.examKriterList.size(); i++) {
                            if(exam.examKriterList.get(i).equals(kriter)) esitmi = true; 
                        }
                        if(esitmi == false) exam.examKriterList.add(kriter);

                        for (int i = 0; i < kriterTextField.size()-1; i++) {
                            if(toplama > 100 || toplama <100)
                            {
                                kriterTextField.get(i).setRequired(true);
                            }
                            
                        }
                        UI.getCurrent().removeWindow(window);
                        
                        kriterlerGrid.setVisible(true);
                        String[] gosterilecekAlanlar = {"formul", "id"};
                        String[] alanBasliklari = {"Kriter Formülü"};

                        StatikSinifVeFonksiyonlar.refreshGridWithHeaders(exam.examKriterList, Criterion.class, kriterlerGrid, gosterilecekAlanlar, alanBasliklari);
                    }
                });
            }
        });
        
        kaydet.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {          
                if(sinavAdi.getValue() == "") sinavAdi.setRequired(true);
                else if(sinavTuru.getValue() == null) sinavTuru.setRequired(true);
                else if(sinavTarih.getValue() == null) sinavTarih.setRequired(true);
                else if(sinavSaati.getValue() == null) sinavSaati.setRequired(true);
                else if(sinavDak.getValue() == null) sinavDak.setRequired(true);
                else if(sinavKontenjani.getValue() == "") sinavKontenjani.setRequired(true);
                else if(derslik.getValue() == null) derslik.setRequired(true);
                else{
                    exam.name = sinavAdi.getValue();
                    SimpleDateFormat sdf = new SimpleDateFormat();
                    sdf.applyPattern("dd-MM-yyyy");
                    exam.date = sdf.format(sinavTarih.getValue());
                    String degisken = sinavSaati.getValue() + ":" + sinavDak.getValue();
                    exam.time = degisken;  
                    exam.personCount = kisiSayisi;
                    
                    if(examList.isEmpty()) examList.add(exam);
                    else{
                        for (int i = 0; i < examList.size(); i++) {
                            if(!examList.get(i).equals(exam)) examList.add(exam);
                        }
                    }
                        
                    System.out.println("idsi: "+Long.valueOf(examList.size()-1));
                    examList.get(examList.size()-1).setId(Long.valueOf(examList.size()-1));
                    
                    window2.getUI().removeWindow(window2);
                    
                    addApplicationsWindow.sinavlarGrid.setVisible(true);
                    
                    String[] gosterilecekAlanlar = {"name", "id"};
                    String[] alanBasliklari = {"Sınav Adı"};
                    
                    StatikSinifVeFonksiyonlar.refreshGridWithHeaders(examList, Exam.class, addApplicationsWindow.sinavlarGrid, gosterilecekAlanlar, alanBasliklari);
                }
            }
        });
    }

    public void enter(ViewChangeListener.ViewChangeEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
