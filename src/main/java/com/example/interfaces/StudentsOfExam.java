/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.InformationOfApps;
import com.example.model.Student;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Property;
import com.vaadin.event.ContextClickEvent;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.ValoTheme;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author hatice
 */
@DesignRoot
public class StudentsOfExam extends VerticalLayout{
    VerticalLayout iconHorizontal, studentsHorizontal, studentsMarksHorizontal, yayimlamaVertical;
    HorizontalLayout sinavSonucYayimlama;
    Button sonucKaydet;
    DateField yayimlamaTarihi;
    
    InformationOfApps info = new InformationOfApps();
    
    List<Student> studentsOfPastExam = new ArrayList<Student>();
    List<TextField> tfield = new ArrayList<TextField>();
    
    final List<String> options = Arrays.asList(new String[] {"Şimdi Yayımla", "İleri Tarihte Yayımla"});
    
    String yayimlama = "Şimdi Yayımla";
    String tarih;
    
    int mark = 0;
    int i = 0;
    public StudentsOfExam(Object selected, ExamService examService){
        Design.read(this);
        
        info = (InformationOfApps) selected;
        this.setStyleName("pastExamsWindow");
        
        yayimlamaTarihi.setRequired(false);
        yayimlamaTarihi.setRequiredError("Yayımlama Tarihi Boş Bırakılamaz!");
        yayimlamaTarihi.setDateFormat("dd-MM-yyyy");
        yayimlamaTarihi.setVisible(false);

        final OptionGroup group = new OptionGroup("", options);
        group.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
        group.setNullSelectionAllowed(false);
        group.select("Şimdi Yayımla");
        
        group.addValueChangeListener( ( Property.ValueChangeEvent event ) -> {
            yayimlama = (String) event.getProperty().getValue();

            if(event.getProperty().getValue() == "İleri Tarihte Yayımla"){
                yayimlamaTarihi.setVisible(true);
            }
            else if(event.getProperty().getValue() == "Şimdi Yayımla"){
                yayimlamaTarihi.setVisible(false);
            }
            
        } );
        
        sinavSonucYayimlama.addComponent(group);
        
        examService.getStudentsOfPastExam(Long.parseLong(info.getExamId())).forEach(
            (a)->{
                studentsOfPastExam.add(a);
                Label t = new Label(a.getTc()+"   "+a.getName()+" "+a.getSurname());
                t.setStyleName("studentNameStyle");
                
                TextField text = new TextField();
                tfield.add(text);
                
                studentsMarksHorizontal.addComponent(text);
                
                if(i%2 == 0){
                    text.setStyleName("studentsMarksStyle");
                    Label i = new Label();
                    i.setIcon(new ThemeResource("images/pink.png"));
                    iconHorizontal.addComponent(i);
                }
                else if(i%2 != 0){
                    text.setStyleName("singleStudentsMarksStyle");
                    Label i = new Label();
                    i.setIcon(new ThemeResource("images/blue.png"));
                    iconHorizontal.addComponent(i);
                }
                
                studentsHorizontal.addComponent(t);
                
                mark = examService.getStudentsMarkOfExam(Long.parseLong(info.getExamId()), studentsOfPastExam.get(i).getId());
                text.setValue(Integer.toString(mark));
                i++;
            }
        );
                
        sonucKaydet.addClickListener(new Button.ClickListener() {
            int yanlismi = 0;
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if(yayimlama == "Şimdi Yayımla"){
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = new Date();
                    tarih = dateFormat.format(date);  
                }
                else if(yayimlama == "İleri Tarihte Yayımla"){
                    if(yayimlamaTarihi.getValue() == null) yayimlamaTarihi.setRequired(true);
                    else{
                        SimpleDateFormat sdf = new SimpleDateFormat();
                        sdf.applyPattern("dd-MM-yyyy");
                        tarih = sdf.format(yayimlamaTarihi.getValue());
                    }
                }
                if(tarih != null){
                    for (int j = 0; j < tfield.size(); j++) {
                        if(examService.addMarksOfExam(Long.parseLong(info.getExamId()), studentsOfPastExam.get(j).getId(), Integer.parseInt(tfield.get(j).getValue()), tarih) != Integer.parseInt(tfield.get(j).getValue())){
                            yanlismi++;
                        }
                    }
                    if(yanlismi > 0){
                        for (int i = 0; i<1; i++){
                            Iterator<Window> w = getUI().getWindows().iterator();
                            getUI().removeWindow(w.next()); 
                        }
                        Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Kayıt İşlemi Gerçekleşirken Bir Hata Oldu!</b>", Notification.Type.WARNING_MESSAGE, true); 

                        notif.setDelayMsec(2000);
                        notif.setPosition(Position.TOP_CENTER);
                        notif.setStyleName("errorValidation");
                        notif.setHtmlContentAllowed(true); 

                        notif.show(Page.getCurrent());
                    }
                    else{
                        for (int i = 0; i<1; i++){
                            Iterator<Window> w = getUI().getWindows().iterator();
                            getUI().removeWindow(w.next()); 
                        }
                        Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe6dc;</i>", "<b style='color: #FFFFFF;'>Kayıt işleminiz gerçekleşmiştir!</b>", Notification.Type.WARNING_MESSAGE, true); 

                        notif.setDelayMsec(2000);
                        notif.setPosition(Position.TOP_CENTER);
                        notif.setStyleName("examValidation");
                        notif.setHtmlContentAllowed(true); 

                        notif.show(Page.getCurrent());
                    }
                }
            }
        });
    }
}
