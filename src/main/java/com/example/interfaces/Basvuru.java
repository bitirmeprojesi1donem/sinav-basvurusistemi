
package com.example.interfaces;


import com.example.model.Classroom;
import com.example.model.Department;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import com.example.model.Application;
/**
 *
 * @author Deneme
 */
@DesignRoot
public class Basvuru extends HorizontalLayout{
    Panel basvuruBolumPanel, basvuruSinavPanel, basvuruSinavBilgiPanel, basvuruPanel;
    VerticalLayout basvuruBolumLayout, basvuruSinavLayout, basvuruSinavBilgiLayout, basvuruLayout,v, logout;
    Label basvuruBolumLabel, basvuruSinavLabel, basvuruSinavBilgiLabel, basvuruLabel;
    Label label4,label7, label8;
    Label lSinavOgretmeni, lZorunluBelgeler, lZorunluBilgiler,lBasvuruBitis;
    Button Basvur;
    File file;

    Grid bolumGrid = new Grid();
    Grid sinavGrid = new Grid();

    ArrayList<Document> liste = new ArrayList();
    ArrayList<Information> zorunluBilgi = new ArrayList<Information>();
    ArrayList<Classroom> derslikler = new ArrayList<>();
      
    ExamService examService;
    DocumentService documentService;
    InformationService informationService;
    ApplicationService appService;
  
    Student student;
    
    MenuBar barmenu = new MenuBar();
    public Basvuru(ApplicationService _appService,DepartmentService dep, ExamService _examService, Student _student, DocumentService _documentService, InformationService _informationService)
    {
        Design.read(this);
        student = _student;
        documentService = _documentService;
        examService = _examService;
        informationService = _informationService;
        appService = _appService;   
        
        barmenu.setStyleName("barmenu");
        logout.addComponent(barmenu);
        
        MenuBar.Command mycommand = new MenuBar.Command() {
            @Override
            public void menuSelected(MenuItem selectedItem) {
                if(selectedItem.getText().equals("Ana Sayfa")){
                    getUI().getNavigator().navigateTo("SinavBasvuru");
                }
                else if(selectedItem.getText().equals("Başvurulan Sınavlar")){
                    getUI().getNavigator().navigateTo("BaşvurulanSınavlar");
                }
                else if(selectedItem.getText().equals("Çıkış Yap")){
                    sinavGrid.setStyleName("gizle");
                    getSession().setAttribute("userRole", null);
                    getUI().getNavigator().navigateTo("Login");
                }
            }  
        };
        MenuItem menu = barmenu.addItem("", new ThemeResource("images/align-justify.png"), null);
        MenuBar.MenuItem anaSayfa = menu.addItem("Ana Sayfa", new ThemeResource("images/home.png"), mycommand);
        MenuItem basvurularim = menu.addItem("Başvurulan Sınavlar", new ThemeResource("images/online-test.png"), mycommand);
        MenuItem cikis = menu.addItem("Çıkış Yap", new ThemeResource("images/sign-out.png"), mycommand);

       
        bolumGrid.setStyleName("basvuruGrid");
        sinavGrid.setStyleName("sinavGrid");
        
        basvuruSinavLayout.setStyleName("gizle");
        basvuruSinavBilgiLayout.setStyleName("gizle");
        basvuruLayout.setStyleName("gizle");
        
        List<Department> depList = dep.getAllDepartments();
        BeanItemContainer<Department> depl = new BeanItemContainer<>(Department.class);
        depl.addAll(depList);
        
        bolumGrid.setContainerDataSource(depl);
        bolumGrid.removeAllColumns();
        bolumGrid.addColumn("name");
        bolumGrid.addColumn("id").setHidden(true);
        bolumGrid.getColumn("name").setHeaderCaption("Bölüm Adı");
      

        bolumGrid.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) bolumGrid.getSelectionModel()).getSelectedRow();
            if(selected == null){
                basvuruSinavLayout.setStyleName("gizle");
                basvuruSinavBilgiLayout.setStyleName("gizle");
                basvuruLayout.setStyleName("gizle");
            }else {
                Department department = (Department) selected;
                Long bolumId = department.id;
                basvuruSinavLayout.setStyleName("basvuruSinavPanel");
                sinav(bolumId);
            }
        });
        
        
        basvuruBolumPanel.setContent(bolumGrid);
    }  
    
    private long time(String tarih1,String tarih2){
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
        String inputString1 = tarih1;
        String inputString2 = tarih2;
        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
           // System.out.println (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace(); 
        }
        return 0;
    }

    private void sinav(Long bolumId) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String today = dateFormat.format(date);
        
        List<Application> appList = appService.getAppOfDepartment(bolumId);
        List<Application> geciciAppList = new ArrayList<Application>();
        BeanItemContainer<Application> examBean = new BeanItemContainer<>(Application.class);
     
        //time bugünden gelen zamanı çıkarıyor
        
        for(int i=0; i<appList.size(); i++){
            System.out.println(appList.get(i).getStartTime());
            Long a = time(appList.get(i).getStartTime(), today);
            Long b = time(appList.get(i).getEndTime(), today); 

            if(a >= 0 && b <= 0){
                geciciAppList.add(appList.get(i));
            }
            //else if(a >= 0) System.out.println("küçük");
        }
        
        examBean.addAll(geciciAppList);
        
        sinavGrid.setContainerDataSource(examBean);
        sinavGrid.removeAllColumns();
        sinavGrid.addColumn("name");
        sinavGrid.addColumn("id").setHidden(true);
        sinavGrid.getColumn("name").setHeaderCaption("Başvuru Adı");

        sinavGrid.addSelectionListener(selectionEvent -> {
            Object selected = ((Grid.SingleSelectionModel) sinavGrid.getSelectionModel()).getSelectedRow();
            if(selected == null){
                basvuruSinavBilgiLayout.setStyleName("gizle");
                basvuruLayout.setStyleName("gizle");
                return;
            }
            Application application = (Application) selected;
            Long appId = application.id;
            basvuruSinavBilgiLayout.setStyleName("basvuruSinavBilgiPanel"); 
            
            sinavBilgi(appId);
        });
        basvuruSinavPanel.setContent(sinavGrid);

    }

    private void sinavBilgi(Long appId) {
        Application e = (Application) appService.getAppFromId(appId);
        String name = appService.getAppDepartment(appId);
        
        String zorunluBelgeler = "";
        String zorunluBilgiler = "";
        String derslikİsimleri = "";
        
        liste = documentService.getRequiredDocumentForApp(appId);
        zorunluBilgi = informationService.getRequiredInformationForApp(appId);
      //  derslikler = examService.getClassroomByExamId(sinavId);
        
//        for(int k=0;k<derslikler.size();k++)
//        {
//            if(k == derslikler.size()-1){
//                derslikİsimleri += derslikler.get(k).getName();
//            }
//            else{
//                derslikİsimleri += derslikler.get(k).getName()+", ";
//            } 
//        }
        
        
        
        
        for(int i= 0; i<liste.size(); i++){
            if(i == liste.size()-1){
                zorunluBelgeler += liste.get(i).getName();
            }
            else{
                zorunluBelgeler += liste.get(i).getName()+", ";
            } 
        }
        if(zorunluBelgeler.isEmpty()){
             lZorunluBelgeler.setValue("Bulunmamaktadır");
        }
        else{
             lZorunluBelgeler.setValue(zorunluBelgeler);   
        }
        
        
        for(int i= 0; i<zorunluBilgi.size(); i++){
            if(i == zorunluBilgi.size()-1){
                zorunluBilgiler += zorunluBilgi.get(i).getName();
            }
            else{
                zorunluBilgiler += zorunluBilgi.get(i).getName()+", ";
            } 
        }
        if(zorunluBilgiler.isEmpty()){
             lZorunluBilgiler.setValue("Bulunmamaktadır");
        }
        else{
             lZorunluBilgiler.setValue(zorunluBilgiler);
        }
        
      //  lSinavTuru.setValue(examService.getExamType(appId));
        lSinavOgretmeni.setValue(appService.getTeacher(appId).getName()+" "+appService.getTeacher(appId).getSurname());
     //   lSinavZamani.setValue(e.time);
     //   lSinavTarih.setValue(e.date);
        lBasvuruBitis.setValue(e.endTime);  
     //   lDerslik.setValue(derslikİsimleri);
        
        Basvur.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
               basvuruLayout.setStyleName("basvuruLayout");
               kayit(e, liste);
               
            }
        }); 
        basvuruSinavBilgiPanel.setContent(v);
    }
    public void kayit(Application app, ArrayList<Document> liste){
     //   dosyaLayout.addComponent(new MyUpload(student, exam, examService, liste, zorunluBilgi));
        
      //   genelLayout.addComponent(dosyaLayout);
       //  genelLayout.addComponent(bilgiLayout);
       //  genelLayout.addComponent(basvuruButonLayout);
     //   basvuruPanel.setContent(genelLayout);
       
        
        basvuruPanel.setContent(new MyUpload(student, app, appService, liste, zorunluBilgi));
        
        
     //   upload.setButtonCaption("Upload Now");
        
            
    }
}

   /*  */