/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.ResultsOfExam;
import com.example.model.Student;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.declarative.Design;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author hatice
 */
@DesignRoot
public class AnnouncedExams extends Panel{
    ExamService examService;
    Grid grid = new Grid();
    
    List<ResultsOfExam> results = new ArrayList<ResultsOfExam>();
    
    public AnnouncedExams(ExamService _examService, Student student){
        Design.read(this);
        examService = _examService;
        
        this.setStyleName("aciklananSinavlarPanel");
        
        grid.setStyleName("ResultsGrid");
        
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String today = dateFormat.format(date);
        
        examService.getResultsOfExamForStudent(student.getId()).forEach(
                (b)->{
                    ResultsOfExam a = new ResultsOfExam();
                    a.examName = b.get("e.name");
                    
                    a.dateOfPublication = b.get("i.dateOfPublication");
                    
                    if(b.get("i.mark") == null) a.mark = "-";
                    else {
                        Long fark = time(b.get("i.dateOfPublication"), today);

                        if(fark >= 0) a.mark = String.valueOf(b.get("i.mark"));
                        else a.mark = "-";
                    }
                    
                    results.add(a);
                }
        );
        
        final BeanItemContainer<ResultsOfExam> examBeans = new BeanItemContainer<>(ResultsOfExam.class);
        examBeans.addAll(results);

        grid.setContainerDataSource(examBeans);
        grid.removeAllColumns();
        grid.addColumn("examName");
        grid.addColumn("mark");
        
        grid.getColumn("examName").setHeaderCaption("Sınav Adı");
        grid.getColumn("mark").setHeaderCaption("Sonuç");
       
        grid.setImmediate(true);
        
        setContent(grid);
    }
    
    private long time(String tarih1,String tarih2){
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
        String inputString1 = tarih1;
        String inputString2 = tarih2;
        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace(); 
        }
        return 0;
    }
}
