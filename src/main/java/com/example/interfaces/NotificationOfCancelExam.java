/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.Exam;
import com.example.model.Student;
import com.example.service.ExamService;
import com.example.view.BasvurulanSinavlarView;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import java.util.ArrayList;
import java.util.List;
import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

/**
 *
 * @author hatice
 */
@DesignRoot
public class NotificationOfCancelExam extends VerticalLayout{
    
    ExamService examService;
    
    List<Exam> cancelExam = new ArrayList<Exam>();
    
    public NotificationOfCancelExam(ExamService _examService, Student student){
        Design.read(this);
        examService = _examService;
        this.setStyleName("nVerticalLayout");
        this.setVisible(false);
        
        cancelExam = examService.getIsDelete(student.getId());
        int kac = 0;
        if(cancelExam.size() > 0){
            setVisible(true);
            
            for (int i = 0; i < cancelExam.size(); i++) {
                kac++;
                if(kac == 0 || kac == 1) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout");
                else if(kac == 2) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout2");
                else if(kac == 3) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout3");
                else if(kac == 4) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout4");
                else if(kac == 5) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout5");
                else if(kac == 6) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout6");
                else if(kac == 7) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout7");
                else if(kac == 8) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout8");
                else if(kac == 9) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout9");
                else if(kac == 10) BasvurulanSinavlarView.bSinavlarLayout.setStyleName("bSinavlarLayout10");
                HorizontalLayout h = new HorizontalLayout();
                h.setStyleName("hLabel");
                Panel cancelPanel = new Panel();
                cancelPanel.addStyleName("cancelExamPanel");
                HorizontalLayout notHLayout = new HorizontalLayout();
                notHLayout.setStyleName("notHLayout");
                
                Label notIcon = new Label();
                notIcon.setIcon(new ThemeResource("images/notification.png"));
                Label a = new Label(cancelExam.get(i).getName()+" sınavınız, sınavın öğretmeni tarafından sınav için yeterliliğe sahip olmadığınız için iptal edilmiştir.");
                Button cancel = new Button();
                cancel.setId(Long.toString(cancelExam.get(i).getId()));
                
                a.addStyleName("cancelLabel");
                notIcon.setStyleName("notIcon");
                cancel.setStyleName("cancelButon");
                
                notHLayout.addComponent(notIcon);
                notHLayout.addComponent(a);
                
                cancel.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        System.out.println("tıklanan buton id: "+event.getButton().getId());
                        examService.deleteIsDelete(student.getId(), Long.parseLong(event.getButton().getId()));
                        
                        if(examService.getOneIsDelete(student.getId(), Long.parseLong(event.getButton().getId())) == null){
                            Page.getCurrent().reload();
                           // getUI().getNavigator().navigateTo("BaşvurulanSınavlar");
                        }
                        else{
                            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>İşlem Gerçekleşirken Bir Hata Oldu!</b>", Notification.Type.WARNING_MESSAGE, true); 

                            notif.setDelayMsec(300);
                            notif.setPosition(Position.TOP_CENTER);
                            notif.setStyleName("examValidation");
                            notif.setHtmlContentAllowed(true); 

                            notif.show(Page.getCurrent());
                        }
                    }
                });
                
                cancelPanel.setContent(notHLayout);
                h.addComponent(cancelPanel);
                h.addComponent(cancel);
                addComponent(h);
            }
        }
    }
}
