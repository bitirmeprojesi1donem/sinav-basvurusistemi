/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.Exam;
import com.example.model.InformationOfApps;
import com.example.model.Officer;
import com.example.model.Teacher;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hatice
 */
@DesignRoot
public class PastExams extends Panel{
    @Autowired
    ExamService examService;
    
    public Grid grid = new Grid();
    
    List<InformationOfApps> pastExams = new ArrayList<InformationOfApps>();
    List<InformationOfApps> tempPastExams = new ArrayList<InformationOfApps>();
    
    public PastExams(ExamService _examService, Teacher officer){
        Design.read(this);
        examService = _examService;
        
        this.addStyleName("pastExams");
        grid.addStyleName("gridPastExams");
        
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String today = dateFormat.format(date);
        
        examService.getAllExams(officer.getId()).forEach((b)->{
                    InformationOfApps a = new InformationOfApps();
                    a.examName = b.get("e.name");
                    a.departmentName = b.get("d.name");
                    a.basvuruBaslama = b.get("e.startTime");
                    a.basvuruBitis = b.get("e.endTime");
                    a.examId = String.valueOf(b.get("ID(e)"));
                    pastExams.add(a);
                }
        );
        
        BeanItemContainer<InformationOfApps> examBean = new BeanItemContainer<>(InformationOfApps.class);
        
        for(int i=0; i<pastExams.size(); i++){
//            Long a = time(pastExams.get(i).getExamDate(), today);
//
//            if(a > 0){
//                tempPastExams.add(pastExams.get(i));
//            }
        }
        
        examBean.addAll(tempPastExams);
        grid.setContainerDataSource(examBean);
        grid.removeAllColumns();
        grid.addColumn("examName");
        grid.addColumn("departmentName");
        grid.addColumn("basvuruBaslama").setHidden(true);
        grid.addColumn("basvuruBitis").setHidden(true);
        grid.addColumn("examId").setHidden(true);
        
        grid.getColumn("examName").setHeaderCaption("Adı");
        grid.getColumn("departmentName").setHeaderCaption("Bölümü");
        
        grid.addSelectionListener(selectionEvent -> {
            final Window studentOfExam = new Window("Alınan Sonuçları Giriniz");
            studentOfExam.setModal(true);
            studentOfExam.setPositionX(300);
            studentOfExam.setPositionY(35);
            studentOfExam.setWidth(700.0f, Unit.PIXELS);
            studentOfExam.setHeight(600.0f, Unit.PIXELS);
            studentOfExam.setDraggable(false);
            studentOfExam.setResizable(false);
            studentOfExam.setVisible(false);

            UI.getCurrent().addWindow(studentOfExam);
            
            Object selected = ((Grid.SingleSelectionModel) grid.getSelectionModel()).getSelectedRow();

            if(selected == null){
                studentOfExam.setVisible(false);
                return;
            }
            studentOfExam.setContent(new StudentsOfExam(selected, examService));
            
            
            studentOfExam.setVisible(true);
        });
        
        setContent(grid);
    }
    
    private long time(String tarih1,String tarih2){
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
        String inputString1 = tarih1;
        String inputString2 = tarih2;
        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
           // System.out.println (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace(); 
        }
        return 0;
    }
}
