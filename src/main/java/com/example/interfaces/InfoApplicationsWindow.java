 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.interfaces;

import com.example.model.Application;
import com.example.model.Classroom;
import com.example.model.Department;
import com.example.model.Document;
import com.example.model.Exam;
import com.example.model.Information;
import com.example.model.InformationOfApps;
import com.example.service.ApplicationService;
import com.example.service.ExamService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hatice
 */
@DesignRoot
class InfoApplicationsWindow extends VerticalLayout{
    Label  label42,  label48, label47, label41, label40, label50, label412,label444;
    Label lBolum40, lSinavAdi42, lbasvuruBaslama47, lbasvuruBitis48, lsinavBilgi41, lsinavBelge50,lSinavAdi412,lkriter44;
    VerticalLayout ekleme1, ekleme2, ekleme4;
    HorizontalLayout hEkleme;
     VerticalLayout sinvBilgi = new VerticalLayout();
   
   
    ArrayList<Information> infos = new ArrayList<Information>();
    ArrayList<Document> docs = new ArrayList<Document>();
     ArrayList<Exam> liste = new ArrayList<Exam>();
    
      Grid sinavGrid = new Grid();
     
    Application exam = new Application();
    InformationOfApps ioe = new InformationOfApps();
    
    ExamService examService;
    ApplicationService appService;
    
    String classes = "";
    String info = "";
    String  doc = "";
    
   
    
    @Autowired
    Department dep;
    public InfoApplicationsWindow(Object selected, ExamService _examService,ApplicationService _appService) {
        Design.read(this);
        
        ioe = (InformationOfApps) selected;
        exam.id = Long.parseLong(ioe.getExamId());
        exam.name = ioe.getExamName();
        exam.startTime = ioe.getBasvuruBaslama();
        exam.endTime = ioe.getBasvuruBitis();
        //exam.totalOfPercent = ioe.getTotalOfPercent();
        
        appService = _appService;
        examService =  _examService;
        
        lBolum40.setValue(appService.getApplicationDepartment(exam.getId()));
        lSinavAdi42.setValue(exam.name);
     
      
        
        
        lbasvuruBaslama47.setValue(exam.getStartTime());
        lbasvuruBitis48.setValue(exam.getEndTime());
        
     
         List<Exam> liste =  examService.findExamsforapp(exam.getId());
        BeanItemContainer<Exam> depl = new BeanItemContainer<>(Exam.class);
        depl.addAll(liste);
        
        sinavGrid.setContainerDataSource(depl);
        sinavGrid.removeAllColumns();
        sinavGrid.addColumn("name");
        sinavGrid.addColumn("id").setHidden(true);
        sinavGrid.getColumn("name").setHeaderCaption("Sinv Adı");
      

        sinavGrid.addSelectionListener(selectionEvent -> {
            sinvBilgi.removeAllComponents();
            Object select = ((Grid.SingleSelectionModel) sinavGrid.getSelectionModel()).getSelectedRow();
            if(select == null){
               
            }else {
                Exam ex = (Exam) select;
                Long exId = ex.id;
                
                final Window window2 = new Window("Başvuru İçin Sınav Bilgileri");
                window2.setModal(true);

                window2.setStyleName("ilkwindow");
                window2.setWidth("400");
                window2.setHeight("500");

                window2.setDraggable(false);
                window2.setResizable(false);


                Label ltr1;
                ltr1 = new Label();
                ltr1.setStyleName("stil");
                Label ltr2;
                ltr2 = new Label();
                 ltr2.setStyleName("stil");
                 Label ltr3 = new Label();
                 ltr3.setStyleName("stil");
                   Label ltr4 = new Label();
                 ltr4.setStyleName("stil");
                  Label ltr5 = new Label();
                 ltr5.setStyleName("stil");
                 ltr1.setValue("Sinav Tarihi "+ex.date);
                 ltr2.setValue("Sinav Zamani "+ex.time);
                 ltr3.setValue("Kontenjani "+ex.getPersonCount());
                 ltr4.setValue("Türü "+examService.getTypeForExam(ex.getId()));
                 ltr5.setValue("Yapilacagi derslikler "+examService.getClasroom(ex.getId()));
                 
                sinvBilgi.addComponent(ltr1);
                sinvBilgi.addComponent(ltr2);
                sinvBilgi.addComponent(ltr3);
                sinvBilgi.addComponent(ltr4);
                sinvBilgi.addComponent(ltr5);
                window2.setContent(sinvBilgi);

                UI.getCurrent().addWindow(window2);

                
               
                
                System.out.println("girdi");
            }
        });
        sinavGrid.setStyleName("snvGrd");
        ekleme1.addComponent(sinavGrid);

//        Label ltr;
//           
//         Button buton = null;
//        for (int i = 0; i < liste.size(); i++) {
//           ltr = new Label();
//            ltr.setValue(liste.get(i).getName());
//            buton = new Button("sinav bilgileri");
//            
//            ekleme4.addComponent(ltr);
//            ekleme4.addComponent(buton);
//           
//        }
//        
//       
//          buton.addClickListener(new Button.ClickListener() {
//
//            @Override
//            public void buttonClick(Button.ClickEvent event) {
//                Label ltr1 = null;
//                 final Window window2 = new Window("Başvuru İçin Sınav Bilgileri");
//                window2.setModal(true);
//
//                window2.setStyleName("ilkwindow");
//                window2.setWidth("1200");
//                window2.setHeight("500");
//
//                window2.setDraggable(false);
//                window2.setResizable(false);
//
//
//
//
//                 String name;
//                 for (int i = 0; i < liste.size(); i++) 
//                 {
//                      ltr1 = new Label();
//
//                       ltr1.setValue( liste.get(i).getName());
//
//                 }
//
//
//                sinvBilgi.addComponent(ltr1);
//                window2.setContent(sinvBilgi);
//
//                UI.getCurrent().addWindow(window2);
//            }
//        });
//        
        //System.out.println(appService.getAppTotOfPer(exam.getId()));
        if(appService.getAppTotOfPer(exam.getId()) != null)
        {
            lkriter44.setValue(appService.getAppTotOfPer(exam.getId()).toString());
        }
        else
        {
            lkriter44.setValue("yoktur");
        }
        
    
        infos = appService.getInformationByAppId(exam.getId());
        for (int i = 0; i < infos.size(); i++) {
            if(i == infos.size()-1){
                info += infos.get(i).getName()+"(" +appService.getRequiredInfo(exam.getId(),infos.get(i).getId())+ "%)";
            }
            else{
                info += infos.get(i).getName()+"(" +appService.getRequiredInfo(exam.getId(),infos.get(i).getId())+ "%)"+", ";
            } 
        }
        if(info.isEmpty()){
            lsinavBilgi41.setValue("Bulunmamaktadır");
        }
        else{
            lsinavBilgi41.setValue(info);
        }
        
        
        docs = appService.getDocumentByAppId(exam.getId());
        for (int i = 0; i < docs.size(); i++) {
            if(i == docs.size()-1){
                doc += docs.get(i).getName();
            }
            else{
                doc += docs.get(i).getName()+", ";
            } 
        }
        if(doc.isEmpty()){
            lsinavBelge50.setValue("Bulunmamaktadır");
        }
        else{
            lsinavBelge50.setValue(doc);
        }    
        
       
        hEkleme.addComponent(ekleme1);
        hEkleme.addComponent(ekleme2);
        hEkleme.addComponent(ekleme4);
        this.addComponent(hEkleme);
    }
    
}
