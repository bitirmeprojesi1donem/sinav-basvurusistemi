/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.TextRenderer;
import java.util.List;

/**
 *
 * @author hatice
 */
public class StatikSinifVeFonksiyonlar {
    public static <T> void refreshGridWithHeaders(List<T> tList, final Class<T> classType, Grid grid, String[] gosterilecekAlanlar, String[] alanBasliklari) {
        BeanItemContainer<T> tBeanItemContainer;
        tBeanItemContainer = Util.getFilledBeanItemContainer(classType, tList);
        grid.setContainerDataSource(tBeanItemContainer);

        grid.removeAllColumns();

        int i = 0;
        for (String s : gosterilecekAlanlar) {
            if(i == gosterilecekAlanlar.length-1) grid.addColumn(s).setHidden(true);
            else{
                grid.addColumn(s);
                grid.getColumn(s).setHeaderCaption(alanBasliklari[i]);
            }
            i++;
        }
    }
}
