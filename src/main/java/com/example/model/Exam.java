/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.model;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Labels;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 *
 * @author hatice
 */
@NodeEntity
public class Exam {
    @GraphId public Long id;

    public String name;
    public String date;
    public String time;
    public int personCount;
    
    public List<Criterion> examKriterList = new ArrayList<Criterion>();

    public List<Classroom> classliste = new ArrayList<Classroom>();
    public Type examType = new Type();
    
    public Exam() {
       
    }

    public int getPersonCount() {
        return personCount;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

   
    
    public Exam( String name, String date, String time)
    {
        this.date = date;
    //    this.departmentName = departmentName;
        this.name = name;
        //this.teacher = teacher;
        //this.type = type;
        this.time = time;
    }
    
    public boolean equals(Object other) {
        if (this == other) return true;
        if (id == null) return false;
        if (!(other instanceof Exam)) return false;
        return id.equals(((Exam) other).id);
    }

    public int hashCode() {
        return id == null ? System.identityHashCode(this) : id.hashCode();
    }
}
