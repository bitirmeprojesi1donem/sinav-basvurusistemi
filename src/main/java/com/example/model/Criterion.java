/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Labels;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 *
 * @author hatice
 */
@NodeEntity
public class Criterion {
    @GraphId public Long id;
    public int averagePercent;
    
    public List<Information> infoList = new ArrayList<Information>();
    
    public List<Integer> percentList = new ArrayList<Integer>();

    public List<Exam> examList = new ArrayList<Exam>();
    
    public String formul;
    public int asilKontenjan;
    public int yedekKontenjan;

    public String getFormul() {
        return formul;
    }

    public void setFormul(String formul) {
        this.formul = formul;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAveragePercent() {
        return averagePercent;
    }

    public void setAveragePercent(int averagePercent) {
        this.averagePercent = averagePercent;
    }

    public List<Information> getInfoList() {
        return infoList;
    }

    public void setInfoList(List<Information> infoList) {
        this.infoList = infoList;
    }

    public List<Integer> getPercentList() {
        return percentList;
    }

    public void setPercentList(List<Integer> percentList) {
        this.percentList = percentList;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + this.averagePercent;
        hash = 59 * hash + Objects.hashCode(this.infoList);
        hash = 59 * hash + Objects.hashCode(this.percentList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Criterion other = (Criterion) obj;
        if (this.averagePercent != other.averagePercent) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.infoList, other.infoList)) {
            return false;
        }
        if (!Objects.equals(this.percentList, other.percentList)) {
            return false;
        }
        return true;
    }
    
    
}
