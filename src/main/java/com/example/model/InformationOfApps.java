/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.model;

import java.util.Objects;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 *
 * @author hatice
 */
@NodeEntity
public class InformationOfApps {
    //@GraphId public Long id;
    
    public String examName;
    public String departmentName;
    public String examId;
    public String basvuruBaslama;
    public String basvuruBitis;
    public String sinavGerekliBilgi;
    public String sinavGerekliBelge;
    public String teacher;

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
    
    public String getBasvuruBaslama() {
        return basvuruBaslama;
    }

    public void setBasvuruBaslama(String basvuruBaslama) {
        this.basvuruBaslama = basvuruBaslama;
    }

    public String getBasvuruBitis() {
        return basvuruBitis;
    }

    public void setBasvuruBitis(String basvuruBitis) {
        this.basvuruBitis = basvuruBitis;
    }

    public String getSinavGerekliBilgi() {
        return sinavGerekliBilgi;
    }

    public void setSinavGerekliBilgi(String sinavGerekliBilgi) {
        this.sinavGerekliBilgi = sinavGerekliBilgi;
    }

    public String getSinavGerekliBelge() {
        return sinavGerekliBelge;
    }

    public void setSinavGerekliBelge(String sinavGerekliBelge) {
        this.sinavGerekliBelge = sinavGerekliBelge;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.examName);
        hash = 97 * hash + Objects.hashCode(this.departmentName);
        hash = 97 * hash + Objects.hashCode(this.examId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InformationOfApps other = (InformationOfApps) obj;
        if (!Objects.equals(this.examName, other.examName)) {
            return false;
        }
        if (!Objects.equals(this.departmentName, other.departmentName)) {
            return false;
        }
        if (!Objects.equals(this.examId, other.examId)) {
            return false;
        }
        return true;
    }
    
    
}
