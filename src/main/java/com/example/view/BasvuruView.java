package com.example.view;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//import com.example.interfaces.Basvuru;
import com.example.interfaces.Basvuru;
import com.example.model.Exam;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Deneme
 */
@UIScope
@SpringView(name = "SinavBasvuru")
public class BasvuruView extends CustomComponent implements View {
    CssLayout basvuruLayout = new CssLayout();

    @Autowired
    DepartmentService d;

    @Autowired
    ExamService examService;

    @Autowired
    DocumentService documentService;

    @Autowired
    InformationService informationService;
    
    @Autowired
    ApplicationService appService;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Student student = (Student) getSession().getAttribute("userRole");

        if (student instanceof Student) {
            setCompositionRoot(basvuruLayout);
            basvuruLayout.setStyleName("basvurulayout");
            basvuruLayout.addComponent(new Basvuru(appService,d, examService, student, documentService, informationService));
        } else if (getSession().getAttribute("userRole") instanceof Teacher) {
            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sayfaya erişim yetkiniz bulunmamaktadır!</b>", Notification.Type.WARNING_MESSAGE, true);

            notif.setDelayMsec(2000);
            notif.setPosition(Position.TOP_CENTER);
            notif.setStyleName("errorValidation");
            notif.setHtmlContentAllowed(true);

            notif.show(Page.getCurrent());
            getUI().getCurrent().getNavigator().navigateTo("Login");
        } else {
            getUI().getCurrent().getNavigator().navigateTo("Login");
        }

    }

}
