/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.view;

import com.example.interfaces.Login;
import com.example.model.Officer;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.service.ExamService;
import com.example.service.OfficerService;
import com.example.service.StudentService;
import com.example.service.TeacherService;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author Deneme
 */


@UIScope
@SpringView(name = "Login")
public class LoginView extends CustomComponent implements View {
    public static Teacher teach;
    public static Student student;
    public static Officer officer;

    @Autowired
    TeacherService teacherService;

    @Autowired
    StudentService studentService;

    @Autowired
    OfficerService officerService;
    
    Login login;
    CssLayout loginLayout;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        System.out.println("enter");
    }

    public LoginView() {
        System.out.println("constructor");
        loginLayout = new CssLayout();
        login = new Login();

        loginLayout.addComponent(login);
        loginLayout.setStyleName("loginLayout");

        setCompositionRoot(loginLayout);
        init();
    }

    public void init() {

        login.loginButton.addClickListener(clickEvent -> {
            if (login.tc.getValue() == "") login.tc.setRequired(true);
            else if (login.parola.getValue() == "") login.parola.setRequired(true);
            else {
                login.TC = login.tc.getValue();
                login.pass = login.parola.getValue();
                System.out.println(login.TC+" "+login.pass);
                if (officerService.getOfficer(login.TC, login.pass) != null && teacherService.getTeacher(login.TC, login.pass) != null) {
                    System.out.println("girdi");
                    officer = officerService.getOfficer(login.TC, login.pass);
                    getSession().setAttribute("userRole", officer);
                    getUI().getNavigator().navigateTo("Sinav-DerslikEkleme");
                }
                else if (teacherService.getTeacher(login.TC, login.pass) != null) {
                    teach = teacherService.getTeacher(login.TC, login.pass);
                    getSession().setAttribute("userRole", teach);
                    getUI().getNavigator().navigateTo("SinavEkleme");
                } 
                else if (studentService.getStudent(login.TC, login.pass) != null) {
                    student = studentService.getStudent(login.TC, login.pass);
                    getSession().setAttribute("userRole", student);
                    // log out icin, log-out butonuna tiklandiginda:
                    // 1. getSession().setAttribute("userRole", null);
                    // 2. logine yonlendir.
                    getUI().getNavigator().navigateTo("SinavBasvuru");
                }
                
                else {
                    Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Böyle bir kullanıcı bulunmamaktadır!</b>", Notification.Type.WARNING_MESSAGE, true);

                    notif.setDelayMsec(300);
                    notif.setPosition(Position.TOP_CENTER);
                    notif.setStyleName("errorValidation");
                    notif.setHtmlContentAllowed(true);

                    notif.show(Page.getCurrent());
                }
            }
        });
    }
}
