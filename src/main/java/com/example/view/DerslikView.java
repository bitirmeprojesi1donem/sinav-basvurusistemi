/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.view;

//import com.example.interfaces.Basvuru;
import com.example.interfaces.DerslikEkle;
import com.example.interfaces.ExamsUsers;
import com.example.interfaces.PastExams;
import com.example.interfaces.addApplication;
import com.example.interfaces.addApplicationsWindow;
import com.example.model.Officer;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.ClassroomService;
import com.example.service.CriterionService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.example.service.OfficerService;
import com.example.service.TypeService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hatice
 */
@UIScope
@SpringView(name = "Sinav-DerslikEkleme")
public class DerslikView extends CustomComponent implements View {

    @Autowired
    ExamService examService;

    @Autowired
    DepartmentService dep;

    @Autowired
    TypeService typeService;

    @Autowired
    DocumentService documentService;

    @Autowired
    InformationService informationService;
    
    @Autowired
    OfficerService officerService;
    
    @Autowired
    ClassroomService cService;

    @Autowired
    ApplicationService appService;
    
    @Autowired
    CriterionService criterionService;
    
    VerticalLayout logout;
    MenuBar barmenu;
    CssLayout eklemeLayout;
    Officer officer;

    Window window;
    
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    public DerslikView() {
        logout = new VerticalLayout();
        barmenu = new MenuBar();
        eklemeLayout = new CssLayout();
        officer = (Officer) getUI().getCurrent().getSession().getAttribute("userRole");
        // setCompositionRoot(eklemeLayout);
        if (officer != null) {
            barmenu.setStyleName("eklemebarmenu");
            logout.addComponent(barmenu);
            logout.setStyleName("eklemelogout");
            MenuBar.Command mycommand = new MenuBar.Command() {
                public void menuSelected(MenuBar.MenuItem selectedItem) {
                    if (selectedItem.getText().equals("Çıkış Yap")) {
                        getSession().setAttribute("userRole", null);
                        getUI().getNavigator().navigateTo("Login");
                    }
                    if(selectedItem.getText().equals("Derslik Bilgisi Ekle")){
                        window = new Window("Derslik Bilgisi Ekle");
                        window.setModal(true);

                        window.setWidth("400");
                        window.setHeight("310");

                        window.setDraggable(false);
                        window.setResizable(false);
                        window.setContent(new DerslikEkle(officerService, cService));
                        UI.getCurrent().addWindow(window);
                    }
                }
            };

            MenuBar.MenuItem menu = barmenu.addItem("", new ThemeResource("images/align-justify.png"), null);
            MenuBar.MenuItem derslik = menu.addItem("Derslik Bilgisi Ekle", new ThemeResource("images/add.png"), mycommand);
            MenuBar.MenuItem cikis = menu.addItem("Çıkış Yap", new ThemeResource("images/logout.png"), mycommand);
            eklemeLayout.addComponent(logout);
            eklemeLayout.setStyleName("eklemeLayout");
           
            setCompositionRoot(eklemeLayout);
        } else if (String.valueOf(getSession().getAttribute("userRole")) == "student") {
            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sayfaya erişim yetkiniz bulunmamaktadır!</b>", Notification.Type.WARNING_MESSAGE, true);

            notif.setDelayMsec(2000);
            notif.setPosition(Position.TOP_CENTER);
            notif.setStyleName("errorValidation");
            notif.setHtmlContentAllowed(true);

            notif.show(Page.getCurrent());
            getUI().getNavigator().navigateTo("Login");
        } else {
            getUI().getNavigator().navigateTo("Login");
        }

    }

    @PostConstruct
    public void init() {
        eklemeLayout.addComponent(new addApplication(criterionService, appService, examService, dep, typeService, officer, documentService, informationService, cService));
        eklemeLayout.addComponent(new ExamsUsers(examService, officer));
        eklemeLayout.addComponent(new PastExams(examService, officer));        
        
    }
}
