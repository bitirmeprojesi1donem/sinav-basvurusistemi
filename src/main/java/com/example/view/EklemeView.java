/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.view;

import com.example.MainUI;
import com.example.interfaces.ExamsUsers;
import com.example.interfaces.PastExams;
import com.example.interfaces.addApplication;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.ClassroomService;
import com.example.service.CriterionService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.example.service.TypeService;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author Deneme
 */
@UIScope
@SpringView(name = "SinavEkleme")
public class EklemeView extends CustomComponent implements View {

    @Autowired
    ExamService examService;

    @Autowired
    DepartmentService dep;

    @Autowired
    TypeService typeService;

    @Autowired
    DocumentService documentService;

    @Autowired
    InformationService informationService;
    
    @Autowired
    ClassroomService cService;

    @Autowired
    ApplicationService appService;
    
    @Autowired
    CriterionService criterionService;
    
    VerticalLayout logout;
    MenuBar barmenu;
    CssLayout eklemeLayout;
    Teacher teacher;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    public EklemeView() {
        logout = new VerticalLayout();
        barmenu = new MenuBar();
        eklemeLayout = new CssLayout();
        teacher = (Teacher) getUI().getCurrent().getSession().getAttribute("userRole");
        // setCompositionRoot(eklemeLayout);
        if (teacher != null) {
            barmenu.setStyleName("eklemebarmenu");
            logout.addComponent(barmenu);
            logout.setStyleName("eklemelogout");
            MenuBar.Command mycommand = new MenuBar.Command() {
                public void menuSelected(MenuBar.MenuItem selectedItem) {
                    if (selectedItem.getText().equals("Çıkış Yap")) {
                        getSession().setAttribute("userRole", null);
                        getUI().getNavigator().navigateTo("Login");
                    }
                }
            };

            MenuBar.MenuItem menu = barmenu.addItem("", new ThemeResource("images/align-justify.png"), null);
            MenuBar.MenuItem cikis = menu.addItem("Çıkış Yap", new ThemeResource("images/logout.png"), mycommand);
            eklemeLayout.addComponent(logout);
            eklemeLayout.setStyleName("eklemeLayout");
            //eklemeLayout.addComponent(new ExamsUsers(examService, LoginView.teach));
            //eklemeLayout.addComponent(new addApplication(examService, dep, typeService, LoginView.teach, documentService, informationService));

            setCompositionRoot(eklemeLayout);
        } else if (String.valueOf(getSession().getAttribute("userRole")) == "student") {
            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sayfaya erişim yetkiniz bulunmamaktadır!</b>", Notification.Type.WARNING_MESSAGE, true);

            notif.setDelayMsec(2000);
            notif.setPosition(Position.TOP_CENTER);
            notif.setStyleName("errorValidation");
            notif.setHtmlContentAllowed(true);

            notif.show(Page.getCurrent());
            getUI().getNavigator().navigateTo("Login");
        } else {
            getUI().getNavigator().navigateTo("Login");
        }

    }

    @PostConstruct
    public void init() {
        eklemeLayout.addComponent(new addApplication(criterionService, appService, examService, dep, typeService, teacher, documentService, informationService, cService));
        eklemeLayout.addComponent(new ExamsUsers(examService, teacher));
        eklemeLayout.addComponent(new PastExams(examService, teacher));
    }
}
