/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.view;

import com.example.interfaces.AnnouncedExams;
//import com.example.interfaces.Basvuru;
import com.example.interfaces.DerslikEkle;
import com.example.interfaces.ExamsUsers;
import com.example.interfaces.NotificationOfCancelExam;
import com.example.interfaces.PastExams;
import com.example.interfaces.ReferencedExams;
import com.example.interfaces.addApplication;
import com.example.model.Officer;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.service.ApplicationService;
import com.example.service.ClassroomService;
import com.example.service.DepartmentService;
import com.example.service.DocumentService;
import com.example.service.ExamService;
import com.example.service.InformationService;
import com.example.service.OfficerService;
import com.example.service.TypeService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hatice
 */
@UIScope
@SpringView(name = "BaşvurulanSınavlar")
public class BasvurulanSinavlarView extends CustomComponent implements View{
    @Autowired
    ExamService examService;

    @Autowired
    TypeService typeService;

    @Autowired
    DocumentService documentService;

    @Autowired
    InformationService informationService;

    @Autowired
    ApplicationService appService;
    
    VerticalLayout logout;
    MenuBar barmenu;
    public static CssLayout bSinavlarLayout;
    
    Student student;    
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        
    }
    
    
    public BasvurulanSinavlarView(){
        logout = new VerticalLayout();
        barmenu = new MenuBar();
        bSinavlarLayout = new CssLayout();
        
        student = (Student) getUI().getCurrent().getSession().getAttribute("userRole");

        if (student instanceof Student) {
            barmenu.setStyleName("barmenu");
            logout.addComponent(barmenu);
            logout.setStyleName("eklemelogout");
            MenuBar.Command mycommand = new MenuBar.Command() {
                @Override
                public void menuSelected(MenuBar.MenuItem selectedItem) {
                    if(selectedItem.getText().equals("Ana Sayfa")){
                        getUI().getNavigator().navigateTo("SinavBasvuru");
                    }
                    else if(selectedItem.getText().equals("Başvurulan Sınavlar")){
                        getUI().getNavigator().navigateTo("BaşvurulanSınavlar");
                    }
                    else if(selectedItem.getText().equals("Çıkış Yap")){
                        getSession().setAttribute("userRole", null);
                        getUI().getNavigator().navigateTo("Login");
                    }
                }  
            };
            MenuBar.MenuItem menu = barmenu.addItem("", new ThemeResource("images/align-justify.png"), null);
            MenuBar.MenuItem anaSayfa = menu.addItem("Ana Sayfa", new ThemeResource("images/home.png"), mycommand);
            MenuBar.MenuItem basvurularim = menu.addItem("Başvurulan Sınavlar", new ThemeResource("images/online-test.png"), mycommand);
            MenuBar.MenuItem cikis = menu.addItem("Çıkış Yap", new ThemeResource("images/sign-out.png"), mycommand);
            bSinavlarLayout.addComponent(logout);
            bSinavlarLayout.setStyleName("bSinavlarLayout");
           
            setCompositionRoot(bSinavlarLayout);
        } else if (getSession().getAttribute("userRole") instanceof Teacher) {
            Notification notif = new Notification("<i class=\"icon\" style=\"color: white;\">&#xe7ad;</i>", "<b style='color: #FFFFFF;'>Bu sayfaya erişim yetkiniz bulunmamaktadır!</b>", Notification.Type.WARNING_MESSAGE, true);

            notif.setDelayMsec(2000);
            notif.setPosition(Position.TOP_CENTER);
            notif.setStyleName("errorValidation");
            notif.setHtmlContentAllowed(true);

            notif.show(Page.getCurrent());
            getUI().getCurrent().getNavigator().navigateTo("Login");
        } else {
            getUI().getCurrent().getNavigator().navigateTo("Login");
        }       
    }
    
    @PostConstruct
    public void init() {
        bSinavlarLayout.addComponent(new NotificationOfCancelExam(examService, student));
        bSinavlarLayout.addComponent(new ReferencedExams(appService, examService, student));
        bSinavlarLayout.addComponent(new AnnouncedExams(examService, student));
    }
}
