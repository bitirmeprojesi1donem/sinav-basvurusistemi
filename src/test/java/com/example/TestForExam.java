/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import com.example.connect.DbConnect;
import com.example.model.Department;
import com.example.model.Exam;
import com.example.model.Officer;
import com.example.model.Teacher;
import com.example.service.DepartmentService;
import com.example.service.ExamService;
import com.example.service.TeacherService;
import org.apache.tomcat.jni.SSLContext;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author hatice
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DbConnect.class)
@ActiveProfiles(profiles = "test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestForExam {
    
    @Autowired 
    ExamService exam; 
    
    @Autowired
    TeacherService t;
    
    @Autowired
    DepartmentService depService;
     //Exam model = new Exam("Diferansiyel","121212","09:00");
       
    @Autowired Neo4jOperations n;
     
    Long oid; 
    Long tid;
    @Before
    public void createOfficer(){
        Officer officer2 = new Officer();
        officer2.name = "Natavan";
        officer2.surname = "Mirzayeva";
        officer2.tc = "11111111111";
        officer2.password = "1234";

        Teacher teacher = new Teacher();
        teacher.name = "Hatice";
        teacher.surname = "ERTÜRK";
        teacher.tc = "12345678910";
        teacher.password = "1234";
        
        assertEquals(officer2, n.save(officer2));
        //oid = n.save(officer2).getId();
        assertEquals(teacher, n.save(teacher));
        //tid = n.save(teacher).getId();
    }
     
    @Test
    public void createWorkIn(){
        Long lid = 46L;
        
        
        Long oid = t.getTeacher("11111111111", "1234").getId();
        assertEquals(oid, depService.createWorkIn(lid, oid));
        
        Long tid = t.getTeacher("12345678910", "1234").getId();
        assertEquals(tid, depService.createWorkIn(lid, tid));
    }
     
       /* @Test
        public void CreateExamTest()
        {
           
            assertEquals(true, exam.control(model));
            assertEquals(model.name, exam.CreateExam(model));
        }
        
        @Test
        public void DeleteExamTest()
        {
            assertEquals(false, exam.control(model));
            exam.DeleteExam(model);
            assertEquals(true, exam.control(model));
        }
        
        
        
        
        @Test
        public void UpdateExamTest()
        {
            
            assertEquals(false,model.equals(exam.UpdateExam(model)));
        }
        */
        
}
